# README #

This project is the Real-time heuristic search framework. It is intended to provide researchers the ability to implement and extend existing real-time algorithms.
Testing will be added to the master branch with automated reports displayed on the project wiki.

### What is this repository for? ###
* Real-time search testing framework. We want to create an easy way for researchers to share and test their algorithms in a variety of settings. The project is still in its infancy, so feel free to make suggestions and edits. 

### Contribution guidelines ###

* Writing unit tests to help ensure the environments accuracy is greatly appreciated as it gives confidence in our results
* Implementing Search Algorithms, at the heart of project is being able to test algorithms. Add your own examples following our guide:
[Adding New Algorithms](https://bitbucket.org/ircl/rts-framework/wiki/Adding%20New%20Algorithms)
* Submitting Issue so we know the work you'd like to see done such as adding new ways to test the algorithms. 
* Bug fixes and reporting
### Testing a build ###

RTSGameTests can be ran to confirm your build is working.

### Running the build ###

* Install Monodevelop
* run mono RTSGame.exe
    * RTSGame.exe [name of config file] [index of problem]
    * optional [name of config file] - name of the config. If blank then experiment.json
    * optional [index of problem] - index of the problem to run out of the ones generated. if blank all problems are ran

For running the build on ComputeCanada use ccRunner.sh. 

### How do I get set up for development and building? ###

* Requires C#, visual studios and monogame. Besure to install depenencies listed in monogame setup
    * Install Visual Studio 2015 / 2017 : https://www.visualstudio.com/downloads/ (.NET desktop development framework)
    * Install MonoGame: http://rbwhitaker.wikidot.com/setting-up-monogame
* Besure to compile for a 64 bit machine to ensure collision free hashes. (16bit row, 16 col, 32 bit time)
* Experiment configurations are in App.Config
* Test will cover two aspects benchmarking against algorithms and ensuring functions are operating as expected.
* Development has been tested on windows 8 and 10, and ran on compute canada linux clusters. All other operating systems are used at users discretion but should be doable with .net ports.
    * http://www.monodevelop.com/
* Build and run application.
helpful link: https://docs.microsoft.com/en-us/windows/uwp/get-started/get-started-tutorial-game-mg2d

### Who do I talk to? ###

* Devon Sigurdson (dbsigurd@ualberta.ca)
* Vadim Bulitko (Bulitko@ualberta.ca)
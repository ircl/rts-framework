﻿using RTSGame.Models.ExperimentModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame
{
    public class ExperimentRunner
    {
        ExperimentParameters currentParameters;
        ParameterConfiguration pc;
        ResultsModel resultsModel;
        //
        // TODO: Add test logic here
        //
        /// <summary>
        /// Sets the main controller to know that experiment has finished.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //public void finishedExperiment(object sender, EventArgs e)
        //{
        //    if (pc.hasNextConfiguration())
        //    {
        //        currentParameters = pc.getNextConfiguration();
        //        //createGameModel();
        //    }
        //    else
        //    {
        //        finishedAllExperiments = true;
        //    }
        //}
        private GameModel CreateGameModel(ExperimentParameters pc)
        {
            GameModel gm = new GameModel(pc,resultsModel);
            return gm;
            //_gameModel.FinishedExperiments += finishedExperiment;

        }
        public ExperimentRunner(string experimentJSON)
        {
            SetupConfigFile(experimentJSON);
            currentParameters = pc.getCurrentParameters();
            string resultFolder = Path.Combine(currentParameters.experimentParameters.resultsPath);
            if (Directory.Exists(resultFolder))
            {
                Directory.Delete(resultFolder, true);
            }
            resultsModel = new ResultsModel(currentParameters);
        }
        public ExperimentRunner(String experimentJSON, int startI, int endI)
        {
            SetupConfigFile(experimentJSON);
            List<ExperimentParameters> experimentParams = pc.getParameters();
            if (experimentParams.Count < startI || startI < 0)
            {
                return;
            }
            currentParameters = experimentParams[startI];
            if (startI == 0)
            {
                OutputInfo outInfo = currentParameters.outputInfo;
                outInfo.copyConfigFile = true;
                currentParameters.outputInfo = outInfo;
            }
            resultsModel = new ResultsModel(currentParameters, startI);
        }
        public ExperimentRunner(String experimentJSON, int index)
        {
            SetupConfigFile(experimentJSON);
            List<ExperimentParameters> experimentParams = pc.getParameters();
            if (experimentParams.Count < index || index < 0)
            {
                return;
            }
            currentParameters = experimentParams[index];
            if (index == 0)
            {
                OutputInfo outInfo = currentParameters.outputInfo;
                outInfo.copyConfigFile = true;
                currentParameters.outputInfo = outInfo;
            }
            resultsModel = new ResultsModel(currentParameters, index);
        }
        private void SetupConfigFile(string experimentJSON)
        {
            pc = new ParameterConfiguration(experimentJSON);
            Console.WriteLine("Machine: {0}", System.Environment.MachineName);
        }


        public List<String> emails;

        public bool RunExperiments()
        {

            List<ExperimentParameters> experimentParams = pc.getParameters();
            int completed = 0;
            int numberToComplete = experimentParams.Count();
            Console.WriteLine("Parallel Run");
            Console.WriteLine("completed {0} / {1}", 0, numberToComplete);
#if DEBUG
            for (int i = 0; i < experimentParams.Count; i++)
            {
                ExperimentParameters ep = experimentParams[i];
                GameModel gm = CreateGameModel(ep);
                Console.WriteLine("completed {0} / {1}", completed, numberToComplete);
                gm.runToCompletion();
                completed++;

            }
#else

             Parallel.ForEach<ExperimentParameters>(experimentParams, (param, pls, index) =>
             {
                GameModel gm = CreateGameModel(param);
                //Console.WriteLine(param.index.ToString() +": " + index.ToString());
                gm.runToCompletion();
                var curCompleted = Interlocked.Increment(ref completed);
                Console.WriteLine("completed {0} / {1}", curCompleted, numberToComplete);
             });
#endif
            return true;
        }
        public bool RunExperiments(int index)
        {
            List<ExperimentParameters> experimentParams = pc.getParameters();
            if (experimentParams.Count < index || index <0)
            {
                Console.WriteLine("Experiment index out of range");
                return true;
            }
            
            int completed = index;
            int numberToComplete = experimentParams.Count();
            ExperimentParameters param = experimentParams[index];
            GameModel gm = CreateGameModel(param);
            gm.runToCompletion();
            Console.WriteLine("completed {0} / {1}", completed, numberToComplete);
            return true;
        }
        public bool RunExperiments(int startI, int endI)
        {
            List<ExperimentParameters> experimentParams = pc.getParameters();
            endI = Math.Min(endI, experimentParams.Count() - 1);
            if (experimentParams.Count < startI || startI < 0||
                experimentParams.Count < endI || endI < 0)
            {
                Console.WriteLine("Experiment index out of range");
                return true;
            }

            int completed = startI;
            int numberToComplete = experimentParams.Count();
            for (int i = startI; i <= endI; i++)
            {
                ExperimentParameters ep = experimentParams[i];
                GameModel gm = CreateGameModel(ep);
                Console.WriteLine("completed {0} / {1}", completed, numberToComplete);
                gm.runToCompletion();
                completed++;

            }
            return true;
        }
    }
}

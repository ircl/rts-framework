﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Configuration;
using RTSGame.Models.ExperimentModels;
using RTSGame.View;
using RTSGame.View.AgentViews;
using System.IO;
using System.Diagnostics;
using static RTSGame.ParameterConfiguration;

namespace RTSGame
{
    /// <summary>
    /// This is the main controller for the Real-time Search game.
    /// It handles the user interaction
    /// </summary>
    public class MainController : Game
    {
        private int speed;
        private int sizeOfSprites = 64;
        private KeyboardState oldState;
        private Boolean visualizeExperiment;
        GraphicsDeviceManager graphics;
        private Camera camera = new Camera();
        SpriteBatch spriteBatch;

        private TileView tileView;
        private GameModel _gameModel;
        private ScoreView scoreView;
        private AgentViewController agentViewController;
        /// <summary>
        /// Start the experiments
        /// </summary>
        public MainController()
           : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        private ExperimentParameters currentParameters;
        Stopwatch sw = new Stopwatch();
        ParameterConfiguration pc;
        ResultsModel results;
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            pc = new ParameterConfiguration("experiment.json");
            currentParameters = pc.getCurrentParameters();
            //after your loop
            Console.WriteLine("Machine: {0}", System.Environment.MachineName);
            string resultFolder = Path.Combine(currentParameters.experimentParameters.resultsPath);

            if (Directory.Exists(resultFolder)) {
                Directory.Delete(resultFolder, true);
            }
            speed = Math.Max(1, currentParameters.experimentParameters.visualizationParameters.delay);
            
            visualizeExperiment = currentParameters.experimentParameters.visualizationParameters.drawVisuals;
            // TODO: Add your initialization logic here 
            results =  new ResultsModel(currentParameters);

            camera.ViewportWidth = graphics.GraphicsDevice.Viewport.Width;
            camera.ViewportHeight = graphics.GraphicsDevice.Viewport.Height;
            graphics.IsFullScreen = true;

            camera.SpriteHeight = this.sizeOfSprites;
            camera.SpriteWidth = this.sizeOfSprites;
            sw.Start();
            spacesw.Start();
            base.Initialize();
        }
        /// <summary>
        /// Updates the camera parameters
        /// </summary>
        private void updateCamera()
        {
            //camera.CenterOn(.currentSpot);

            camera.MapHeight = _gameModel.mapHeight();
            camera.MapWidth = _gameModel.mapWidth();
        }

        Texture2D floorTexture;
        Texture2D wallTexture;
        Texture2D agentTexture;
        Texture2D goalTexture;
        Texture2D heuristicTexture;
        SpriteFont fontTexture;
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            // use this.Content to load your game content here

            spriteBatch = new SpriteBatch(GraphicsDevice);
            floorTexture = CreateTexture(GraphicsDevice, (int)sizeOfSprites, (int)sizeOfSprites, pixel => Color.White);
            //wallTexture = Content.Load<Texture2D>("Wall");
            wallTexture = CreateTexture(GraphicsDevice, sizeOfSprites, sizeOfSprites, pixel => Color.Black);
            agentTexture = CreateTexture(GraphicsDevice, (int)sizeOfSprites, (int)sizeOfSprites, pixel => Color.Green);
            goalTexture = CreateTexture(GraphicsDevice, (int)sizeOfSprites, (int)sizeOfSprites, pixel => Color.Red);
            heuristicTexture = CreateTexture(GraphicsDevice, (int)sizeOfSprites, (int)sizeOfSprites, pixel => Color.LightBlue);
            fontTexture = Content.Load<SpriteFont>("Score"); // Use the name of your sprite font file here instead of 'Score'.

            tileView = new TileView(sizeOfSprites, wallTexture, floorTexture, fontTexture);

            //// TODO: pull out like i did with the search agent.
            agentViewController = new AgentViewController( sizeOfSprites, agentTexture, goalTexture, heuristicTexture, fontTexture);
            scoreView = new ScoreView(fontTexture);

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Handles the users key presses
        /// </summary>
        private void updateKeyPresses()
        {
            KeyboardState newState = Keyboard.GetState();  // get the newest state

            //handle the input
            if (oldState.IsKeyUp(Keys.Right) && newState.IsKeyDown(Keys.Right))
            {
                // do something here
                // this will only be called when the key if first pressed
                _gameModel.nextAgent();
            }
            if (oldState.IsKeyUp(Keys.Left) && newState.IsKeyDown(Keys.Left))
            {
                _gameModel.previousAgent();
            }

            oldState = newState;  // set the new state as the old state for next time
        }
        /// <summary>
        /// Sets the main controller to know that experiment has finished.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void finishedExperiment(object sender, EventArgs e)
        {
            if (pc.hasNextConfiguration())
            {
                currentParameters = pc.getNextConfiguration();
                createGameModel();
            }
            else
            {
                finishedAllExperiments = true;
            }
        }
        private void setAgentsToDraw()
        {
            if (_gameModel.hasAgents())
            {
                agentViewController.setAgent(_gameModel.getAgent());
                agentViewController.setAgents(_gameModel.getAgents());
                hasAgentsToDraw = true;
            }
            else
            {
                hasAgentsToDraw = false;
            }
        }
        private void createGameModel()
        {
            _gameModel = new GameModel(currentParameters, results);
            _gameModel.FinishedExperiments += finishedExperiment;
            _gameModel.update();
            setAgentsToDraw();            
        }
        private bool hasAgentsToDraw = false;
        private int count = 0;
        private Boolean finishedAllExperiments = false;
        private int STARTDELAY = 1;
        private bool firstUpdate = true;
        Stopwatch spacesw = new Stopwatch();
        
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (firstUpdate)
            {
                createGameModel();
                firstUpdate = false;
            }
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }
            bool skip = false;
            if (Keyboard.GetState().IsKeyDown(Keys.Space) && spacesw.ElapsedMilliseconds > 10000)
            {
                skip = true;
                spacesw.Restart();
            }
            if (finishedAllExperiments)
            {
                Exit();
            }
            if (count > 0 && count % speed == 0 && sw.Elapsed.TotalSeconds > STARTDELAY)
            {
                if (!visualizeExperiment)
                {
                    while (!finishedAllExperiments)
                    {
                        _gameModel.update();
                    }
                }
                else
                {
                    _gameModel.update(skip);
                    updateKeyPresses();
                    camera.HandleInput();
                    setAgentsToDraw();
                }
            }
            //agentView.setAgent(_gameModel.getAgent());
            //agentView.agents = _gameModel.getAgents();
            //agentViewController.setAgent(_gameModel.getAgent(), sizeOfSprites, agentTexture, goalTexture, heuristicTexture, fontTexture);
            if (visualizeExperiment)
            {
                updateCamera();
                //agentViewController.setAgents(_gameModel.getAgents());
            }

            count++;
            base.Update(gameTime);
        }
        /// <summary>
        /// Creates a Texture for the game to draw.
        /// </summary>
        /// <param name="device">Graphics device for the system</param>
        /// <param name="width">Width of the texture</param>
        /// <param name="height">height of the texture</param>
        /// <param name="paint">colour of the new texture</param>
        /// <returns></returns>
        public static Texture2D CreateTexture(GraphicsDevice device, int width, int height, Func<int, Color> paint)
        {
            //initialize a texture
            Texture2D texture = new Texture2D(device, width, height);

            //the array holds the color for each pixel in the texture
            Color[] data = new Color[width * height];
            for (int pixel = 0; pixel < data.Length; pixel++)
            {
                //the function applies the color according to the specified pixel
                data[pixel] = paint(pixel);
            }

            //set the color
            texture.SetData(data);

            return texture;
        }
        private int titleBuffer = 50;
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (visualizeExperiment && !firstUpdate)
            {
                GraphicsDevice.Clear(Color.White);
                if (currentParameters.experimentParameters.visualizationParameters.showScores)
                {
                    currentParameters.experimentParameters.visualizationParameters.showName = true;
                }
                if (currentParameters.experimentParameters.visualizationParameters.showName)
                {
                    currentParameters.experimentParameters.visualizationParameters.titleBuffer = titleBuffer;
                    tileView.yOffset = titleBuffer;
                } else
                {
                    currentParameters.experimentParameters.visualizationParameters.titleBuffer = 0;
                    tileView.yOffset = 0;
                }
                // TODO: Add your drawing code here
                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend,
                     null, null, null, null, camera.TranslationMatrix);
                float xScale = (float)graphics.PreferredBackBufferWidth / (sizeOfSprites * (float)_gameModel.mapWidth());
                float yScale = (float)graphics.PreferredBackBufferHeight / (sizeOfSprites * (float)_gameModel.mapHeight() + tileView.yOffset);
                Vector2 nonuniformscale = Vector2.One;
                nonuniformscale.X = xScale;
                nonuniformscale.Y = yScale;
                
                //tileView.draw(spriteBatch, _gameModel, nonuniformscale);
                agentViewController.setVisualizationParameters(currentParameters.experimentParameters.visualizationParameters);
                if (hasAgentsToDraw)
                {
                    agentViewController.draw(spriteBatch, nonuniformscale);
                    if (currentParameters.experimentParameters.visualizationParameters.showName)
                    {
                        scoreView.drawName(spriteBatch, _gameModel.getAgent().name, _gameModel.getAgent().id, _gameModel.getProblemType());
                    }
                }

                if (currentParameters.experimentParameters.visualizationParameters.showScores && hasAgentsToDraw)
                {
                    scoreView.draw(spriteBatch, _gameModel.getAgent().reachedGoal, _gameModel.getAgent().distanceTravelled, _gameModel.getScrubbing());
                }
                spriteBatch.End();
                base.Draw(gameTime);
            }
        }
    }
}
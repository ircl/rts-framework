﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models.Configurations
{
    public class Cutoffs
    {
        public float distance{get;set;}
        public float steps { get; set; }
        public float thinkingTime { get; set; }
        public float timePerMove { get; set; }
        public float goalAchivementTime { get; set; }
    }
    public class VisualizationParameters {
        public int delay { get; set; }
        public bool drawVisuals { get; set; }
        public bool showHeuristic { get; set; }
        public bool showAgentId { get; set; }
        public bool showScores { get; set; }
        public bool showName { get; set; }
        public bool singleExpansionPerUpdate { get; set; }
        public bool hideUnseenNodes { get; set; }
        public bool showStart { get; set; }
        public int titleBuffer { get; set; }
    }
    /// <summary>
    ///  used for loading and populating experiment
    /// </summary>
   public class ExperimentFile
   {
        public Cutoffs cutoffs { get; set; }
        public List<int> numberOfAgents { get; set; }
        public List<string> goalType { get; set; }
        public string resultsPath { get; set; }
        public bool removeAgentsAtGoal { get; set; }
        public bool fourConnected { get; set; }
        public bool useMultipleAlgorithms { get; set; }
        public List<string> algorithms { get; set; }
        public bool blockDiagonalWithCardinalWall { get; set; }
        public VisualizationParameters visualizationParameters { get; set; }
        public float thinkingTimePerMove { get; set; }
        public string wallFile { get; set; }
        public int seed { get; set; }
        public int numberOfRepeats { get; set; }
        public bool outputImages { get; set; }
        public List<string> experimentName { get; set; }
        public List<string> emails { get; set; }
   }
   public class GameParameters
   {
        public string goalType { get; set; }
        public Cutoffs cutoffs { get; set; }
        public int numberOfAgents { get; set; }
        public bool removeAgentsAtGoal { get; set; }
        public bool fourConnected { get; set; }
        public bool blockDiagonalWithCardinalWall { get; set; }
        public VisualizationParameters visualizationParameters { get; set; }
        public float thinkingTimePerMove { get; set; }
        public int seed { get; set; }
        public string resultsPath { get; set; } // what folder to store the results in

        public string experimentName { get; set; } // name of the experiment

   }
   
   public class AlgorithmParameters
    {
        public string name { get; set; }
        public string controller { get; set; }
        public string heuristic { get; set; }
        public float thinkingTime { get; set; }
        public bool singleExpansionPerUpdate { get; set; }
        public Dictionary<string, string> parameters;
    }
   public class AlgorithmFile
    {
        public List<AlgorithmParameters> algorithms { get; set; }
    }
    public class GameMaps
    {
        public string mapfolder { get; set; }
        public int trialsPerMap { get; set; }
    }
    public class WallFile
    {
        public List<int> numberOfWalls { get; set; }
        public float thinkingTime { get; set; }
        public float timeLimit { get; set; }
        public List<string> algorithmFiles { get; set; }
    }

    public class WallParameters
    {
        public AlgorithmParameters algorithm { get; set; }
        public float timeLimit { get; set; }

        public int numberOfWalls { get; set; }
    }
}

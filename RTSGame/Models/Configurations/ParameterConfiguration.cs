﻿using Newtonsoft.Json;
using RTSGame.Models;
using RTSGame.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame
{
 
    public class ParameterConfiguration
    {
        List<ExperimentParameters> experimentConfigurations = new List<ExperimentParameters>();
        public ParameterConfiguration(String experimentPath)
        {
            generateConfigurations(experimentPath);
            //writeParametersToFile();
        }
        ExperimentFile experimentFile;
        WallFile wp;
        List<AlgorithmParameters> wallAlgorithms;
        List<AlgorithmParameters> agentAlgorithms;
        List<String> experimentNames;
        private void generateConfigurations(string experimentPath)
        {
            experimentFile = loadExperiment(experimentPath);
            wp = loadWallParameters(experimentFile.wallFile);
            wallAlgorithms = loadAlgorithms(wp.algorithmFiles);
            agentAlgorithms = loadAlgorithms(experimentFile.algorithms);
            experimentNames = experimentFile.experimentName;
            if(experimentFile.seed <= 0)
            {
                Random random = new Random();
                experimentFile.seed = random.Next();
            }
            //var build = ((AssemblyInformationalVersionAttribute)Assembly
            //  .GetAssembly(typeof(YOURTYPE))
            //  .GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false)[0])
            //  .InformationalVersion;
            string gitCommitId = System.Windows.Forms.Application.ProductVersion.ToString();
            int problemId = 0;
            for (int repeat = 0; repeat < experimentFile.numberOfRepeats; repeat++)
            {
                foreach(String goalType in experimentFile.goalType)
                {
                    for (int i = 0; i < experimentFile.numberOfAgents.Count(); i++)
                    {
                        int numberOfAgents = experimentFile.numberOfAgents[i];
                        for (int j = 0; j < wp.numberOfWalls.Count(); j++)
                        {
                            int numberOfWalls = wp.numberOfWalls[j];
                            foreach (AlgorithmParameters wallAlgorithm in wallAlgorithms)
                            {

                                foreach (String experiment in experimentNames)
                                {
                                    if (experimentFile.useMultipleAlgorithms)
                                    {
                                        List<string> assignments = CreateAlgorithmAssignments(numberOfAgents, agentAlgorithms.Count);
                                        List<AlgorithmParameters> algos = new List<AlgorithmParameters>();
                                        foreach (AlgorithmParameters agentAlgorithm in agentAlgorithms)
                                        {
                                            AlgorithmParameters ap = CreateAlgorithmParameters(agentAlgorithm);
                                            algos.Add(ap);
                                        }
                                        foreach (string assignment in assignments)
                                        {
                                            Dictionary<string, string> mapping = parseParameter(assignment);

                                            List<AlgorithmParameters> aps = new List<AlgorithmParameters>();
                                            foreach (KeyValuePair<string, string> entry in mapping)
                                            {
                                                aps.Add(algos[int.Parse(entry.Value)]);
                                            }
                                            OutputInfo outputInfo = CreateOutputInfo(gitCommitId);
                                            GameParameters experimentParameters = CreateGameParameters(numberOfAgents, repeat, experiment,goalType);
                                            WallParameters MovingObstacleParameters = CreateWallParameters(wallAlgorithm, numberOfWalls);
                                            if (experiment.Contains("json"))
                                            {
                                                GameMaps gm = loadGameMaps(experiment);
                                                string mapFolder = Path.Combine(Directory.GetCurrentDirectory(),
                                                    "Content", "searchExperiments", gm.mapfolder);
                                                string[] maps = Directory.GetFiles(mapFolder, "*.*", SearchOption.AllDirectories);

                                                foreach (string mapName in maps)
                                                {

                                                    ExperimentParameters parameters = new ExperimentParameters()
                                                    {
                                                        repeat = repeat,
                                                        index = experimentConfigurations.Count(),
                                                        algorithmParameters = aps,
                                                        outputInfo = outputInfo,
                                                        experimentParameters = experimentParameters,
                                                        MovingObstacleParameters = MovingObstacleParameters,
                                                        mapParameters = new MapParameters
                                                        {
                                                            usingJsonMap = true,
                                                            mapName = mapName,
                                                            trialsPerMap = gm.trialsPerMap
                                                        }
                                                    };
                                                    experimentConfigurations.Add(parameters);
                                                }
                                            }
                                            else
                                            {
                                                ExperimentParameters parameters = new ExperimentParameters()
                                                {
                                                    repeat = repeat,
                                                    index = experimentConfigurations.Count(),
                                                    algorithmParameters = aps,
                                                    outputInfo = outputInfo,
                                                    experimentParameters = experimentParameters,
                                                    MovingObstacleParameters = MovingObstacleParameters,
                                                    mapParameters = new MapParameters
                                                    {
                                                        usingJsonMap = false
                                                    }
                                                };
                                                experimentConfigurations.Add(parameters);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        OutputInfo outputInfo = CreateOutputInfo(gitCommitId);
                                        GameParameters experimentParameters = CreateGameParameters(numberOfAgents, problemId, experiment, goalType);
                                        WallParameters MovingObstacleParameters = CreateWallParameters(wallAlgorithm, numberOfWalls);
                                        if (experiment.Contains("json"))
                                        {
                                            GameMaps gm = loadGameMaps(experiment);
                                            string mapFolder = Path.Combine(Directory.GetCurrentDirectory(),
                                                "Content", "searchExperiments", gm.mapfolder);
                                            string[] maps = Directory.GetFiles(mapFolder, "*.*", SearchOption.AllDirectories);

                                            foreach (string mapName in maps)
                                            {
                                                foreach (AlgorithmParameters agentAlgorithm in agentAlgorithms)
                                                {
                                                    AlgorithmParameters ap = CreateAlgorithmParameters(agentAlgorithm);
                                                    List<AlgorithmParameters> aps = Enumerable.Repeat(ap, numberOfAgents).ToList();

                                                    ExperimentParameters parameters = new ExperimentParameters()
                                                    {
                                                        repeat = repeat,
                                                        index = experimentConfigurations.Count(),
                                                        algorithmParameters = aps,
                                                        outputInfo = outputInfo,
                                                        problemId = problemId,
                                                        experimentParameters = experimentParameters,
                                                        MovingObstacleParameters = MovingObstacleParameters,
                                                        mapParameters = new MapParameters
                                                        {
                                                            usingJsonMap = true,
                                                            mapName = mapName,
                                                            trialsPerMap = gm.trialsPerMap
                                                        }
                                                    };
                                                    experimentConfigurations.Add(parameters);
                                                }
                                                problemId++;
                                            }
                                        }
                                        else
                                        {
                                            foreach (AlgorithmParameters agentAlgorithm in agentAlgorithms)
                                            {
                                                AlgorithmParameters ap = CreateAlgorithmParameters(agentAlgorithm);
                                                List<AlgorithmParameters> aps = Enumerable.Repeat(ap, numberOfAgents).ToList();
                                                ExperimentParameters parameters = new ExperimentParameters()
                                                {
                                                    repeat = repeat,
                                                    index = experimentConfigurations.Count(),
                                                    algorithmParameters = aps,
                                                    outputInfo = outputInfo,
                                                    problemId = problemId,
                                                    experimentParameters = experimentParameters,
                                                    MovingObstacleParameters = MovingObstacleParameters,
                                                    mapParameters = new MapParameters
                                                    {
                                                        usingJsonMap = false
                                                    }
                                                };
                                                experimentConfigurations.Add(parameters);
                                            }
                                            problemId++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private List<string> CreateAlgorithmAssignments(int numberOfAgents, int numberOfAlgorithms)
        {
            List<List<int>> assignements = new List<List<int>>();
            ParameterMatrix pmatrix = new ParameterMatrix();
            for (int i = 0; i < numberOfAgents; i++)
            {
                List<string> op = new List<string>();
                for (int j = 0; j < numberOfAlgorithms; j++)
                {
                    op.Add(j.ToString());
                }
                pmatrix.addParameter(i.ToString(), op);
            }
            List<string> variations = pmatrix.createCombinations();
            return variations;
        }
        private AlgorithmParameters CreateAlgorithmParameters(AlgorithmParameters agentAlgorithm)
        {
            return new AlgorithmParameters()
            {
                heuristic = agentAlgorithm.heuristic,
                name = agentAlgorithm.name,
                controller = agentAlgorithm.controller,
                singleExpansionPerUpdate= experimentFile.visualizationParameters.singleExpansionPerUpdate,
                thinkingTime = experimentFile.cutoffs.timePerMove,
                parameters = agentAlgorithm.parameters
            };
        }
        private OutputInfo CreateOutputInfo(string gitCommitId)
        {
            return new OutputInfo
            {
                gitCommit = gitCommitId,

                machineName = Environment.MachineName,
                copyConfigFile = false,
                useHeader = true,
                experimentIndex = 0,
                loadedExperimentFile = experimentFile,
                agentAlgorithms = agentAlgorithms,
                wallAlgorithms = wallAlgorithms,
                wallFile = wp,
                emails = experimentFile.emails
            };
        }
        private GameParameters CreateGameParameters(int numberOfAgents, int repeat, string experiment, string goalType)
        {
            return new GameParameters
            {
                cutoffs = experimentFile.cutoffs,
                blockDiagonalWithCardinalWall = experimentFile.blockDiagonalWithCardinalWall,
                fourConnected = experimentFile.fourConnected,
                removeAgentsAtGoal = experimentFile.removeAgentsAtGoal,
                goalType = goalType,
                resultsPath = experimentFile.resultsPath,
                numberOfAgents = numberOfAgents,
                seed = experimentFile.seed + repeat,
                thinkingTimePerMove = experimentFile.thinkingTimePerMove,
                visualizationParameters = experimentFile.visualizationParameters,
                experimentName = experiment
            };
        }
        private WallParameters CreateWallParameters(AlgorithmParameters wallAlgorithm, int numberOfWalls)
        {
            return  new WallParameters
            {
                algorithm = new AlgorithmParameters()
                {
                    parameters = wallAlgorithm.parameters,
                    controller = wallAlgorithm.controller,
                    heuristic = wallAlgorithm.heuristic,
                    thinkingTime = wp.thinkingTime,
                    name = wallAlgorithm.name
                },
                timeLimit = wp.timeLimit,
                numberOfWalls = numberOfWalls
            };
        }
        
        private ExperimentFile loadExperiment(string path)
        {
            path = Path.Combine(Directory.GetCurrentDirectory(), "Configurations", path);
            string info = File.ReadAllText(path);
            ExperimentFile experiment = JsonConvert.DeserializeObject<ExperimentFile>(info);
            return experiment;
        }
        private GameMaps loadGameMaps(string path)
        {
            path = Path.Combine(Directory.GetCurrentDirectory(), "Content","searchExperiments","experiments", path);
            string info = File.ReadAllText(path);
            GameMaps experiment = JsonConvert.DeserializeObject<GameMaps>(info);
            return experiment;
        }
        private WallFile loadWallParameters(string path) {
            path = Path.Combine(Directory.GetCurrentDirectory(), "Configurations", path);
            string info = File.ReadAllText(path);
            WallFile wallParameters = JsonConvert.DeserializeObject < WallFile>(info);
            return wallParameters;
        }
        private void addParameter(ParameterMatrix prmMatrix, string key, string value)
        {
            List<String> parameters = value.Split(',').ToList();
            prmMatrix.addParameter(key, parameters);
        }
       
        private List<AlgorithmParameters> loadAlgorithms(List<String> algorithmPaths)
        {
            List<AlgorithmParameters> combinations = new List<AlgorithmParameters>();
            foreach (string algorithmFilePathRaw in algorithmPaths)
            {
                string algorithmFilePath = algorithmFilePathRaw.Trim();
                string path = Path.Combine(Directory.GetCurrentDirectory(), "Configurations", algorithmFilePath);
                string info = File.ReadAllText(path);
                AlgorithmFile algoFile = JsonConvert.DeserializeObject<AlgorithmFile>(info);
                foreach (AlgorithmParameters algorithmParameters in algoFile.algorithms)
                {
                    string heuristicType = algorithmParameters.heuristic;
                    string name = algorithmParameters.name;
                    if (algorithmParameters.parameters.Count == 0)
                    {
                        combinations.Add(new AlgorithmParameters()
                        {
                            heuristic = heuristicType,
                            name = name,
                            controller=algorithmParameters.controller,
                            parameters = new Dictionary<string, string>()
                        });
                    }
                    else
                    {
                        ParameterMatrix pmatrix = new ParameterMatrix();
                        foreach (KeyValuePair<string, string> entry in algorithmParameters.parameters)
                        {
                            addParameter(pmatrix, entry.Key, entry.Value);
                        }
                        List<string> variations = pmatrix.createCombinations();

                        foreach (string variation in variations)
                        {

                            Dictionary<string, string> parsedParam = parseParameter(variation);
                            int lookahead;
                            if (parsedParam.ContainsKey("lookahead"))
                            {
                                lookahead = int.Parse(parsedParam["lookahead"]);
                            }
                            else
                            {
                                lookahead = 0;
                            }
                            combinations.Add(new AlgorithmParameters()
                            {
                                heuristic = heuristicType,
                                name = name,
                                controller = algorithmParameters.controller,

                                parameters = parsedParam
                            });
                        }
                    }
                }
            }
            return combinations;
        }
        
        int configI = 0;
        private Dictionary<string,string> parseParameter(String strParam)
        {
            var dict = strParam.Split(',')
                .Select(s => s.Split(':'))
                .ToDictionary(
                    p => p[0].Trim()
                , p => p[1].Trim()
                );
            return dict;
        }
        public bool hasNextConfiguration()
        {
            return experimentConfigurations.Count > configI + 1;
        }
        public ExperimentParameters getCurrentParameters()
        {
            return experimentConfigurations[configI];
        }
        public ExperimentParameters getNextConfiguration()
        {
            configI++;
            ExperimentParameters parameters = experimentConfigurations[configI];
            return parameters;
        }
        public List<ExperimentParameters> getParameters()
        {
            return experimentConfigurations;
        }
        public struct OutputInfo
        {
            public string gitCommit { get; set; }
            public string machineName { get; set; }
            public bool useHeader { get; set; }
            public bool copyConfigFile { get; set; }
            public ExperimentFile loadedExperimentFile { get; set; }
            public WallFile wallFile { get; set; }
            public List<String> emails { get;set;}
            public List<AlgorithmParameters> agentAlgorithms { get; set; }
            public List<AlgorithmParameters> wallAlgorithms { get; set; }
            public int experimentIndex { get; set; }
        }
        public struct ExperimentParameters {
            //public VisualizationParameters visualizationParameters { get; set; }
            public int repeat { get; set; }
            public int index { get; set; }
            public int problemId { get; set; }
            public GameParameters experimentParameters { get; set; }
            public WallParameters MovingObstacleParameters { get; set; }
            public List<AlgorithmParameters> algorithmParameters { get; set; }
            public MapParameters mapParameters { get; set; }
            public OutputInfo outputInfo { get; set; }
        }  
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models.Configurations
{
    // Definition of a parameter
    /// <summary>
    /// Parameter matrix
    /// </summary>
    public class ParameterMatrix
    {
        List<string> results = new List<string>();
        /// <summary>
        /// Parameter in matrix
        /// </summary>
        private struct Parameter
        {
            /// <summary>
            /// Name of parameter
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Possible values for the parameter
            /// </summary>
            public List<string> PossibleValues { get; set; }
            /// <summary>
            /// Creates a parameter
            /// </summary>
            /// <param name="name">name of parameter</param>
            /// <param name="values">values of the parameter</param>
            public Parameter(string name, List<string> values)
            {
                Name = name;
                PossibleValues = values;
            }
        }
        List<Parameter> parameters = new List<Parameter>();
        /// <summary>
        /// Empty constructor
        /// </summary>
        public ParameterMatrix()
        {
        }
        /// <summary>
        /// add parameter to the matrix
        /// </summary>
        /// <param name="name">name of parameter</param>
        /// <param name="values">list of options</param>
        public void addParameter(string name, List<string> values)
        {
            Parameter parameter = new Parameter(name, values);
            parameters.Add(parameter);
        }
        private void createCombinations(string built, int depth)
        {
            if (depth >= parameters.Count())
            {

                built = built.Substring(0, built.Length - 1);

                results.Add(built);
                return;
            }

            Parameter next = parameters[depth];
            built += "" + next.Name + ":";
            foreach (var option in next.PossibleValues)
                createCombinations(built + option + ",", depth + 1);
        }
        /// <summary>
        /// Create combinations of parameters
        /// </summary>
        /// <returns>list of combinations</returns>
        public List<String> createCombinations()
        {
            this.createCombinations("", 0);
            return results;
        }
    }
}

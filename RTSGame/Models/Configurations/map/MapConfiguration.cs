﻿using RTSGame.Models.MapModels.MapGenerators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models.Configurations
{
    class MapConfigurationController
    {
       
    }
    public class MapParameters
    {

        public string mapName { get; set; }
        public string parameters { get; set; }
        public bool usingJsonMap { get; set; }
        public int trialsPerMap { get; set; }
    }
}

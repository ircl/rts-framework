﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models.DeadlockReslovers
{
    public class DeadlockController
    {
        public DeadlockController()
        {

        }
        bool CheckForDeadlock(SearchAgent agent)
        {
            Tile moveToTile = agent.getNextNode();
            if (moveToTile.isOccupied() &&
                !moveToTile.isWall() &&
                !moveToTile.GetAgent().atGoal())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        internal bool CheckCircularDeadlock(HashSet<SearchAgent> deadlockedAgents, SearchAgent deadlockAgent)
        {
            if (CheckForDeadlock(deadlockAgent))
            {
                Tile nextNode = deadlockAgent.getNextNode();
                SearchAgent nextInDeadLock = nextNode.GetAgent();
                if (nextInDeadLock == deadlockAgent) return false;
                if (deadlockedAgents.Contains(nextInDeadLock))
                {
                    return true; // found circular refrence (also base case for recurission);
                }
                else
                {
                    deadlockedAgents.Add(nextInDeadLock);
                    deadlockAgent = nextInDeadLock;
                    return CheckCircularDeadlock(deadlockedAgents, deadlockAgent);
                }
            }
            else
            {
                return false; // not a deadlock
            }
        }
        public HashSet<SearchAgent> GetDeadLockedAgents(SearchAgent agent)
        {
            HashSet<SearchAgent> deadlockedAgents = new HashSet<SearchAgent>()
            {
                agent
            };
            bool deadLocked = CheckCircularDeadlock(deadlockedAgents, agent);
            if (deadLocked)
            {
                return deadlockedAgents;
            }
            else
            {
                return new HashSet<SearchAgent>();
            }
        }
        /// <summary>
        /// Checks if the agent is in a dead lock
        /// </summary>
        /// <param name="agent">agent in question</param>
        /// <returns></returns>
        public bool IsDeadLocked(SearchAgent agent)
        {
            HashSet<SearchAgent> deadlockedAgents = new HashSet<SearchAgent>()
            {
                agent
            };
            bool deadLocked = CheckCircularDeadlock(deadlockedAgents, agent);
            return deadLocked;
        }
        /// <summary>
        /// Returns true if the agent is no longer in a dead lock
        /// </summary>
        /// <param name="agent">in the dead lock</param>
        /// <returns>whether the agent is in a dead lock at the end of the function</returns>
        public bool ResolveDeadlock(SearchAgent agent, int timeStep)
        {
            HashSet<SearchAgent> deadLockedAgents = GetDeadLockedAgents(agent);
            List<SearchAgent> agentsWithOpenSpots = deadLockedAgents.Where(d => d.currentSpot.getNeighbours(false).Any(t => t.isAvailable())).ToList();
            if (agentsWithOpenSpots.Count == 0)
            {
                return false;
            }
            int mostPathCount = agentsWithOpenSpots.Max(t=>t.currentSpot.GetPathCount());
            SearchAgent mostBlockingAgent = agentsWithOpenSpots.First(t => t.currentSpot.GetPathCount() == mostPathCount);
            bool ableToMoveOffPath = mostBlockingAgent.moveOffPath(timeStep);
            if (ableToMoveOffPath)
            {
                return IsDeadLocked(agent);
            }
            else
            {
                return false;
            }
        }
    }
}

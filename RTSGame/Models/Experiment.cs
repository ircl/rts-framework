﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models
{
    /// <summary>
    /// Experiment that will be run
    /// </summary>
    public class Experiment
    {
        /// <summary>
        /// List of Scenario configurations
        /// </summary>
        public List<ScenarioConfig> scenarios = new List<ScenarioConfig>();
        /// <summary>
        /// Adds a scenario to the list
        /// </summary>
        /// <param name="newScenario">new scenario to add</param>
        public void addScenario(ScenarioConfig newScenario)
        {
            scenarios.Add(newScenario);
        }
    }
}

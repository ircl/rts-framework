﻿using CsvHelper;
using RTSGame.Models.SearchData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.Models.SearchModels.AgentController;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.ExperimentModels
{
    //public delegate void ChangedEventHandler(object sender, EventArgs e);
    /// <summary>
    /// Game model for the experiment. Handles what problems to run and how to record results
    /// </summary>
    class GameModel
    {
        private ExperimentGoalController goalController;
        private LevelModel levelModel;
        private Utilities utils = new Utilities();
        private ExperimentParameters parameters;
        public delegate void ChangedEventHandler(object sender, EventArgs e);
        public event ChangedEventHandler FinishedExperiments;
        private Dictionary<string, GridMap> mapCache = new Dictionary<string, GridMap>();
        /// <summary>
        /// State variables for the experiment
        /// </summary>
        private struct State
        {
            public int numberOfAgents { get; set; }
            public int visualizedAgent { get; set; }
            public string experimentPath { get; set; }
            public string mapName { get; set; }
            public int selectedAgent { get; set; }
            public List<Problem> currentProblems { get; set; }
        }
        private State state = new State();
        private ResultsModel results;

        /// <summary>
        /// Initialize the experiment, and starts the agents on the first problem for the first scenario.
        /// </summary>
        public GameModel(ExperimentParameters parameters, ResultsModel resultsModel)
        {
            this.parameters = parameters;
            goalController = new ExperimentGoalController(parameters);
            results = resultsModel;
            state.numberOfAgents = parameters.experimentParameters.numberOfAgents;
            state.experimentPath = parameters.experimentParameters.experimentName;
            state.selectedAgent = 0;
            createNewLevel();
        }
        /// <summary>
        /// Listener function for the event all agents at goal for the current level.
        /// </summary>
        private void addResultsAndWriteToFile()
        {
            List<AgentInfo> agentInfo = levelModel.getAgentStats();
            for (int i = 0; i < agentInfo.Count(); i++)
            {
                if (i == 0)
                {
                    //Console.WriteLine(agentInfo[i].name + " finished searching");
                }
                agentFinishedSearching(agentInfo[i]);
            }
            //results.writeProblemToCSV();
        }
        private int seedLevelCount = 0;
        private void createNewLevel()
        {
            state.mapName = goalController.GetMapName();
            ++seedLevelCount;
            if (mapCache.ContainsKey(state.mapName))
            {
                GridMap cachedMap = mapCache[state.mapName];
                cachedMap.resetTiles();

                levelModel = new LevelModel(state.mapName, this.parameters,
                           this.parameters.experimentParameters.seed + (seedLevelCount),
                           cachedMap
                );
            } else
            {

                levelModel = new LevelModel(state.mapName, this.parameters,
                           this.parameters.experimentParameters.seed + (seedLevelCount)
                );
                if (!state.mapName.ToLower().Contains("json"))
                {
                    mapCache.Add(state.mapName, levelModel.mapModel);
                }
            }

        }
        /// <summary>
        /// Updates the current level model.
        /// </summary>
        public void update(bool moveOn = false)
        {
            if (hasFinishedExperiment)
            {
                return;
            }
            if (!levelModel.startedProblems)
            {
                levelModel.startProblems(getNextProblem(0));
            }
            levelModel.update();
            if (levelModel.allAgentsFinishedSearching() || moveOn)
            {
                updateGameState();
            }
        }
        public void runToCompletion()
        {
            while (!hasFinishedExperiment)
            {
                if (!levelModel.startedProblems)
                {
                    levelModel.startProblems(getNextProblem(0));
                }
                levelModel.update();
                if (levelModel.allAgentsFinishedSearching())
                {
                    updateGameState();
                }
                
            }
            return;
        }
        private bool hasFinishedExperiment = false;
        /// <summary>
        /// Updates internal game state and level model accordingly.
        /// </summary>
        private void updateGameState()
        {
            addResultsAndWriteToFile();
            if (goalController.HasNextProblem())
            {
                goalController.IncrementProblemNumber();
                createNewLevel();
            }
            else
            {
                if (goalController.HasNextScenario()) {
                    goalController.MoveToNextScenario();
                    
                    //Console.WriteLine(" Scenario ({0} / {1})", goalController.currentScenarioNumber(), goalController.numberOfScenarios);
                    createNewLevel();
                }
                else
                {
                    hasFinishedExperiment = true;
                    finishedExperiment();
                }
            }
        }
        /// <summary>
        /// Gets the current maps height
        /// </summary>
        /// <returns>the map height</returns>
        public int mapHeight()
        {
            return levelModel.mapHeight();
        }
        /// <summary>
        /// Get the cureent maps width
        /// </summary>
        /// <returns>the maps width</returns>
        public int mapWidth()
        {
            return levelModel.mapWidth();
        }
        /// <summary>
        /// Handles the actions taken when an agent finishes searching.
        /// </summary>
        /// <param name="e">Event args for the agent who reached the goal</param>
        private void agentFinishedSearching(AgentInfo e)
        {
            float scrubbing = utils.CalculateScrubbing(e.statesVisited);
            float subopt = (float)e.distance / state.currentProblems[e.id].Optimal_Travel_Cost;
            results.AddProblemResult(
                state.mapName,
                goalController.CurrentProblemNumber(),
                goalController.CurrentScenarioNumber(),
                e,
                scrubbing,
                subopt,
                state.numberOfAgents,
                levelModel.parameters.MovingObstacleParameters.numberOfWalls,
                levelModel.parameters.MovingObstacleParameters.algorithm.name,
                parameters.index,
                parameters.repeat,
                parameters.experimentParameters.goalType,
                parameters.problemId
                );
            //Console.WriteLine("Agent {0} reached the goal with a subopt of {1:0.00} and a scrubbing of {2:0.00}", id, subopt, scrubbing);
        }

        internal bool hasAgents()
        {
            return levelModel.hasAgents();
        }

        /// <summary>
        /// Gets the next problem
        /// </summary>
        /// <param name="step">how far the current problme should be index, currently either 0 steps or 1.</param>
        /// <returns>The next problem</returns>
        private List<Problem> getNextProblem(int step)
        {
            List<Problem> problems;
            if (step == 0)
            {
                problems = goalController.GetCurrentProblems();
            } else
            {
                problems = goalController.GetNextProblem();
            }
            if (problems.Count < state.numberOfAgents)
            {
                goalController.FillProblems(problems, state.numberOfAgents, levelModel.getEmptySpaces());
            }
            state.currentProblems = problems;
            return problems;
        }
        /// <summary>
        /// Increments the selected agent to the next  agent
        /// </summary>
        public void nextAgent() {
            if (state.selectedAgent + 1 >= state.numberOfAgents)
            {
                state.selectedAgent = 0;
            } else
            {
                state.selectedAgent++;
            }
        }
        /// <summary>
        /// Decrements the selected agent to the previous one.
        /// </summary>
        public void previousAgent() {
            if (state.selectedAgent > 0)
            {
                state.selectedAgent--;
            }
            else
            {
                state.selectedAgent = state.numberOfAgents - 1;
            }
        }
        /// <summary>
        /// Get the current selected agent.
        /// </summary>
        /// <returns>The selected agent.</returns>
        public SearchAgent getAgent()
        {
            //if ("drawAfterEachMove"))
            //{
            //    return levelModel.getLastUpdatedAgent();
            //}
            //else
            //{
                return levelModel.getAgent(state.selectedAgent);

            //}
        }
        /// <summary>
        /// Gets all of the agents.
        /// </summary>
        /// <returns>All the agents solving the current problem</returns>
        public List<SearchAgent> getAgents()
        {
            return levelModel.getAgents();
        }
        /// <summary>
        /// Gets the current srcubbing of the agent.
        /// </summary>
        /// <returns></returns>
        public float getScrubbing()
        {
            return utils.CalculateScrubbing(levelModel.getAgent(state.selectedAgent).statesVisited);
        }
        /// <summary>
        /// Gets the tile at a given location.
        /// </summary>
        /// <param name="row">Tile row.</param>
        /// <param name="col">Tile column</param>
        /// <returns>Tile at given row and column.</returns>
        public Tile getTileAt(int row, int col)
        {
            return levelModel.getTileAt(row, col);
        }

        /// <summary>
        /// Wraps up anything required when the experiment finishes and records the result to a CSV.
        /// </summary>
        private void finishedExperiment()
        {
            finishedExperimentTrigger(EventArgs.Empty);

        }
        /// <summary>
        /// Throws the finished experiment event.
        /// </summary>
        /// <param name="e">null event parameter</param>
        protected virtual void finishedExperimentTrigger(EventArgs e)
        {
            if (FinishedExperiments != null)
            {
                FinishedExperiments(this, e);
            }
        }

        internal string getProblemType()
        {
            return this.parameters.experimentParameters.goalType;
        }
    }
}

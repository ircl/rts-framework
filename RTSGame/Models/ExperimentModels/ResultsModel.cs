﻿using Newtonsoft.Json;
using RTSGame.Models.SearchData;
using RTSGame.Models.SearchModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static RTSGame.Models.SearchModels.AgentController;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.ExperimentModels
{
    /// <summary>
    /// Records the results
    /// </summary>
    class ResultsModel
    {
        /// <summary>
        /// Struct for recording a problems result for a single agent.
        /// </summary>
        public struct ProblemResult
        {
            public int Scenario_num { get; set; }
            public int Problem_num { get; set; }
            public float Subopt { get; set; }
            public float Scrubbing { get; set; }
            public string MapName { get; set; }
            public AgentInfo AgentInfo { get; set; }
            public int NumberOfAgents { get; set; }
            public int NumberOfWalls { get; set; }
            public string WallType { get; set; }
            public int index { get; set; }
            public int repeat { get; set; }
            public string ProblemType { get; set; }
            public int experimentId { get; set; }
            /// <summary>
            /// Creates the problems result
            /// </summary>
            /// <param name="mapName">The name of the map the agent was on</param>
            /// <param name="scenario_num">results scenario number</param>
            /// <param name="problem_num">results problem number</param>
            /// <param name="subopt">results suboptimality</param>
            /// <param name="scrubbing">results scrubbing</param>
            /// <param name="agentInfo">info about the agent</param>
            /// <param name="numberOfAgents">number of agents</param>
            public ProblemResult(string mapName, int scenario_num, int problem_num,
                float subopt, float scrubbing, AgentInfo agentInfo, int numberOfAgents, int numberOfWalls, string wallType,
                int index, int repeat, string problemType, int experimentId)
            {
                this.MapName = mapName;
                this.Scenario_num = scenario_num;
                this.Problem_num = problem_num;
                this.Scrubbing = scrubbing;
                this.Subopt = subopt;
                this.AgentInfo = agentInfo;
                this.NumberOfAgents = numberOfAgents;
                this.NumberOfWalls = numberOfWalls;
                this.WallType = wallType;
                this.index = index;
                this.ProblemType = problemType;
                this.repeat = repeat;
                this.experimentId = experimentId;
            }
        }
        ///// <summary>
        ///// Record of a single agents results.
        ///// </summary>
        //public struct AgentResult
        //{
        //    public int agentId { get; set; }
        //    public List<ProblemResult> problemResults;
        //    /// <summary>
        //    /// Creates a record of the agents results.
        //    /// </summary>
        //    /// <param name="id"></param>
        //    public AgentResult(int id)
        //    {
        //        agentId = id;
        //        problemResults = new List<ProblemResult>();
        //    }
        //}
        //private Dictionary<int, AgentResult> results = new Dictionary<int, AgentResult>();
        private string resultFilePath;
        private ExperimentParameters parameters;
        private ReaderWriterLockSlim lock_ = new ReaderWriterLockSlim();
        //TextWriter resultFileWriter;
        /// <summary>
        /// Empty result model initialization.
        /// </summary>
        public ResultsModel(ExperimentParameters parameters)
        {
            this.parameters = parameters;
            //after your loop
            this.resultFilePath = Path.Combine(parameters.experimentParameters.resultsPath, "results.txt");
            WriteHeaderAndFile();
        }
        int index = 0;
        //TextWriter resultFileWriter;
        /// <summary>
        /// Empty result model initialization.
        /// </summary>
        public ResultsModel(ExperimentParameters parameters, int index)
        {
            this.index = index;
            this.parameters = parameters;
            //after your loop
            this.resultFilePath = Path.Combine(parameters.experimentParameters.resultsPath, "results_"+ index.ToString()+".csv");
            WriteHeaderAndFile();

        }
        private void WriteHeaderAndFile() { 

            if (!File.Exists(resultFilePath))
            {
                new FileInfo(resultFilePath).Directory.Create();
                //var csv = new StringBuilder();
                String header = "AgentID," +
                    "MapName," +
                    "Index," +
                    "ScenarioNumber," +
                    "ProblemNumber," +
                    "ExperimentId," + 
                    "StartX," +
                    "StartY," +
                    "LastX," +
                    "LastY, " +
                    "GoalX," +
                    "GoalY," +
                    "Subopt," +
                    "Scrub," +
                    "GoalAchivementTime," +
                    "ThinkingTime," +
                    "MaxThinkingTime," +
                    "AverageThinkningTime,"+
                    "FirstMoveTime," +
                    "NumberOfStatesTouched,"+
                    "NumberOfStatesExpanded," +
                    "Distance," +
                    "SimulationTicks," +
                    "NumberOfSteps," +
                    "Collision," +
                    "ReachedDistanceCutoff," +
                    "ReachedStepCutoff," +
                    "FinishedInGoal," +
                    "NumberOfMovingWalls," +
                    "MovingWallType," +
                    "NumberOfAgents," +
                    "AgentType," +
                    "Lookahead," +
                    "Heuristic," + 
                    "ProblemType";
                //csv.AppendLine(header);
                //OutFile = File.Open(resultFilePath, FileMode.OpenOrCreate);
                //File.WriteAllText(resultFilePath, csv.ToString());
                if (parameters.outputInfo.copyConfigFile)
                {
                    string configPath = Path.Combine(parameters.experimentParameters.resultsPath, "config.json");
                    String test = JsonConvert.SerializeObject(parameters.outputInfo, Formatting.Indented);
                    File.WriteAllText(configPath, test);
                }

                //resultFileWriter = TextWriter.Synchronized(new StreamWriter(resultFilePath));
                //resultFileWriter.WriteLine(header);
                //resultFileWriter.Flush();
                if (parameters.outputInfo.useHeader)
                {
                    WriteStringToResFile(header);
                }
                // Get the current configuration file.
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                // Save the configuration file.
                //config.Save(ConfigurationSaveMode.Full);
                // To copy a file to another location and 
                // overwrite the destination file if it already exists.
                //System.IO.File.Copy(config.FilePath, configFilePath, true);
            }
            //results[0] = new AgentResult(0);

            //File.WriteAllText(configFilePath, configFile);
        }
        /// <summary>
        /// Adds the problem result to the specified agents file. if this is the first result for the agent a record is created.
        /// </summary>
        /// <param name="problem">id for the problem</param>
        /// <param name="scenario">id for the scenario</param>
        /// <param name="scrubbing">scrubbing for the problem</param>
        /// <param name="subopt">suboptimality for the problem</param>
        /// <param name="e">Info for the agent solving the problem</param>
        /// <param name="mapName">map the agent operated on</param>
        /// <param name="numberOfAgents">number of agents</param>
        public void AddProblemResult(string mapName, int problem, int scenario, AgentInfo e, float subopt, float scrubbing,
            int numberOfAgents, int numberOfWall, string wallType, int index, int repeat, string goalType, int problemId)
        {

            //if (!results.ContainsKey(e.id))
            //{
            //    results[e.id] = new AgentResult(e.id);
            //}
            ProblemResult pr = new ProblemResult(mapName, scenario, problem, scrubbing, subopt, e, numberOfAgents, numberOfWall, wallType,
                index, repeat, goalType, problemId);
            WriteProblemToCSV(pr);
            //results[e.id].problemResults.Add(pr);
        }
        ///// <summary>
        ///// Returns the results for the agent
        ///// </summary>
        ///// <param name="agentId">id of the agent in question.</param>
        ///// <returns>the agents results so far.</returns>
        //public AgentResult getResultsForAgent(int agentId)
        //{
        //    return results[agentId];
        //}
        private void WriteProblemToCSV(ProblemResult pr)
        {
            String resultString = FormatProblem(pr);
            WriteStringToResFile(resultString);
            //resultFileWriter.WriteLine(resultString);
            //resultFileWriter.WriteLineAsync(resultString);


        }
        private void WriteStringToResFile(string resultString)
        {
            lock_.EnterWriteLock();
            try
            {
                //String resultString = formatProblem(pr);
                //TextWriter.Synchronized(resultFileWriter).WriteLine(resultString);
                File.AppendAllText(resultFilePath, resultString + Environment.NewLine);
            }
            finally
            {
                lock_.ExitWriteLock();
            }
        }
        private string FormatProblem(ProblemResult problemResult)
        {
            string resultString = problemResult.AgentInfo.id.ToString();
            resultString += ("," + problemResult.MapName);
            resultString += ("," + problemResult.index);
            resultString += ("," + problemResult.Scenario_num);
            resultString += ("," + problemResult.Problem_num);
            resultString += ("," + problemResult.experimentId);
            resultString += ("," + problemResult.AgentInfo.startX);
            resultString += ("," + problemResult.AgentInfo.startY);
            resultString += ("," + problemResult.AgentInfo.endX);
            resultString += ("," + problemResult.AgentInfo.endY);

            resultString += ("," + problemResult.AgentInfo.goalX);
            resultString += ("," + problemResult.AgentInfo.goalY);

            resultString += ("," + problemResult.Subopt);
            resultString += ("," + problemResult.Scrubbing);
            resultString += ("," + problemResult.AgentInfo.gat);
            resultString += ("," + problemResult.AgentInfo.thinkingTime);
            resultString += ("," + problemResult.AgentInfo.maxThinkingTime);
            resultString += ("," + problemResult.AgentInfo.averageThinkingTime);
            resultString += ("," + problemResult.AgentInfo.initialMoveCost);
            resultString += ("," + problemResult.AgentInfo.numberOfTouchedStates);
            resultString += ("," + problemResult.AgentInfo.numberOfExpandedStates);

            resultString += ("," + problemResult.AgentInfo.distance);
            resultString += ("," + problemResult.AgentInfo.numberOfStepsUntilAllAgentsReachedGoal);
            resultString += ("," + problemResult.AgentInfo.numberOfStepsUntilAgentReachedGoal);
            resultString += ("," + problemResult.AgentInfo.collisions);

            resultString += ("," + problemResult.AgentInfo.reachedDistanceCutoff);
            resultString += ("," + problemResult.AgentInfo.reachedStepCutoff);
            resultString += ("," + problemResult.AgentInfo.atGoal);
            resultString += ("," + problemResult.NumberOfWalls);
            resultString += ("," + problemResult.WallType);

            resultString += ("," + problemResult.NumberOfAgents);

            resultString += ("," + problemResult.AgentInfo.name);


            resultString += ("," + problemResult.AgentInfo.lookAhead);
            resultString += ("," + problemResult.AgentInfo.heuristic);
            resultString += ("," + problemResult.ProblemType);

            return resultString;
        }
       
    }
}

﻿using CsvHelper;
using RTSGame.Models.GoalModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.ExperimentModels
{
    /// <summary>
    /// Controller that creates the goals. Needs refactoring
    /// </summary>
    class ExperimentGoalController
    {
        Utilities utils = new Utilities();
        ProblemGenerator ProblemGenerator;
        ExperimentParameters parameters;
        /// <summary>
        /// Empty constructor
        /// </summary>
        public ExperimentGoalController(ExperimentParameters parameters)
        {
            this.parameters = parameters;
            ProblemGenerator = new ProblemGenerator(parameters);
            string path = Path.Combine(Directory.GetCurrentDirectory(), "Content", "searchExperiments");
            if (parameters.mapParameters.usingJsonMap)
            {
                state.CurrentExperiment = LoadMapExperiment();
            } else
            {
                state.CurrentExperiment = LoadExperiment(path, parameters.experimentParameters.experimentName);
            }
            state.CurrentScenarioNumber = 0;
            state.CurrentProblemNumber = 0;
            SetScenario(state.CurrentScenarioNumber);
            numberOfScenarios = state.CurrentExperiment.scenarios.Count;
        }

        private State state = new State();
        private int silverMapCount = 0;
        /// <summary>
        /// State of the experiment
        /// </summary>
        private struct State
        {
            public Experiment CurrentExperiment { get; set; }
            public int CurrentProblemNumber { get; set; }
            public int CurrentScenarioNumber { get; set; } // number of repeats for each map
            public List<int> ProblemIndexes { get; set; }
        }
        public int numberOfScenarios;
  

        
        
        /// <summary>
        /// Checks if there are any more problems for the current scenario
        /// </summary>
        /// <returns>Return true if there are more problems</returns>
        public Boolean HasNextProblem()
        {
            return state.CurrentProblemNumber + 1 < state.ProblemIndexes.Count;
        }
        /// <summary>
        /// Checks if there are any more scenarios.
        /// </summary>
        /// <returns> Returns true if there are more scenarios.</returns>
        public Boolean HasNextScenario()
        {
            return state.CurrentScenarioNumber + 1 < state.CurrentExperiment.scenarios.Count();
        }
        /// <summary>
        /// Increments the current state to the next scenaroi. Throws out of bounds error if there is none.
        /// </summary>
        public void MoveToNextScenario()
        {
            state.CurrentScenarioNumber++;
            state.CurrentProblemNumber = 0;
            SetScenario(state.CurrentScenarioNumber);
        }
        /// <summary>
        /// Get the list of problems for the current scenario and problem index.
        /// </summary>
        /// <returns>List of problems for the current scenario.</returns>
        public List<Problem> GetCurrentProblems()
        {
            List<Problem> predefinedProblem = state.CurrentExperiment.scenarios[state.CurrentScenarioNumber]
                .problems.Where(p => p.Problem_number == state.ProblemIndexes[state.CurrentProblemNumber]).ToList();
            return predefinedProblem;
        }
        /// <summary>
        /// Gets the next problem in the scenario.
        /// </summary>
        /// <returns></returns>
        public List<Problem> GetNextProblem()
        {
            state.CurrentProblemNumber++;
            List<Problem> predefinedProblem = state.CurrentExperiment.scenarios[state.CurrentScenarioNumber]
                .problems.Where(p => p.Problem_number == state.ProblemIndexes[state.CurrentProblemNumber]).ToList();
            return predefinedProblem;
        }
        public void IncrementProblemNumber()
        {
            state.CurrentProblemNumber++;
        }
        /// <summary>
        /// Sets the scenario to specified index
        /// </summary>
        /// <param name="scenarioNumber">index of desired scenario</param>
        private void SetScenario(int scenarioNumber)
        {
            state.CurrentScenarioNumber = scenarioNumber;
            
            state.ProblemIndexes = state.CurrentExperiment.scenarios[state.CurrentScenarioNumber]
                .problems.Select(p => p.Problem_number).Distinct().ToList();
        }
        /// <summary>
        /// Gets the name of the map for the current scenario.
        /// </summary>
        /// <returns>Map name of the current scenario</returns>
        public string GetMapName()
        {
            string mapName = state.CurrentExperiment.scenarios[state.CurrentScenarioNumber].Map_Name; 
            if (mapName == "randomSilver")
            {
                silverMapCount++;
                mapName += ("_map" + silverMapCount + ".txt");
                mapName = Path.Combine("maps", "silverMaps", mapName);

            }
            return mapName;
        }
        /// <summary>
        /// Gets the current problem numbers
        /// </summary>
        /// <returns>The current problem index</returns>
        public int CurrentProblemNumber()
        {
            return state.CurrentProblemNumber;
        }
        /// <summary>
        /// Gets the current scenario index
        /// </summary>
        /// <returns> Returns the current scenario index</returns>
        public int CurrentScenarioNumber()
        {
            return state.CurrentScenarioNumber;
        }

        /// <summary>
        /// Helper function to add up to the number of requeired probelms based on the list of available openSpaces. No solvabilty checks are in place.
        /// </summary>
        /// <param name="problems">List of the current problems</param>
        /// <param name="numberRequired">Number of desired problems</param>
        /// <param name="openSpaces">List of spaces the problem can use for its goals and start locations.</param>
        public void FillProblems(List<Problem> problems,  int numberRequired, List<Tile> openSpaces)
        {
            ProblemGenerator.FillProblems(problems, numberRequired, openSpaces);
        }

        /// <summary>
        /// Internal function to load the experiment
        /// </summary>
        /// <param name="path">Path the experiment resides</param>
        /// <param name="experimentName"> name of the experiment</param>
        /// <returns>The experiment consisting of a list of problems and scenarios.</returns>
        private static Experiment LoadExperiment(string path, string experimentName)
        {
            //string[] experimentFile = System.IO.File.ReadAllLines(path);
            Experiment experiment = new Experiment();
            string experimentPath = Path.Combine(path, "experiments", experimentName);
            using (TextReader experimentReader = File.OpenText(experimentPath))
            {
                var experimentCSV = new CsvReader(experimentReader);
                experimentCSV.Configuration.HasHeaderRecord = true;
                IEnumerable<ScenarioConfig> experiments = experimentCSV.GetRecords<ScenarioConfig>();
                foreach (ScenarioConfig scenario in experiments)
                {
                    var scenarioPath = Path.Combine(path, scenario.Scenario_File);
                    using (TextReader scenarioRead = File.OpenText(scenarioPath))
                    {
                        var scenarioReader = new CsvReader(scenarioRead);
                        scenarioReader.Configuration.HasHeaderRecord = true;
                        IEnumerable<Problem> problems = scenarioReader.GetRecords<Problem>();
                        foreach (Problem problemRecord in problems)
                        {
                            //problemRecord.Start_X--;
                            //problemRecord.Start_Y--;
                            //problemRecord.Goal_X--;
                            //problemRecord.Goal_Y--;

                            scenario.addProblem(problemRecord);
                        }
                    }
                    experiment.addScenario(scenario);
                }
            }
            return experiment;
        }

        /// <summary>
        /// Internal function to load  experiment for each map in a directory
        /// </summary>
        /// <param name="path">Path the experiment resides</param>
        /// <param name="experimentName"> name of the experiment</param>
        /// <returns>The experiment consisting of a list of problems and scenarios.</returns>
        private Experiment LoadMapExperiment()
        {
            //string[] experimentFile = System.IO.File.ReadAllLines(path);
            Experiment experiment = new Experiment();
            for(int i =0; i < this.parameters.mapParameters.trialsPerMap;i++)
            {
                ScenarioConfig scenario = new ScenarioConfig()
                {
                    Map_Name = this.parameters.mapParameters.mapName,
                    problems = new List<Problem>()
                };
                experiment.addScenario(scenario);

            }

            return experiment;
        }
    }
}

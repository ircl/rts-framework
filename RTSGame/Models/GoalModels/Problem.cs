﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models
{
    /// <summary>
    /// Specifies a seach problem for a single agent.
    /// </summary>
    public class Problem
    {
        /// <summary>
        /// Id number for the problem
        /// </summary>
        public int Problem_number { get; set; }
        /// <summary>
        /// X positon of the start
        /// </summary>
        public int Start_X { get; set; }
        /// <summary>
        /// Y Postion of the start
        /// </summary>
        public int Start_Y { get; set; }
        /// <summary>
        /// X position of the goal
        /// </summary>
        public int Goal_X { get; set; }
        /// <summary>
        /// Y postion of the goal
        /// </summary>
        public int Goal_Y { get; set; }
        /// <summary>
        /// Optimal travel cost of the agent.
        /// </summary>
        public float Optimal_Travel_Cost { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.GoalModels
{
    class ProblemGenerator
    {
        Random rand;
        ExperimentParameters parameters;
        HashSet<Tile> goalLocations;
        HashSet<Tile> startingLocations;
        Utilities utilities = new Utilities();
        private enum Direction
        {
            left,
            right,
            up,
            down
        };
        internal struct OpenSpaceProps
        {
            internal int maxY;
            internal int minY;
            internal int maxX;
            internal int minX;
            internal int middleY;
            internal int middleX;
            
            internal OpenSpaceProps(List<Tile> openSpaces) {
                middleY = (int)openSpaces.Average(t => t.row);
                middleX = (int)openSpaces.Average(t => t.col);
                minX = openSpaces.Min(t => t.col);
                maxX = openSpaces.Max(t => t.col);
                minY = openSpaces.Min(t => t.row);
                maxY = openSpaces.Max(t => t.row);
            }
        }

        internal ProblemGenerator(ExperimentParameters parameters)
        {
            this.parameters = parameters;
            if (parameters.experimentParameters.seed > 0)
            {
                rand = new Random(parameters.experimentParameters.seed);
            }
            else
            {
                rand = new Random();
            }
        }
        internal void FillProblems(List<Problem> problems, int numberRequired, List<Tile> openSpaces)
        {
            GenerateProblems(problems, numberRequired, openSpaces);
        }
        //private List<Tile> GetOutsideTiles(List<Tile> openSpaces)
        //{
        //    return openSpaces.GetRange(openSpaces.Count / 2, openSpaces.Count / 2);
        //}
        //private List<Tile> GetInsideTiles(List<Tile> openSpaces)
        //{
        //    return openSpaces.GetRange(0, openSpaces.Count / 2 - 1);
        //}
        private List<Tile> GetNearByTiles(List<Tile> openSpaces, Tile tile, int radius, HashSet<Tile> filter){
            return  openSpaces.Where(t=> utilities.GetEuclideanDistance(t.col, t.row, tile.col, tile.row) < radius &&
             !filter.Contains(t)).ToList();
        }
        private bool HasNearByTiles(List<Tile> openSpaces, Tile tile, int radius, HashSet<Tile> filter)
        {
            return  openSpaces.Any(t=> utilities.GetEuclideanDistance(t.col, t.row, tile.col, tile.row) < radius &&
                !filter.Contains(t));
        }
        private bool GenerateOutsideInProblems( List<Tile> openSpaces, List<Problem> problems)
        {
            
            //List<Tile> outsideTiles = GetOutsideTiles(openSpaces);
            List<Tile> validStartTiles = openSpaces.Where(t => !startingLocations.Contains(t)).ToList();
            if (validStartTiles.Count == 0) return false;
            int startNum = rand.Next(validStartTiles.Count / 2, validStartTiles.Count - 1);
            Tile start = validStartTiles[startNum];

            List<Tile> validGoalTiles = openSpaces.Where(t => t.labeled_component == start.labeled_component && !goalLocations.Contains(t)).ToList();            
            if (validGoalTiles.Count == 0)
            {
                return false;
            }

            int goalNum = rand.Next(0, validGoalTiles.Count / 2) ;
            Tile goal = validGoalTiles[goalNum];
            problems.Add(CreateProblem(goal, start, problems.Count));
            return true;
        }
         private bool GenerateGroupProblems( List<Tile> openSpaces, List<Problem> problems, float startRadius, float goalRadius, Tile startCenterTile, Tile goalCenterTile)
        {
            bool FoundStartTiles = false;
            int NumberOfRelaxations = Math.Max(startCenterTile.gridMap.height,startCenterTile.gridMap.width);
            int Relaxations = 0;
            List<Tile> validStartTiles = new List<Tile>();
            while(!FoundStartTiles && Relaxations < NumberOfRelaxations){
                int relaxedRadius = (int)(startRadius + Relaxations);
                bool hasTile = HasNearByTiles(openSpaces, startCenterTile,relaxedRadius, startingLocations);
                if (hasTile)
                {
                    validStartTiles = GetNearByTiles(openSpaces, startCenterTile, relaxedRadius, startingLocations);
                    FoundStartTiles = true;
                    break;
                }

                Relaxations++;
            }
            if (!FoundStartTiles){
                return FoundStartTiles;
            }

            bool FoundGoalTiles = false;
            Relaxations = 0;
            List<Tile> validGoalTiles = new List<Tile>();
            while(!FoundGoalTiles && Relaxations < NumberOfRelaxations){
                int relaxedRadius =  (int)(goalRadius + Relaxations);
                if (HasNearByTiles(openSpaces, goalCenterTile, relaxedRadius, goalLocations)){
                    validGoalTiles = GetNearByTiles(openSpaces, goalCenterTile, relaxedRadius, goalLocations);
                    FoundGoalTiles = true;
                    break;
                }
                Relaxations++;
            }
            if (!FoundGoalTiles){
                return FoundGoalTiles;
            }
            validStartTiles = validStartTiles.Where(t => !startingLocations.Contains(t)).ToList();
            if (validStartTiles.Count == 0){
                return false;
            }
            int startNum = rand.Next(0, validStartTiles.Count);
            Tile start = validStartTiles[startNum];
            validGoalTiles = validGoalTiles.Where(t => t.labeled_component == start.labeled_component && !goalLocations.Contains(t)).ToList();
           
            if (validGoalTiles.Count == 0)
            {
                return false;
            }
            int goalNum = rand.Next(0, validGoalTiles.Count);
            Tile goal = validGoalTiles[goalNum];
            problems.Add(CreateProblem(goal, start, problems.Count));
            return true;
        }
        private bool GenerateInsideOutProblems(List<Tile> openSpaces, List<Problem> problems)
        {

            //List<Tile> insideTiles = GetInsideTiles(openSpaces);
            List<Tile> validStartTiles = openSpaces.Where(t => !startingLocations.Contains(t)).ToList();
            if (validStartTiles.Count == 0) return false;
            int startNum = rand.Next(0, validStartTiles.Count / 2);
            Tile start = validStartTiles[startNum];
            List<Tile> validGoalTiles = openSpaces.Where(t =>!goalLocations.Contains(t) && t.labeled_component == start.labeled_component).ToList();
            //validGoalTiles = GetOutsideTiles(validGoalTiles);
            if (validGoalTiles.Count == 0)
            {
                return false;
            }
            int goalNum = rand.Next(validGoalTiles.Count / 2, validGoalTiles.Count - 1);
            Tile goal = validGoalTiles[goalNum];
            problems.Add(CreateProblem(goal, start, problems.Count));
            return true;
        }
        internal bool GenerateRandomProblem( List<Tile> openSpaces, List<Problem> problems)
        {
            List<Tile> validStartSpaces = openSpaces.Where(t => !startingLocations.Contains(t)).ToList();
            if (validStartSpaces.Count == 0) return false;
           // validStartSpaces = validStartSpaces.Where(t => t.row == 3 && t.col == 11).ToList();
            int startNum = rand.Next(0, validStartSpaces.Count);
            Tile start = validStartSpaces[startNum];
            
            List<Tile> validGoalTiles = openSpaces.Where(t => t.labeled_component == start.labeled_component 
                && !goalLocations.Contains(t)).ToList();
            ////validGoalTiles = validGoalTiles.Where(t => t.row > 9 && t.col == 11).ToList();

            if (validGoalTiles.Count == 0)
            {
                return false;
            }
            int goalNum = rand.Next(0, validGoalTiles.Count);
            Tile goal = validGoalTiles[goalNum];
            problems.Add(CreateProblem(goal, start, problems.Count));
            return true;
        }
        
        private bool GenerateSwitchSideProblems(List<Tile> openSpaces, List<Problem> problems, Direction direction)
        {
            OpenSpaceProps openSpaceProps = new OpenSpaceProps(openSpaces);
           
            switch (direction)
            {
                case (Direction.down):
                    openSpaces.Sort((a, b) => (Math.Abs(a.row - openSpaceProps.minY)).CompareTo(Math.Abs(b.row - openSpaceProps.minY)));
                    break;
                case (Direction.up):
                    openSpaces.Sort((a, b) => (Math.Abs(openSpaceProps.maxY - a.row)).CompareTo(Math.Abs(openSpaceProps.maxY - b.row)));

                    break;
                case (Direction.right):
                    openSpaces.Sort((a, b) => (Math.Abs(a.col - openSpaceProps.minX)).CompareTo(Math.Abs(b.col - openSpaceProps.minX)));

                    break;
                case (Direction.left):
                    openSpaces.Sort((a, b) => Math.Abs(a.col - openSpaceProps.maxX).CompareTo(Math.Abs(b.col - openSpaceProps.maxX)));
                    break;
            }
            List<Tile> validStartTiles = openSpaces.Where(t => !startingLocations.Contains(t)).ToList();
            if (validStartTiles.Count == 0) {
                return false;
            }
            int startNum = rand.Next(0, validStartTiles.Count / 2);
            Tile start = validStartTiles[startNum];

            List<Tile> validGoalTiles = openSpaces.Where(t => t.labeled_component == start.labeled_component
                && !goalLocations.Contains(t)).ToList();
            //OpenSpaceProps goalSpaceProps = new OpenSpaceProps(validGoalTiles);
            //switch (direction)
            //{
            //    case (Direction.down):
            //        openSpaces.Sort((a, b) => (Math.Abs(a.row - openSpaceProps.maxY)).CompareTo(Math.Abs(b.row - openSpaceProps.maxY)));
            //        break;
            //    case (Direction.up):
            //        openSpaces.Sort((a, b) => (Math.Abs(openSpaceProps.minY - a.row)).CompareTo(Math.Abs(openSpaceProps.minY - b.row)));

            //        break;
            //    case (Direction.right):
            //        openSpaces.Sort((a, b) => (Math.Abs(a.col - openSpaceProps.maxX)).CompareTo(Math.Abs(b.col - openSpaceProps.maxX)));

            //        break;
            //    case (Direction.left):
            //        openSpaces.Sort((a, b) => Math.Abs(a.col - openSpaceProps.minX).CompareTo(Math.Abs(b.col - openSpaceProps.minX)));
            //        break;
            //}
            if (validGoalTiles.Count == 0) {
                return false;
            } else if (validGoalTiles.Count == 1) {
                
                Tile goal = validGoalTiles[0];

                problems.Add(CreateProblem(goal, start, problems.Count));
            } else
            {
                int goalNum = rand.Next(validGoalTiles.Count() / 2, validGoalTiles.Count);
                Tile goal = validGoalTiles[goalNum];

                problems.Add(CreateProblem(goal, start, problems.Count));
            }
            return true;
        }
        internal void GenerateProblems(List<Problem> problems, int numberRequired, List<Tile> openSpaces)
        {
            goalLocations = new HashSet<Tile>();
            startingLocations = new HashSet<Tile>();
            bool verticalSwap = rand.Next(2) == 1;
            Array values = Enum.GetValues(typeof(Direction));
            Direction randomDirection = (Direction)values.GetValue(rand.Next(values.Length));
            if(parameters.experimentParameters.goalType == "outside in" || parameters.experimentParameters.goalType == "inside out")
            {
                OpenSpaceProps openSpaceProps = new OpenSpaceProps(openSpaces);
                openSpaces.Sort((a,b) => utilities.GetEuclideanDistance(a.col, a.row, openSpaceProps.middleX, openSpaceProps.middleY)
                    .CompareTo(utilities.GetEuclideanDistance(b.col, b.row, openSpaceProps.middleX, openSpaceProps.middleY)));
            }
            Tile startCenterTile = null;
            Tile goalCenterTile = null;
            float startRadius = 10f;
            float goalRadius = 10f;
            if (parameters.experimentParameters.goalType.Contains("group")) {
                List<String> groupParams = parameters.experimentParameters.goalType.Substring(5).Split(':').ToList();

                startRadius = Math.Max(float.Parse(groupParams[0]), 1.0f) * ((float)Math.Sqrt(numberRequired) / 2);
                goalRadius =  Math.Max(float.Parse(groupParams[1]), 1.0f) * ((float)Math.Sqrt(numberRequired) / 2);
                startCenterTile = openSpaces[rand.Next(openSpaces.Count - 1)];
                List<Tile> applicableGoalCenters = openSpaces.Where(t => t.labeled_component == startCenterTile.labeled_component).ToList();

                goalCenterTile = applicableGoalCenters[rand.Next(applicableGoalCenters.Count - 1)];
            }
            while (problems.Count < numberRequired)
            {
                bool addedProb = false;
                if (parameters.experimentParameters.goalType == "random")
                {
                   addedProb = GenerateRandomProblem(openSpaces, problems);
                } else if (parameters.experimentParameters.goalType == "outside in")
                {
                    addedProb = GenerateOutsideInProblems(openSpaces, problems);
                } else if (parameters.experimentParameters.goalType == "inside out")
                {
                    addedProb = GenerateInsideOutProblems(openSpaces, problems);
                } else if (parameters.experimentParameters.goalType == "swap sides")
                {
                    if (verticalSwap)
                    {
                        addedProb = GenerateSwitchSideProblems(openSpaces, problems,rand.Next(2) == 1 ? Direction.down : Direction.up);
                    } else
                    {
                        addedProb = GenerateSwitchSideProblems(openSpaces, problems, rand.Next(2) == 1 ? Direction.left : Direction.right);
                    }
                } else if (parameters.experimentParameters.goalType == "cross sides")
                {
                    addedProb = GenerateSwitchSideProblems(openSpaces, problems, randomDirection);
                } else if (parameters.experimentParameters.goalType.Contains("group")){
                    addedProb = GenerateGroupProblems(openSpaces, problems, startRadius, goalRadius, startCenterTile, goalCenterTile);
                } else
                {
                    Console.WriteLine("Unknown problem type!");
                    throw new ArgumentException("Unknown problem type");
                }
                if (!addedProb)
                {
                    
                    Console.WriteLine("Unable to create problem" + parameters.index);
                    break;
                }
            }
        }
        
        /// <summary>
        /// Creates a problem based on the given input. No solvabilty checks are in place. Currently provides no optimal solution cost.
        /// </summary>
        /// <param name="goalSpace">Tile for where the goal will be</param>
        /// <param name="startSpace">Tile for where the start will be</param>
        /// <param name="problem_id">id number for the problem</param>
        /// <returns>The probem created based on the input</returns>
        private Problem CreateProblem(Tile goalSpace, Tile startSpace, int problem_id)
        {
            if (goalLocations.Contains(goalSpace))
            {
                throw new ArgumentException("This goal was already taken");
            }
            if (startingLocations.Contains(startSpace))
            {
                throw new ArgumentException("This start was already taken!");
            }
            goalLocations.Add(goalSpace);
            startingLocations.Add(startSpace);
            // clear the new taken space from the map;
            Problem newProblem = new Problem
            {
                Goal_X = goalSpace.col,
                Goal_Y = goalSpace.row,

                Start_X = startSpace.col,
                Start_Y = startSpace.row,

                Problem_number = problem_id,
                Optimal_Travel_Cost = float.NaN // TODO: Replace with A* solutions.
            };
            return newProblem;
        }
    }
}
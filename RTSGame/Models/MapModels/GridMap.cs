﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame
{
    /// <summary>
    /// Grid map representation of the world.
    /// </summary>
    public class GridMap
    {
        public string id;
        public int width;
        public int height;
        private Utilities _utils = new Utilities();
        private List<List<Tile>> tiles;
        private bool isFourConnected = false;
        private Utilities utils = new Utilities();
        bool blockDiagonalWithCardinalWall = false;
        bool padMap;
        bool removeAgentAtGoals = false;
        private string[] padMapText(String[] mapText)
        {
            for (int i = 0; i < mapText.Count(); i++)
            {
                mapText[i] = "1" + mapText[i] + "1";
            }
            String wallPad = new string('1', mapText[0].Count());
            String[] newMapText = new String[mapText.Count() + 2];
            newMapText[0] = wallPad;
            for(int i =0; i < mapText.Count(); i++)
            {
                newMapText[i + 1] = mapText[i];
            }
            newMapText[mapText.Count() + 1] = wallPad;
            return newMapText;
        }
        /// <summary>
        /// Converts a List of text in to the grid map.
        /// </summary>
        /// <param name="mapText">List of text to be parsed.</param>
        /// <param name="id">map id</param>
        /// <param name="isFourConnected">use four connected grid?</param>
        /// <param name="skipCorners">Allow diagonals with cardinal blocked</param>
        /// <param name="removeAgentAtGoals">Should agents in their goal exist</param>
        public GridMap(string[] mapText, string id, bool isFourConnected = false, bool skipCorners= false, bool removeAgentAtGoals = false, bool padGrid = true)
        {
            this.removeAgentAtGoals = removeAgentAtGoals;
            this.isFourConnected = isFourConnected;
            this.blockDiagonalWithCardinalWall = skipCorners;
            if (padGrid)
            {
                mapText = padMapText(mapText);
            }
            this.padMap = padGrid;
            this.tiles = convertMapStringToTiles(mapText);
            createTileData();
            labelComponents(tiles);
            this.id = id;
            this.height = tiles.Count;
            this.width = tiles[0].Count;
        }
        private void createTileData()
        {
            for (int row = 0; row < tiles.Count; row++)
            {
                List<Tile> rowTiles = tiles[row];
                for (int col = 0; col < rowTiles.Count; col++)
                {
                    rowTiles[col].populateFlowData();
                }
            }
        }
        internal bool isAdjcentToTunnel(Tile tile)
        {
            List<Tile> neighbours = tile.getNonWallNeighbours(false);
            return neighbours.Any(d=> utils.IsCardinalNeighbour(d,tile) && isTunnel(d));
        }

        public void resetTiles()
        {
            foreach (List<Tile> row in tiles)
            {
                foreach(Tile t in row)
                {
                    t.resetTile();
                }
            }
        }

        internal bool isAdjcentToTwoSourcesOrSinks(Tile tile)
        {
            List<Tile> cardinalN = tile.getNonWallNeighbours(false);
            
            return cardinalN.Count(d=>(isSink(d)) && utils.IsCardinalNeighbour(d,tile)) == 2 ||
                cardinalN.Count(d => (isSource(d)) && utils.IsCardinalNeighbour(d, tile)) == 2;
        }

        /// <summary>
        /// Internal coverter function.
        /// </summary>
        /// <param name="mapText">Text to be parsed</param>
        /// <returns>2D matirx of 8-connected tiles.</returns>
        private List<List<Tile>> convertMapStringToTiles(string[] mapText)
        {
            List<List<Tile>> mapTiles = new List<List<Tile>>(mapText.Length);

            for (Int16 rowI = 0; rowI < mapText.Length; rowI++)
            {
                string row = mapText[rowI];
                List<Tile> rowTiles = new List<Tile>(row.Length);
                for (Int16 colI = 0; colI < row.Length; colI++)
                {
                    Boolean wall = false;
                    if (row[colI] == '1')
                    {
                        wall = true;
                    }
                    int tileId = _utils.GetTileId(rowI, colI);
                    Tile tile = new Tile(wall, wall, tileId, rowI, colI,this);
                    tile.setRemoveAgentAtGoal(this.removeAgentAtGoals);
                    rowTiles.Add(tile);
                }
                mapTiles.Add(rowTiles);
            }
            return mapTiles;
        }
        /// <summary>
        /// Gets all non wall tiles.
        /// </summary>
        /// <returns>List of non-wall tiles</returns>
        public List<Tile> getOpenSpaces()
        {
            List<Tile> openSpaces = new List<Tile>();
            for (int i = 0; i < this.tiles.Count; i++)
            {
                List<Tile> row = this.tiles[i];
                for (int j = 0; j < row.Count; j++)
                {
                    Tile tile = row[j];
                    if (!tile.isWall())
                    {
                        openSpaces.Add(tile);
                    }
                }
            }
            return openSpaces;
        }
        private void labelComponents(List<List<Tile>> map)
        {
            List<int> linked = new List<int>();
            int labelNum = 0;
            for (int i = 0; i < map.Count; i++)
            {
                List<Tile> row = map[i];
                for (int j = 0; j < row.Count; j++)
                {
                    Tile curTile = row[j];
                    if (curTile.labeled_component == -1 &&
                        !curTile.isWall())
                    {
                        utils.LabelConnected(curTile, labelNum);
                        labelNum++;
                    }                    
                }
            }
        }
        
        public bool isLeftFlow (Tile tile)
        {
            int offset = padMap ? 1 : 0;
            return tile.row % 2 == offset;
        }
        public bool isUpFlow(Tile tile)
        {
            int offset = padMap ? 1 : 0;
            return tile.col % 2 == offset;
        }
        public bool isSink(Tile tile)
        {
            List<Tile> inBoundHorizontalN = getHorizontalFlowNeighbour(tile, true);
            List<Tile> inBoundVerticalN = getVerticalFlowNeighbour(tile, true);
            List<Tile> outBoundHorizontalN = getHorizontalFlowNeighbour(tile);
            List<Tile> outBoundVerticalN = getVerticalFlowNeighbour(tile);
            if ((outBoundHorizontalN.Count == 0 || outBoundHorizontalN.All(t => t.isWall())) &&
                (outBoundVerticalN.Count == 0 || outBoundVerticalN.All(t => t.isWall())))
            {
                
                return true;
                
            }
            return false;
        }
        /// <summary>
        /// checks if the tile is a source
        /// </summary>
        /// <param name="tile">tile requesting neighbours</param>
        public bool isSource(Tile tile)
        {
            List<Tile> inBoundHorizontalN = getHorizontalFlowNeighbour(tile, true);
            List<Tile> inBoundVerticalN = getVerticalFlowNeighbour(tile, true);
            List<Tile> outBoundHorizontalN = getHorizontalFlowNeighbour(tile);
            List<Tile> outBoundVerticalN = getVerticalFlowNeighbour(tile);
            //if ((inBoundHorizontalN.Count == 0 || inBoundHorizontalN.All(t => t.isWall())) &&
            //    (inBoundVerticalN.Count == 0 || inBoundVerticalN.All(t => t.isWall())))
            //{
            //    if ((outBoundHorizontalN.Count > 0 && outBoundHorizontalN.Any(t => !t.isWall())) &&
            //        (outBoundVerticalN.Count > 0 && outBoundVerticalN.Any(t => !t.isWall())))
            //    {
            //        return true;
            //    }
            //}

            //////
            // 111
            // 101
            // 100
            //
            ////
            if ((inBoundHorizontalN.Count == 0 || inBoundHorizontalN[0].isWall()) &&
                (inBoundVerticalN.Count == 0 || inBoundVerticalN[0].isWall()))
            {
                if ((outBoundHorizontalN.Count > 0 && !outBoundHorizontalN[0].isWall()) ||
                    (outBoundVerticalN.Count > 0 && !outBoundVerticalN[0].isWall()))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Adds neighbour above if odd, and below if even.
        /// </summary>
        /// <param name="tile">tile that is looking for neighbours</param>
        /// <param name="inbound">is the request for inbound neighbours</param>
        /// <returns>vertical neighbours</returns>
        public List<Tile> getVerticalFlowNeighbour(Tile tile, bool inbound = false)
        {
            List<Tile> neighbours = new List<Tile>();

            int upOffset = tile.isUpFlow ? -1 : 1;
            upOffset = inbound ? -upOffset : upOffset;
            if (tile.row + upOffset < tiles.Count && tile.row + upOffset >= 0)
            {
                neighbours.Add(tiles[tile.row + upOffset][tile.col]);
            }
            return neighbours;
        }
        /// <summary>
        /// Adds neighbour to the left if odd, and to the right if even.
        /// </summary>
        /// <param name="tile">tile that is looking for neighbours</param>
        /// <param name="inbound">is the request for inbound neighbours</param>
        /// <returns>where to add the neighbours</returns>
        public List<Tile> getHorizontalFlowNeighbour(Tile tile, bool inbound = false)
        {
            List<Tile> neighbours = new List<Tile>();

            int leftOffset = tile.isLeftFlow ? -1 : 1;
            leftOffset = inbound ? -leftOffset : leftOffset;
            if (tile.col + leftOffset < tiles[tile.row].Count && tile.col + leftOffset >= 0)
            {
                neighbours.Add(tiles[tile.row][tile.col + leftOffset]);
            }
            return neighbours;
        }
        /// <summary>
        /// get sink neighbour if there is on.
        /// </summary>
        /// <param name="tile">tile that is looking for neighbours</param>
        /// <returns>the neighbours</returns>
        public List<Tile> getSinkNeighbours(Tile tile)
        {
            List<Tile> neighbours = new List<Tile>();
            int leftOffset = tile.isLeftFlow ? -1 : 1;
            int upOffset = tile.isUpFlow ? -1 : 1;
            bool isSinkTile = tile.isSink;
            
            if (isSinkTile)
            {
                List<Tile> diagonalN = tile.getNonWallNeighbours(false).Where(d => !utils.IsCardinalNeighbour(d, tile)).ToList();
                bool diagonalAddedIsSink = diagonalN.Any(d => d.isSink);
                if (diagonalAddedIsSink)
                {
                    neighbours.AddRange(tile.getNeighbours(false));
                }
                else
                {
                    neighbours.AddRange(diagonalN);
                }
            }
            else
            {
                //List<Tile> verticalTileN = getVerticalFlowNeighbour(tile);
                //List<Tile> horizontalTileN = getHorizontalFlowNeighbour(tile);
                //if (horizontalTileN.Count > 0 && horizontalTileN.All(t => isSink(t)) &&
                //    verticalTileN.Count > 0 && verticalTileN.All(t => isSink(t)))
                //{
                //    List<Tile> inboundVerticalTileN = getVerticalFlowNeighbour(tile, true);
                //    List<Tile> inboundHorizontalTileN = getHorizontalFlowNeighbour(tile, true);
                //    neighbours.AddRange(inboundVerticalTileN);
                //    neighbours.AddRange(inboundHorizontalTileN);
                //}
            }
            
            return neighbours;
        }
        public List<Tile> getDiagonalSources(Tile center, List<Tile> n)
        {
            List<Tile> sources = new List<Tile>();
            foreach(Tile t in n)
            {
                if (!utils.IsCardinalNeighbour(t,center) &&
                    t.isSource) {
                    sources.Add(t);
                }
            }
            return sources;
        }
        public List<Tile> getCardinalSources(Tile center, List<Tile> n)
        {
            List<Tile> sources = new List<Tile>();
            foreach (Tile t in n)
            {
                if (utils.IsCardinalNeighbour(t, center) &&
                    t.isSource)
                {
                    sources.Add(t);
                }
            }
            return sources;
        }
        /// <summary>
        ///         /// Adds the sink neighbours to the list of neighbours.
        /// </summary>
        /// <param name="tile">tile requesting neighbours</param>
        /// <returns></returns>
        public List<Tile> getSourceNeighbours(Tile tile)
        {
            List<Tile> allN = tile.getNonWallNeighbours(false);
            bool tileIsSource = tile.isSource;
            List<Tile> neighbours = new List<Tile>();
            if (!tileIsSource)
            {
                List<Tile> diagonalsources = getDiagonalSources(tile, allN);
                List<Tile> cardinalSources = getCardinalSources(tile, allN);
                if (cardinalSources.Count == 2)
                {
                    neighbours.AddRange(getNeighbours(tile));
                } else
                {
                    neighbours.AddRange(diagonalsources);
                }
            } 
            return neighbours;
        }
        public List<Tile> getDualSinkSourceNeighbours(Tile tile)
        {
            List<Tile> dualSourceOut = new List<Tile>();
            List<Tile> allN = tile.getNonWallNeighbours(false);
            
            return allN.Where(t=> t.isAdjcentToTwoSourcesOrSinks).ToList();
        }


        public bool isTunnel(Tile tile)
        {
            List<Tile> cardinalN = tile.getNeighbours(false).Where(t => utils.IsCardinalNeighbour(t, tile) && !t.isWall()).ToList();
            List<Tile> diagonalN = tile.getNeighbours(false).Where(t => !utils.IsCardinalNeighbour(t, tile) && !t.isWall()).ToList();
            if (cardinalN.Count == 2) { 
                if (cardinalN.All(d => d.row == tile.row) ||
                    cardinalN.All(d => d.col == tile.col))
                {
                    return true;
                } else
                {
                    if (!diagonalN.Any(d=> cardinalN.All(t=> (t.row == d.row || t.col == d.col))))
                    {
                        return true;
                    } else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Returns the neighbours added if the tile is a tunnel. adjcent
        /// </summary>
        /// <param name="tile"></param>
        /// <returns></returns>
        public List<Tile> getTunnelNeighbours(Tile tile)
        {
            List<Tile> nonWallCardinalN = getNeighbours(tile).Where(t => utils.IsCardinalNeighbour(t,tile) && !t.isWall()).ToList();

            if (tile.isTunnel || tile.isAdjcentToTunnel )
            {
                return nonWallCardinalN;
            } 
            else
            {
                return nonWallCardinalN.Where(d=> d.isAdjcentToTunnel || d.isTunnel).ToList();
            }

        }
        public List<Tile> getDiagonalNeighbours(Tile tile)
        {
            List<Tile> neighbours = tile.getNeighbours(false);
            List<Tile> additionalNeighbours = neighbours.Where(d=>(
                !d.isWall() &&
                !utils.IsCardinalNeighbour(d,tile) &&
                d.getNeighbours(false).Count(t=> (t.row == tile.row || t.col == tile.col) && t.isWall()) == 2
            )).ToList();
           
            return additionalNeighbours;

        }
        /// <summary>
        /// Gets the flow restricted neighbours as described by FAR https://www.aaai.org/Papers/ICAPS/2008/ICAPS08-047.pdf
        /// </summary>
        /// <param name="tile">tile requesting neighbours</param>
        /// <returns>neighbours including walls</returns>
        public List<Tile> getFlowAnnotatedNeighbours(Tile tile)
        {
            int row = tile.row;
            int col = tile.col;

            List<Tile> neighbours = new List<Tile>();
            neighbours.AddRange(getVerticalFlowNeighbour(tile)); // add vertical tile
            neighbours.AddRange(getHorizontalFlowNeighbour(tile).Distinct().ToList()); // add horizontal tile
            neighbours.AddRange(getTunnelNeighbours(tile).Distinct().ToList()); // check if its a tunnel
            neighbours.AddRange(getSinkNeighbours(tile).Distinct().ToList()); // add sink tiles
            neighbours.AddRange(getSourceNeighbours(tile).Distinct().ToList()); // add source tiles
            neighbours.AddRange(getDualSinkSourceNeighbours(tile)); // add dual sink/souce tiles
            
            //if (!this.blockDiagonalWithCardinalWall)
            //{
            //    neighbours.AddRange(getDiagonalNeighbours(tile).Distinct().ToList()); // add diagaonal jumps
            //}
            List<Tile> uniqueNeighbours = neighbours.Distinct().ToList();
            return uniqueNeighbours;
        }
        /// <summary>
        /// Returns the unrestricted neighbours of a tile
        /// </summary>
        /// <param name="tile">tile asking for neighbours</param>
        /// <returns>neighbours</returns>
        public List<Tile> getNeighbours(Tile tile) {
            int row = tile.row;
            int col = tile.col;
            int rowStart = Math.Max(row - 1, 0);
            int rowEnd = Math.Min(row + 1, tiles.Count - 1);

            List<Tile> neighbours = new List<Tile>();
            for (int neighbourRow = rowStart; neighbourRow <= rowEnd; neighbourRow++)
            {
                int colStart = Math.Max(col - 1, 0);
                int colEnd = Math.Min(col + 1, tiles[neighbourRow].Count - 1);
                for (int neighbourCol = colStart; neighbourCol <= colEnd; neighbourCol++)
                {
                    if (!(neighbourRow == row && neighbourCol == col))
                    {
                        if (isFourConnected)
                        {
                            if (row == neighbourRow || col == neighbourCol) // is cardinal
                            {
                                Tile connectingNode = tiles[neighbourRow][neighbourCol];
                                neighbours.Add(connectingNode);
                            }
                        }
                        else
                        {
                            if (this.blockDiagonalWithCardinalWall)
                            {
                                if (!(row == neighbourRow || col == neighbourCol)) // is a diagonal
                                {
                                    Tile tileInTheSameRow = tiles[row][neighbourCol];
                                    Tile tileInTheSameCol = tiles[neighbourRow][col];
                                    if (!(tileInTheSameCol.isWall() || tileInTheSameRow.isWall()))
                                    {
                                        Tile connectingNode = tiles[neighbourRow][neighbourCol];
                                        neighbours.Add(connectingNode);
                                    }
                                }
                                else
                                {
                                    Tile connectingNode = tiles[neighbourRow][neighbourCol];
                                    neighbours.Add(connectingNode);
                                }
                            }
                            else
                            {
                                Tile connectingNode = tiles[neighbourRow][neighbourCol];
                                neighbours.Add(connectingNode);
                            }
                        }
                    }
                }
            }
            //currentNode.setNeighbours(neighbours);
            return neighbours;
        }
        /// <summary>
        /// Gets the tile at the specified location.
        /// </summary>
        /// <param name="row">row of the tile</param>
        /// <param name="col">column of the tile.</param>
        /// <returns></returns>
        public Tile getTile(int row, int col)
        {
            return tiles[row][col];
        }


    }
}

﻿using RTSGame.Models.MapModels;
using RTSGame.Models.MapModels.MapGenerators;
using RTSGame.Models.SearchControllers;
using RTSGame.Models.SearchData;
using RTSGame.Models.SearchModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.Models.SearchModels.AgentController;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.ExperimentModels
{

    /// <summary>
    /// Model tracking the current level/map status.
    /// </summary>
    class LevelModel
    {
        AgentController agentController = null;
        public GridMap mapModel;
        private MapGenerator mapGen;
        private Utilities utilities = new Utilities();
        public ExperimentParameters parameters;

        /// <summary>
        /// Creates the level model
        /// </summary>
        /// <param name="mapName">map name the model is representing.</param>
        /// <param name="parameters">params</param>
        /// <param name="seed">rng seed</param>
        /// <param name="preloadedMap">preloaded map</param>
        public LevelModel(string mapName, ExperimentParameters parameters, int seed, GridMap preloadedMap = null)
        {
            this.parameters = parameters;
            this.parameters.mapParameters.mapName = mapName;
            // load the level
            mapGen = new MapGenerator(seed);
            if(preloadedMap != null)
            {
                mapModel = preloadedMap;
            } else
            {
                loadLevel();
            }
            wallController = new WallController(parameters, mapModel);

            // create the agents
            // set their goals
            // update the map
        }

        private WallController wallController;
        /// <summary>
        /// Loads the map for the model
        /// </summary>
        private void loadLevel()
        {
  
            string[] mapText;
            string mapPrefix = Path.Combine(Directory.GetCurrentDirectory(), "Content", "searchExperiments");
            string mapFile = parameters.mapParameters.mapName.Contains(mapPrefix) ? parameters.mapParameters.mapName :
                Path.Combine(mapPrefix, parameters.mapParameters.mapName);
            if (parameters.mapParameters.mapName.Contains("randomSilver")|| parameters.mapParameters.mapName.Contains("randomMapConfigs"))
            {
                mapText = mapGen.genarateSilverMap(mapFile, parameters);
            } else if (parameters.mapParameters.mapName.ToLower().Contains("json"))
            {
                mapText = mapGen.genarateSilverMap(mapFile, parameters);
            }
            else if (System.IO.File.ReadLines(parameters.mapParameters.mapName).First().Contains("type"))
            {
                mapText = utilities.LoadMovingAIMap(parameters.mapParameters.mapName);
            } else
            {
                mapText = System.IO.File.ReadAllLines(mapFile);
            }
            mapModel = new GridMap(mapText, parameters.mapParameters.mapName, parameters.experimentParameters.fourConnected,
                parameters.experimentParameters.blockDiagonalWithCardinalWall, parameters.experimentParameters.removeAgentsAtGoal, false);
            //agentController = new AgentController(this);
            // load string of map;
            // create new map;
        }

        /// <summary>
        /// Gets all the empty spaces on the map.
        /// </summary>
        /// <returns>List of empty tiles.</returns>
        public List<Tile> getEmptySpaces()
        {
            return mapModel.getOpenSpaces();
        }
        int updates = 0;
        /// <summary>
        /// Has the level update itself.
        /// </summary>
        public void update()
        {
            agentController.Update();
            if (updates % 40== 0)
            {
                wallController.MoveWalls();
            }
            updates++;
        }
        public bool startedProblems = false;
        /// <summary>
        /// Creates new search agents to solve the inputed problems.
        /// </summary>
        /// <param name="problems">List of problems to be solved on the level. One problem per agent.</param>
        public void startProblems(List<Problem> problems)
        {
            if (parameters.algorithmParameters[0].controller == "whca")
            {
                agentController = new WindowedHeirchicalCOOPAStarController(parameters);
            }
            else if (parameters.algorithmParameters[0].controller.ToLower() == "far")
            {
                agentController = new FARAStarController(parameters);
            }
            else
            {
                agentController = new AgentController(parameters);
            }
            for (int i = 0; i < problems.Count; i++)
            {
               
                Tile start = mapModel.getTile(problems[i].Start_Y, problems[i].Start_X);
                Tile goal = mapModel.getTile(problems[i].Goal_Y, problems[i].Goal_X);
                agentController.CreateSearchAgent(start, goal, i, mapModel.width, mapModel.height, parameters);
            }
            startedProblems = true;
        }    
        /// <summary>
        /// Get the map height
        /// </summary>
        /// <returns>The current map height</returns>
        public int mapHeight()
        {
            return mapModel.height;
        }
        /// <summary>
        /// Get the map width
        /// </summary>
        /// <returns>the current map width</returns>
        public int mapWidth()
        {
            return mapModel.width;
        }
        /// <summary>
        /// gets the agents on the map
        /// </summary>
        /// <returns>A list of search agents on the map.</returns>
        public List<SearchAgent> getAgents(){
            if (agentController != null)
            {
                return agentController.GetAgents();

            } else
            {
                return new List<SearchAgent>();
            }
        }
        /// <summary>
        /// Get the tile at the specified location.
        /// </summary>
        /// <param name="row">row of the tile</param>
        /// <param name="col">column of the tile</param>
        /// <returns>tile at the specified location.</returns>
        public Tile getTileAt(int row, int col)
        {
            return mapModel.getTile(row, col);
        }
        /// <summary>
        /// get the search agent on the map by id.
        /// </summary>
        /// <param name="id">id of the search agent. Usually corresponding to the problem number the agent is solving.</param>
        /// <returns>The search agent with that ID.</returns>
        public SearchAgent getAgent(int id)
        {
            return agentController.GetAgent(id);
        }
        public List<AgentInfo> getAgentStats()
        {
            return agentController.GetAgentStats();
        }
        public bool allAgentsFinishedSearching()
        {
            return agentController.AllAgentsFinishedSearching();
        }
        public SearchAgent getLastUpdatedAgent()
        {
            return agentController.GetLastUpdatedAgent();
        }

        internal bool hasAgents()
        {
            if (agentController != null)
            {
                return agentController.agents.Count > 0;
            } else
            {
                return false;
            }
        }
    }
}

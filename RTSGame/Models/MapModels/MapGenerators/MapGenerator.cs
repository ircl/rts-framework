﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.MapModels.MapGenerators
{
    class SilverConfig
    {
        public int mapWidth { get; set; }
        public int mapHeight { get; set; }
        public float mazeDensity { get; set; }
    }
    class MapGenerator
    {

        private int mapsGenerated = 0;
        private int fixedSeed = 1;
        Random rnd;
        public MapGenerator(int seed)
        {
            fixedSeed = seed;
            if (seed > 0)
            {
                rnd = new Random(fixedSeed + mapsGenerated);
            }
            else
            {
                rnd = new Random();
            }
        }
        private SilverConfig loadMapFile(String pathFile)
        {
         
            String path = Path.Combine(Directory.GetCurrentDirectory(), "Content","searchExperiments", pathFile);
            string info = File.ReadAllText(path);
            SilverConfig experiment = JsonConvert.DeserializeObject<SilverConfig>(info);
            return experiment;
        }
        public string[] genarateSilverMap(string mapName, ExperimentParameters parameters)
        {
            SilverConfig silverConfig = loadMapFile(parameters.mapParameters.mapName);
            parameters.mapParameters.parameters = JsonConvert.SerializeObject(silverConfig, Formatting.Indented);
            int width = silverConfig.mapWidth;
            int height = silverConfig.mapHeight;
            float mazeDensity = silverConfig.mazeDensity;
            string[] maze = generateSilverMap(width, height, mazeDensity);

            //string finalPath = path + mapName;
            //using (TextWriter tw = new StreamWriter(mapName))
            //{
            //    for (int i = 0; i < height; i++)
            //    {
            //        tw.WriteLine(maze[i]);
            //    }
            //}
            return maze;
        }
        public string[] generateSilverMap(int width, int height, float mazeDensity)
        {
            mapsGenerated++;
            string[] maze = new string[height];
            // TODO: run this on each new level not here. only 1 file should be written.
            
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    int r = rnd.Next(0, 2);
                    if (rnd.Next() % 10000 < 10000 * mazeDensity)
                    {
                        maze[i] += "1";
                    }
                    else
                    {
                        maze[i] += "0";
                    }
                }
            }
           
            return maze;
        }
    }
}

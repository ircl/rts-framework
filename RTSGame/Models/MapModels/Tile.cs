﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame
{

    /// <summary>
    /// Tile for the agents to navigate on.
    /// </summary>
    public class Tile
    {
        /// <summary>
        /// Neighours of the tile
        /// </summary>
        //private List<Tile> neighbours;
        /// <summary>
        /// Boolean for if the tile is a wall.
        /// </summary>
        private Boolean _wall;
        /// <summary>
        /// Boolean for if the tile is occupied
        /// </summary>
        private Boolean _occupied;

        private int pathCount = 0;
        /// <summary>
        /// list of agents that marked the tile as their goal
        /// </summary>
        public int labeled_component = -1;
        public Int16 row;
        public Int16 col;
        public int id;
        private SearchAgent occupyingAgent = null;
        private bool removeAgentAtGoal;
        public GridMap gridMap;
        private bool _startingOccupied;
        private bool _startingWall;
        internal bool isTunnel { get; set; }
        internal bool isAdjcentToTunnel { get; set; }
        internal bool isSink { get; set; }
        internal bool isSource { get; set; }
        internal bool isLeftFlow { get; set; }
        internal bool isUpFlow { get; set; }
        internal bool isAdjcentToTwoSourcesOrSinks { get; set; }
        /// <summary>
        /// Creates a tile
        /// </summary>
        /// <param name="occupied">boolean for if the tile is occupied</param>
        /// <param name="wall">boolean for if the tile is a wall</param>
        /// <param name="id">id of the tile</param>
        /// <param name="row">row of the tile</param>
        /// <param name="col">column of the tile</param>
        public Tile(Boolean occupied, Boolean wall, int id, Int16 row, Int16 col, GridMap gridMap)
        {
            this.gridMap = gridMap;
            this._occupied = occupied;
            _startingOccupied = occupied;
            _startingWall = wall;
            this._wall = wall;
            this.id = id;
            this.col = col;
            this.row = row;
        }
        public void populateFlowData()
        {
            isLeftFlow = gridMap.isLeftFlow(this);
            isUpFlow = gridMap.isUpFlow(this);
            isAdjcentToTunnel = gridMap.isAdjcentToTunnel(this);
            isSink = gridMap.isSink(this);
            isSource = gridMap.isSource(this);
            isTunnel = gridMap.isTunnel(this);
            isAdjcentToTwoSourcesOrSinks = gridMap.isAdjcentToTwoSourcesOrSinks(this);         
        }
        /// <summary>
        /// Remove agents when they reach goal?
        /// </summary>
        /// <param name="removeAtGoal">true if remove agent at goal</param>
        public void setRemoveAgentAtGoal(bool removeAtGoal)
        {
            removeAgentAtGoal = removeAtGoal;

        }
        public void resetTile()
        {
            this._occupied = _startingOccupied;
            _wall = _startingWall;
        }
   
        /// <summary>
        /// Add an agent to the tile
        /// </summary>
        /// <param name="agent">agent occupying</param>
        /// <param name="atGoal">is the agent at its goal</param>
        internal void addAgent(SearchAgent agent, Boolean atGoal)
        {
            if (this._occupied)
            {
                new Exception("Tried moving to occupied");
            }
            if (atGoal && removeAgentAtGoal)
            {
                //this._goalAgents.Add(agentId);
            } else
            {
                //this._movingAgents.Add(agentId);
                this.occupyingAgent = agent;
                this._occupied = true;
            }
        }
        /// <summary>
        /// remove the agent from the tile.
        /// </summary>
        public void removeAgent()
        {
            this.occupyingAgent = null;
            if (!this._wall)
            {
                this._occupied = false;
            }             
        }
        ///// <summary>
        ///// set the tile as occupied or not
        ///// </summary>
        ///// <param name="occupied">boolean for if the tile is occupied </param>
        //public void setOccupied(bool occupied)
        //{
        //    this._occupied = occupied;
        //}
        /// <summary>
        /// check if tile is occupied
        /// </summary>
        /// <returns>true if occupied</returns>
        public Boolean isOccupied()
        {
            return this._occupied;
        }
        /// <summary>
        /// check if tile is available.
        /// </summary>
        /// <returns>true if not occupied and not a wall</returns>
        public Boolean isAvailable()
        {
            return this._occupied == false && this._wall == false && !this.isMovingWall;
        }

        internal SearchAgent GetAgent()
        {
            return occupyingAgent;
        }
        internal void IncrementPathCount()
        {
            pathCount++;
        }
        internal void DecrementPathCount()
        {
            pathCount--;
        }
        internal int GetPathCount()
        {
            return pathCount;
        }
        ///// <summary>
        ///// set the tiles neighbours
        ///// </summary>
        ///// <param name="neighbours">list of neighbouring tiles</param>
        //public void setNeighbours(List<Tile> neighbours)
        //{
        //    this.neighbours = neighbours;
        //}
        /// <summary>
        /// check if the id belongs to one of the tiles neighbours.
        /// </summary>
        /// <param name="tileId">id of tile in question</param>
        /// <returns>true if is a neighbour</returns>
        public Boolean hasEdge(int tileId)
        {
            if (this._wall)
                return false;
            return this.getNeighbours().Any(d => d.id == tileId);
        }
        /// <summary>
        /// Wether its a moving wall.
        /// </summary>
        public bool isMovingWall;
        /// <summary>
        /// set tile as a wall or not
        /// </summary>
        /// <param name="isWall">true if mark as wall</param>
        public void setIsWall(Boolean isWall, bool isMovingWall = false)
        {
            this._wall = isWall;
            
            this.isMovingWall = isMovingWall;
        }
        
        /// <summary>
        /// is the tile a wall?
        /// </summary>
        /// <returns>return true iff wall</returns>
        public Boolean isWall()
        {
            return this._wall;
        }
        /// <summary>
        /// Returns the tile neighbours including walls
        /// </summary>
        /// <param name="flowRestricted">if the neighbours should be flow restricted, false by default.</param>
        /// <returns>neighbours</returns>
        public List<Tile> getNeighbours(bool flowRestricted = false)
        {
            if (flowRestricted)
            {
                return gridMap.getFlowAnnotatedNeighbours(this);
            } else
            {
                return gridMap.getNeighbours(this);
            }
        }
        List<Tile> cachedFlowN = null;
        /// <summary>
        /// Get all non wall neighbours
        /// </summary>
        /// <param name="flowRestricted">Should the neighbours be flow restricted, false by default</param>
        /// <returns>List of non wall neighbours</returns>
        public List<Tile> getNonWallNeighbours(bool flowRestricted = false)
        {
            if (flowRestricted)
            {
                if (cachedFlowN == null)
                {
                    List<Tile> tmp = gridMap.getFlowAnnotatedNeighbours(this).Where(d => !d.isWall()).ToList();  
                    if(tmp.Count == 0)
                    {
                        tmp = this.getNeighbours(false).Where(d => !d.isWall()).ToList();
                    }
                    cachedFlowN = tmp;
                }
                return cachedFlowN;
            } else
            {
                return gridMap.getNeighbours(this).Where(d => !d.isWall()).ToList();
            }
        }
    }
}

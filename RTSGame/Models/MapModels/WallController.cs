﻿using RTSGame.Models.Configurations;
using RTSGame.Models.SearchModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.MapModels
{
    /// <summary>
    /// wall controller class
    /// </summary>
    class WallController
    {
        ExperimentParameters parameters;
        GridMap mapModel;
        List<SearchAgent> wallAgents = new List<SearchAgent>();
        Random rand = new Random();
        Utilities utils = new Utilities();
        private Stopwatch _resetclock = new Stopwatch();

        /// <summary>
        /// Creates the uLRTA search concrete factory
        /// </summary>
        private SearchCreator ultraCreator = new uLRTACreator();
        /// <summary>
        /// Creates the A star Concrete Factory
        /// </summary>
        private SearchCreator aStarCreator = new AStarCreator();
        private float resetLimit = float.MaxValue;
        /// <summary>
        /// Creates local repair aStar agents;
        /// </summary>
        private SearchCreator localRepairAStarCreator = new localRepairAStarCreator();
        public WallController(ExperimentParameters paramaterConfiguration, GridMap mapModel)
        {
            parameters = paramaterConfiguration;
            this.mapModel = mapModel;
            if (paramaterConfiguration.MovingObstacleParameters.timeLimit > 0)
            {
                resetLimit = paramaterConfiguration.MovingObstacleParameters.timeLimit;
            }
            openSpaces = mapModel.getOpenSpaces();
            if (paramaterConfiguration.experimentParameters.seed > 0)
            {
                rand = new Random(paramaterConfiguration.experimentParameters.seed);
            }
            else
            {
                rand = new Random();
            }
            CreateWallAgents();
            _resetclock.Start();
        }
        private void CreateWallAgents()
        {
            int numberOfMovingWalls = parameters.MovingObstacleParameters.numberOfWalls;
            List<Tile> openSpaces = mapModel.getOpenSpaces();
            while (wallAgents.Count() < numberOfMovingWalls)
            {
                if (openSpaces.Count <= 1)
                {
                    Console.WriteLine("Not enough open tiles");
                    break;
                }
                int startNum = rand.Next(0, openSpaces.Count);
                Tile start = openSpaces[startNum];

                openSpaces.RemoveAt(startNum);
                if (!start.getNonWallNeighbours().Any(d => d.isAvailable()))
                {
                    continue;
                }

                int goalNum = rand.Next(0, openSpaces.Count);
                //Tile goal = utils.getRandomConnectedNeighbour(start, )
                Tile goal = openSpaces[goalNum];
                if (goal.labeled_component != start.labeled_component)
                {
                    continue;
                }
                if (goal.isWall() || start.isWall())
                {
                    Console.WriteLine("Error! Either the Start or Goal is a wall");
                }
                openSpaces.RemoveAt(goalNum);
                SearchAgent searchAgent;
                if (parameters.MovingObstacleParameters.algorithm.name == "uLRTA")
                {
                    searchAgent = ultraCreator.FactoryMethod(wallAgents.Count, goal, start, mapModel.height, mapModel.width, parameters.MovingObstacleParameters.algorithm);
                } else
                {
                    searchAgent = localRepairAStarCreator.FactoryMethod(wallAgents.Count, goal, start, mapModel.height, mapModel.width, parameters.MovingObstacleParameters.algorithm);
                }
                start.setIsWall(false, true);
                wallAgents.Add(searchAgent);
            }
        }
        List<Tile> openSpaces;

        /// <summary>
        /// moves walls one step
        /// </summary>
        public void MoveWalls()
        {
            if (_resetclock.ElapsedMilliseconds > resetLimit)
            {
                wallAgents.Clear();
                CreateWallAgents();
                _resetclock.Reset();
            }
            foreach (SearchAgent wall in wallAgents)
            {
                 if (wall.currentSpot != wall.goal) {
                   
                    //Tile moveToTile = wall.updateTick();
                    //if (moveToTile.isAvailable())
                    //{
                    //    wall.currentSpot.setIsWall(false, false);
                    //    openSpaces.Add(wall.currentSpot);
                    //    wall.crossEdge(moveToTile, utils.GetTravelCost(moveToTile, wall.currentSpot));
                    //    wall.currentSpot.setIsWall(false, true);
                    //    openSpaces.Remove(wall.currentSpot);
                    //}
                }

            }
        }

    }
}

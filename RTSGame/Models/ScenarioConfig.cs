﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models
{
    /// <summary>
    /// The configuration file for a scenario
    /// </summary>
    public class ScenarioConfig
    {
        /// <summary>
        /// File of the scenario
        /// </summary>
        public string Scenario_File { get; set; }
        /// <summary>
        /// Map the scenario is ran on
        /// </summary>
        public string Map_Name { get; set; }
        /// <summary>
        /// List of problems in the scenario
        /// </summary>
        public List<Problem> problems = new List<Problem>();
        /// <summary>
        /// Add problem to the scenario
        /// </summary>
        /// <param name="newProblem">new problem to add to the scenario</param>
        public void addProblem(Problem newProblem)
        {
            problems.Add(newProblem);
        }
    }
}

﻿using RTSGame.Models.ExperimentModels;
using RTSGame.Models.SearchData;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;
using static RTSGame.Utilities;

namespace RTSGame.Models.SearchModels
{

    /// <summary>
    /// Controller that generates the search agents using the Search Agent Factory.
    /// </summary>
    public class AgentController
    {

        public List<SearchAgent> agents = new List<SearchAgent>();
        /// <summary>
        /// Specifys the search agent type from the app.config file. All agents are of one type, should be adjusted in the future.
        /// </summary>
        bool useDistanceCutoff;
        bool useStepCutoff;
        bool useThinkingTime = false;
        bool useGATLimit = false;

        private Utilities utils = new Utilities();
        internal AgentCreator AgentCreator = new AgentCreator();
        
        internal ExperimentParameters parameters;
        /// <summary>
        /// initialize agent controller.
        /// </summary>
        public AgentController(ExperimentParameters parameters)
        {
            this.parameters = parameters;
            useThinkingTime = parameters.experimentParameters.cutoffs.thinkingTime > 0;
            useGATLimit = parameters.experimentParameters.cutoffs.goalAchivementTime > 0;
            useStepCutoff = parameters.experimentParameters.cutoffs.steps > 0;
            useDistanceCutoff = parameters.experimentParameters.cutoffs.distance > 0;
            //Console.WriteLine("number of agents: {0}", parameters.algorithmParameters.numberOfAgents);
        }

        /// <summary>
        /// Public function that creates a search agent using the Search Agent Factory.
        /// </summary>
        public virtual void CreateSearchAgent(Tile start, Tile goal, int i, int width,int height, ExperimentParameters parameters)
        {
            throw new NotImplementedException("This should be implemented");            
        }
        
        /// <summary>
        /// Return the agent with the matching Id.
        /// </summary>
        /// <param name="id">id of the agent.</param>
        /// <returns>the matching agent</returns>
        public SearchAgent GetAgent(int id)
        {
            return agents[id];
        }
        /// <summary>
        /// Get all the agents for this controller
        /// </summary>
        /// <returns>list of all the agents.</returns>
        public List<SearchAgent> GetAgents()
        {
            return agents;
        }
        public virtual void GetAllAgentsMoves()
        {
            throw new NotImplementedException("This should be implemented");

        }
        public virtual void ExecuteAllAgentsMoves()
        {

            throw new NotImplementedException("This should be implemented");

        }
        internal int timeStep = 0;        
        /// <summary>
        /// agent that was updated
        /// </summary>
        internal int currentAgentBeingMoved = 0;
        /// <summary>
        /// Update the agents belonging to the controller.
        /// </summary>
        /// <returns>returns if any agent failed to reach the goal</returns>
        public void Update()
        {
            UpdateAllAgents();
            timeStep++;
        }
        public void IncermentTimeStep()
        {
            timeStep++;
        }
        public virtual void UpdateAllAgents()
        {
            throw new NotImplementedException("must update agents");
        }
        public bool AllAgentsFinishedSearching()
        {
            return agents.All(a => (a.atGoal() || AgentHasPassedCutoff(a))); 
        }
        public SearchAgent GetLastUpdatedAgent()
        {
            return agents[currentAgentBeingMoved];
        }
        public List<AgentInfo> GetAgentStats()
        {
            List<AgentInfo> records = new List<AgentInfo>();
            foreach (SearchAgent agent in agents)
            {
                Tile startLoc = agent.startingLocation;
                Tile lastSpot = agent.currentSpot;
                Tile goal = agent.goal;
                AgentInfo agentInfo = new AgentInfo();
                bool passedStepCutoff = AgentHasPassedStepCutoff(agent);
                bool passedDistanceCutoff = AgentHasPassedDistanceCutoff(agent);
                agentInfo.id = agent.id;
                agentInfo.distance = agent.distanceTravelled;
                agentInfo.gat = agent.goalAchivementTime;
                agentInfo.statesVisited = agent.statesVisited;
                agentInfo.numberOfStepsUntilAllAgentsReachedGoal = agent.numberOfSteps;
                agentInfo.reachedStepCutoff = passedStepCutoff;
                agentInfo.reachedDistanceCutoff = passedDistanceCutoff;
                agentInfo.atGoal = agent.atGoal();
                agentInfo.startX = startLoc.col;
                agentInfo.startY = startLoc.row;
                agentInfo.endX = lastSpot.col;
                agentInfo.endY = lastSpot.row;
                agentInfo.goalX = goal.col;
                agentInfo.goalY = goal.row;
                agentInfo.numberOfStepsUntilAgentReachedGoal = agent.stepsTakenToReachGoal;
                agentInfo.name = agent.name;
                agentInfo.maxThinkingTime = agent._thikingTimes.Count == 0 ? float.NaN : agent._thikingTimes.Max();
                agentInfo.averageThinkingTime = agent._thikingTimes.Count == 0 ? float.NaN : agent._thikingTimes.Average();
                agentInfo.thinkingTime = agent.thinkingTime;
                agentInfo.numberOfExpandedStates = agent.getNumberOfStatesExpanded();
                agentInfo.numberOfTouchedStates = agent.getNumberOfStatesTouched();
                agentInfo.heuristic = agent.gene.heuristic;
                agentInfo.collisions = agent.collisions;
                agentInfo.initialMoveCost = agent.firstMoveTime;
                records.Add(agentInfo);
            };
            return records;
        }

        public bool AgentHasPassedCutoff(SearchAgent agent)
        {
            bool passedCutoff = AgentHasPassedDistanceCutoff(agent) || AgentHasPassedStepCutoff(agent) || 
                AgentHasPassedGoalAchivementTimeCutoff(agent) || AgentHasPassedThinkingTimeCutoff(agent);
            return passedCutoff;
        }
        /// <summary>
        /// Checks if the agent has passed the step limit
        /// </summary>
        /// <param name="agent">agent in question</param>
        /// <returns>true if too many steps are taken</returns>
        private bool AgentHasPassedStepCutoff(SearchAgent agent)
        {
            if (useStepCutoff)
            {
                if (agent.numberOfSteps >= parameters.experimentParameters.cutoffs.steps)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Checks if the agent has passed the step limit
        /// </summary>
        /// <param name="agent">agent in question</param>
        /// <returns>true if too many steps are taken</returns> 
        private bool AgentHasPassedThinkingTimeCutoff(SearchAgent agent)
        {
            if (useThinkingTime)
            {
                if (agent.thinkingTime >= parameters.experimentParameters.cutoffs.thinkingTime)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Checks if the agent has passed the step limit
        /// </summary>
        /// <param name="agent">agent in question</param>
        /// <returns>true if too many steps are taken</returns>
        private bool AgentHasPassedGoalAchivementTimeCutoff(SearchAgent agent)
        {
            if (useGATLimit)
            {
                if (agent._totalTime.ElapsedMilliseconds >= parameters.experimentParameters.cutoffs.goalAchivementTime)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks if the agent has passed the distance cutoff
        /// </summary>
        /// <param name="agent">agent in question</param>
        /// <returns> true if distance is greater than cutoff</returns>
        private bool AgentHasPassedDistanceCutoff(SearchAgent agent)
        {
            if (useDistanceCutoff)
            {
                if (agent.distanceTravelled >= parameters.experimentParameters.cutoffs.distance)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Check if all the controllers agents are at the goal.
        /// </summary>
        /// <returns>True if all agents have reached the goal.</returns>
        private Boolean AllAgentsAtGoal()
        {
            return !agents.Any(a => !a.reachedGoal);
        }
    }
    
}

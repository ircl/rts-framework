﻿using RTSGame.Models.DeadlockReslovers;
using RTSGame.Models.SearchModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.SearchControllers
{
   
    public class BasicController :
        AgentController
    {
        
        private Utilities utils = new Utilities();
        AgentCreator AgentCreator = new AgentCreator();

        /// <summary>
        /// initialize agent controller.
        /// </summary>
        public BasicController(ExperimentParameters parameters):
            base(parameters)
        {
            this.parameters = parameters;
        }
        /// <summary>
        /// Creates a search agent
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goal"></param>
        /// <param name="i"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="parameters"></param>
        public override void CreateSearchAgent(Tile start, Tile goal, int i, int width, int height, ExperimentParameters parameters)
        {
            SearchAgent agent = AgentCreator.CreateAgent(start, goal, i, width, height, parameters.algorithmParameters[i]);
            start.addAgent(agent, agent.reachedGoal);
            agents.Add(agent);
        }
      
 
        /// <summary>
        /// Gets all the agents moves
        /// </summary>
        public override void GetAllAgentsMoves()
        {
            //agents.Sort((a, b) => (plans %(a.id+1)).CompareTo(plans% (b.id +1)));
            foreach (SearchAgent agent in agents)
            {
                if (!AgentHasPassedCutoff(agent))
                {
                    agent.planMoveList(timeStep);
                }
            }
        }
        /// <summary>
        /// Execute the agents moves
        /// </summary>
        public override void ExecuteAllAgentsMoves() // the execution step
        {
            foreach (SearchAgent agent in agents)
            {
               Tile moveToTile = agent.getNextNode();
               agent.crossEdge(moveToTile, utils.GetTravelCost(agent.currentSpot, moveToTile));
            }
        }
        public override void UpdateAllAgents()
        {
            GetAllAgentsMoves(); // the planning step.
            ExecuteAllAgentsMoves(); // execute the plan.
        }  
    }
}

﻿using RTSGame.Models.DeadlockReslovers;
using RTSGame.Models.SearchModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.SearchControllers
{
   
    public class FARAStarController :
        AgentController
    {
        
        private Utilities utils = new Utilities();
        ReservationTable reservationTable = new ReservationTable();    
        DeadlockController resolver = new DeadlockController();      
        /// <summary>
        /// initialize agent controller.
        /// </summary>
        public FARAStarController(ExperimentParameters parameters):
            base(parameters)
        {
            this.parameters = parameters;
        }
        /// <summary>
        /// Creates a search agent
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goal"></param>
        /// <param name="i"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="parameters"></param>
        public override void CreateSearchAgent(Tile start, Tile goal, int i, int width, int height, ExperimentParameters parameters)
        {
            SearchAgent agent = AgentCreator.CreateAgent(start,goal,i,width,height,parameters.algorithmParameters[i]);

            // Where we can add / swap the type of search agent
            start.addAgent(agent, agent.reachedGoal);
            //agent.SetReservationTable(reservationTable);
            agent.SetNumberOfReservations();
            agents.Add(agent);
        }
      
        /// <summary>
        /// Gets all the agents moves
        /// </summary>
        public override void GetAllAgentsMoves()
        {
            //agents.Sort((a, b) => (plans %(a.id+1)).CompareTo(plans% (b.id +1)));
            foreach (SearchAgent agent in agents)
            {
                if (!AgentHasPassedCutoff(agent))
                {
                    agent.planMoveList(timeStep);
                }
            }
        }
        Dictionary<int,bool> reserveSuccessful = new Dictionary<int, bool>();

        /// <summary>
        /// Has agents reserve their next moves
        /// </summary>
        public void ReserveNextMoves()
        {
            reserveSuccessful.Clear();
            foreach(SearchAgent agent  in agents){
                if (int.Parse(agent.gene.parameters["numberOfReservations"]) > 0)
                {
                    bool reserved = reservationTable.ReserveMoves(agent, timeStep);
                    reserveSuccessful.Add(agent.id,reserved);
                } else
                {
                    reserveSuccessful.Add(agent.id,true);
                }
            }           
        }
        /// <summary>
        /// Execute the agents moves
        /// </summary>
        public override void ExecuteAllAgentsMoves() // the execution step
        {
            agents.Sort((a,b) => (a.id % (1 +timeStep)).CompareTo(b.id % (1+timeStep)));
            foreach (SearchAgent agent in agents)
            {
                if (!AgentHasPassedCutoff(agent))
                {
                    Tile moveToTile = reserveSuccessful[agent.id] ?
                        agent.getNextNode():
                        agent.currentSpot; // if they were able to reserve their path we use their plan
                    // otherwise they wait.                    
                    if (moveToTile == agent.currentSpot || moveToTile.isAvailable()) // can move to spot
                    {
                        agent.crossEdge(moveToTile, utils.GetTravelCost(agent.currentSpot, moveToTile));
                    }
                    else if (agent.gene.parameters.ContainsKey("useDeadlockBreakingProcedure") &&
                        bool.Parse(agent.gene.parameters["useDeadlockBreakingProcedure"]) &&
                        resolver.IsDeadLocked(agent)) // is in a deadlock
                    {
                        int maxTrys = 20;
                        int i = 0;
                        while (resolver.IsDeadLocked(agent) && i < maxTrys)
                        {
                            resolver.ResolveDeadlock(agent, timeStep);
                            i++;
                        }
                    }
                    else if (moveToTile.GetAgent().atGoal()&&
                        agent.gene.parameters.ContainsKey("forceOffGoal") &&
                        bool.Parse(agent.gene.parameters["forceOffGoal"])) // not in a dead lock, but an agent is in the way on their goal.
                    {
                        SearchAgent blockingAgent = moveToTile.GetAgent();
                        blockingAgent.moveOffPath(timeStep);
                        agent.crossEdge(moveToTile, utils.GetTravelCost(agent.currentSpot, moveToTile));
                    } else
                    {
                        agent.crossEdge(moveToTile, utils.GetTravelCost(agent.currentSpot, agent.currentSpot));
                    }
                }
            }
        }
        public override void UpdateAllAgents()
        {
            GetAllAgentsMoves(); // the planning step.
            ReserveNextMoves(); // reserve step
            ExecuteAllAgentsMoves(); // execute the plan.
            timeStep++;
            ClearOldReservations();
        }

        private void ClearOldReservations()
        {
            reservationTable.ClearOldReservations(timeStep);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models.SearchControllers
{
    class Reservation
    {
        public int time;
        public Tile tile;
        public Reservation(Tile t, int time)
        {
            tile = t;
            this.time = time;
        }
    }
    public class ReservationTable
    {
        Dictionary<Tile, HashSet<int>> reservations = new Dictionary<Tile, HashSet<int>>();
        public bool isAvailable(Tile tile, int time)
        {
            if (reservations.ContainsKey(tile))
            {
                HashSet<int> reservedTimes = reservations[tile];
                return !reservedTimes.Contains(time);
            }
            else
            {
                return true;
            }

        }
        public void Clear()
        {
            reservations = new Dictionary<Tile, HashSet<int>>();
        }
        internal bool freeSpot(Tile t, int time)
        {
            if (!reservations.ContainsKey(t))
            {
                return false;
            } else
            {
                HashSet<int> reservedTimes = reservations[t];
                if (reservedTimes.Contains(time))
                {
                    reservedTimes.Remove(time);
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        public bool reserveSpot(Tile tile, int time)
        {
            if (!reservations.ContainsKey(tile))
            {
                reservations.Add(tile, new HashSet<int>() { time });
                return true;
            }
            else
            {
                HashSet<int> reservedTimes = reservations[tile];
                bool reserved = true;
                if (isAvailable(tile, time))
                {
                    reservedTimes.Add(time);
                }
                else
                {
                    reserved = false;
                }

                return reserved;
            }
        }
        internal void freeSpots(List<Reservation> myReservations)
        {
            foreach(Reservation r in myReservations)
            {
                freeSpot(r.tile, r.time);
            }
        }
        internal void reserveSpots(List<Reservation> myReservations)
        {
            foreach(Reservation r in myReservations)
            {
                reserveSpot(r.tile, r.time);
            }
        }
        public bool ReserveMoves(SearchAgent agent, int time)
        {
            //if (reservedUntil > time)
            //{
            //    return true;
            //}
            if (agent.currentSpot == agent.goal)
            {
                return false;
            }
            //myReservations.Clear();
            Dictionary<int,Tile> moves = agent.getMoves();
            if (moves == null || moves.Count == 0)
            {
                return false;
            }
            List<Reservation> myReservations = new List<Reservation>();
            Tile spot = agent.currentSpot;
            int marked = 0;
            int timeOffset = 1; // because the time is now we need an offset of one to know about reserving my next move in t + 1
            while (marked < Math.Min(agent.numberOfReservations, moves.Count))
            {
                if (!moves.ContainsKey(spot.id))
                {
                    break;
                }
                Tile nextTile = moves[spot.id];
                if (this.isAvailable(nextTile, time + timeOffset) &&
                    this.isAvailable(nextTile, time + timeOffset + 1))
                {
                    
                    Reservation resoNow = new Reservation(nextTile, time + timeOffset);
                    Reservation resoLeaving = new Reservation(nextTile, time + timeOffset + 1);
                    //reservationTable.reserveSpot(nextTile, time + timeOffset);
                    myReservations.Add(resoNow);
                    myReservations.Add(resoLeaving);
                    marked++;
                    spot = nextTile;
                }
                else
                {
                    myReservations.Clear();

                    break;
                }
                timeOffset++;
                if (spot == agent.goal)
                {
                    break;
                }
            }
            this.reserveSpots(myReservations);
            return true;
        }

        internal void ClearOldReservations(int time)
        {
            foreach(KeyValuePair<Tile,HashSet<int>> resPair in reservations)
            {
                resPair.Value.RemoveWhere(t => t < time);
            }
        }
    }
}

﻿using RTSGame.Models.SearchModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.SearchControllers
{
    
    class WindowedHeirchicalCOOPAStarController :
        AgentController
    {
        
        private Utilities utils = new Utilities();
        ReservationTable reservationTable = new ReservationTable();
        windowedHierarchicalAStarCreator windowedHierarchicalAStarCreator = new windowedHierarchicalAStarCreator();
        int windowSize = 0;
        int depth = 0;
        /// <summary>
        /// initialize agent controller.
        /// </summary>
        public WindowedHeirchicalCOOPAStarController(ExperimentParameters parameters):
            base(parameters)
        {
            this.windowSize = int.Parse(parameters.algorithmParameters[0].parameters["windowSize"]);
            this.parameters = parameters;
        }
        public int GetActionCost()
        {
            return 1;
        }
        public override void CreateSearchAgent(Tile start, Tile goal, int i, int width, int height, ExperimentParameters parameters)
        {
            WindowedHierarchicalAStar agent;
            String agentType = parameters.algorithmParameters[i].name;

            // Where we can add / swap the type of search agent
            agent = (WindowedHierarchicalAStar)windowedHierarchicalAStarCreator.FactoryMethod(i, goal, start, height, width, parameters.algorithmParameters[i]);
            agent.setReservationTable(reservationTable);
            start.addAgent(agent, agent.reachedGoal);
            agent.setThinkingTimeLimit(parameters.experimentParameters.cutoffs.goalAchivementTime);
            //goal.setGoal(i);
            agents.Add(agent);
        }
        int plans = 0;
        
        public override void UpdateAllAgents()
        {
            if (depth >= windowSize -1)
            {
                depth = 0;
                //reservationTable.Clear();
            }
            if (depth == 0)
            {
                plans++;
                agents.Sort((a, b) => (plans %(a.id+1)).CompareTo(plans% (b.id +1)));
                agents.Sort((a, b) => a.atGoal().CompareTo(b.atGoal()));
                foreach (SearchAgent agent in agents)
                {
                    WindowedHierarchicalAStar whaAgent = (WindowedHierarchicalAStar)agent;
                    if (!AgentHasPassedCutoff(agent))
                    {
                        whaAgent.planMoveList(timeStep);
                    }
                }
            }
            foreach(SearchAgent agent in agents)
            {
                WindowedHierarchicalAStar whcaAgent = (WindowedHierarchicalAStar)agent;
                if (!AgentHasPassedCutoff(agent))
                {
                    Tile moveToTile = whcaAgent.getNextNode();
                    if (moveToTile.getNeighbours().Contains(agent.currentSpot) ||
                        agent.currentSpot == moveToTile)
                    {
                        bool succesful = whcaAgent.crossEdge(moveToTile, utils.GetTravelCost(moveToTile, agent.currentSpot));
                        if (succesful)
                        {
                            continue;
                        }
                    }
                    ///////
                    // Search failed
                    ///////
                    if (whcaAgent.whcaParameters.enhanced)
                    {
                        reservationTable.Clear();
                        //timeStep = timeStep + (windowSize - 1 - depth);
                        foreach (SearchAgent resetAgent in agents)
                        {
                            ((WindowedHierarchicalAStar)resetAgent).clearReservations();
                        }
                        timeStep = 0;
                        depth = 0;
                        return;
                    }

                }
            }
            depth++;
        }       
    }
}

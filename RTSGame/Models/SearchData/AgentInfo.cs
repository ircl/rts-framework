﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models.SearchData
{
    public struct AgentInfo
    {
        /// <summary>
        /// Id of agent
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// distance the agent traveled
        /// </summary>
        public float distance { get; set; }
        /// <summary>
        /// Goal achivement time for the problem.
        /// </summary>
        public float gat { get; set; }
        /// <summary>
        /// List of ints for each state id the agent visits.
        /// </summary>
        public List<int> statesVisited { get; set; }
        /// <summary>
        /// Wether the agent reached the cutoff
        /// </summary>
        public bool reachedDistanceCutoff { get; set; }
        /// <summary>
        /// Reached step cutoff
        /// </summary>
        public bool reachedStepCutoff { get; set; }
        /// <summary>
        /// number of steps until all agents reached goal or gave up.
        /// </summary>
        public int numberOfStepsUntilAllAgentsReachedGoal { get; set; }
        /// <summary>
        /// number of steps for individual agent. staying in goal is not a step.
        /// </summary>
        public int numberOfStepsUntilAgentReachedGoal { get; set; }
        /// <summary>
        /// Max thinking time per move
        /// </summary>
        public float maxThinkingTime { get; set; }
        /// <summary>
        /// min thinking time per move
        /// </summary>
        public float averageThinkingTime { get; set; }
        /// <summary>
        /// is the agent at the goal
        /// </summary>
        public bool atGoal { get; set; }
        /// <summary>
        /// wherer did the agent start X
        /// </summary>
        public int startX { get; set; }
        /// <summary>
        /// where did the agent start Y
        /// </summary>
        public int startY { get; set; }
        /// <summary>
        /// where did the agent end X
        /// </summary>
        public int endX { get; set; }
        /// <summary>
        /// where did the agent end Y
        /// </summary>
        public int endY { get; set; }
        /// <summary>
        /// where was the agents goal X
        /// </summary>
        public int goalX { get; set; }
        /// <summary>
        /// where was the agents goal Y
        /// </summary>
        public int goalY { get; set; }
        public string name { get; set; }
        public int lookAhead { get; set; }
        public string heuristic { get; set; }
        public float thinkingTime { get; set; }
        public int collisions { get; set; }
        public float initialMoveCost { get; set; }
        public int numberOfExpandedStates { get; set; }
        public int numberOfTouchedStates { get; set; }
    }
}

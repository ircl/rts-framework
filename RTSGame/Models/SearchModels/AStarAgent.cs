﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

using RTSGame.Models.Configurations;

namespace RTSGame
{

    /// <summary>
    /// Sub class of Search agent that uses the A* Algorithm to  solve the problem.
    /// </summary>
    class AStarAgent : SearchAgent
    {

        
        
        public Dictionary<Int32, Tile> parentTileMapping = new Dictionary<int, Tile>();
        public Dictionary<Int32, Tile> principalVariation = new Dictionary<int, Tile>();
        private AlgorithmParameters config;
        private AStarParameters gene;
        private HashSet<int> prunedIds = new HashSet<int>();
        private struct AStarParameters
        {
            public decimal VisibleAgentDistance { get; set; }
            public bool SolvePathCompletely { get; set; }
            public int Lookaheads { get; set; }
            public bool UseFlowRestriction { get; set; }
        }
        private AStarParameters ParseAStarParameters()
        {
            return new AStarParameters {
                VisibleAgentDistance = decimal.Parse(config.parameters["visible agent distance"]),
                SolvePathCompletely = bool.Parse(config.parameters["solvePathCompletely"]),
                Lookaheads = bool.Parse(config.parameters["solvePathCompletely"]) ?
                    Int32.MaxValue : Math.Max(1, Int32.Parse(config.parameters["lookaheads"])),
                UseFlowRestriction = config.parameters.ContainsKey("useFlowRestricted") ?
                    bool.Parse(config.parameters["useFlowRestricted"]) :
                    false
            };
        }
        //public AStarParameters config;
        /// <summary>
        /// Initializes the A* algorithm.
        /// </summary>
        /// <param name="id">id of the agent</param>
        /// <param name="goal">goal of the agent.</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="height">height of map.</param>
        /// <param name="width">Width of map</param>
        /// <param name="name">name of the agent. </param>
        /// <param name="config">config</param>
        public AStarAgent(int id, Tile goal, Tile start, int height, int width, string name, AlgorithmParameters config) 
            : base(id, goal, start, height, width, name, config)
        {
            this.config = config;
            gene = ParseAStarParameters();
            this.name = "AStar_ignoreAgents(" + gene.VisibleAgentDistance.ToString() + ")";
            TileComparer tc = new TileComparer(this);
            openQueue = new SortedSet<Tile>(tc);
            //initializeAlgorithm();
        }

        private void InitializeAlgorithm()
        {
            //this.gCost = new float[h.GetLength(0), h.GetLength(1)];
            gCost.Clear();
            openQueue.Clear();
            //this.gCost[startingLocation.row, startingLocation.col] = 0;
            gCost.Add(currentSpot.id, 0);
            //this.openList = new List<Tile>();
            this.closedList = new HashSet<Tile>();
            this.parentTileMapping = new Dictionary<Int32, Tile>();
            this.principalVariation = new Dictionary<Int32, Tile>();
            //this.openList.Add(start);
            openQueue.Add(currentSpot);
        }
        public override void setNewGoal(Tile newGoal)
        {
            base.setNewGoal(newGoal);
            InitializeAlgorithm();
        }
        
        Tile bestOpenTile;
        Random random = new Random();
        private void ModH()
        {
            List<int> keys = new List<int>(hMap.Keys);

            foreach (int key in keys)
            {
                decimal additionalNoise = (decimal)random.NextDouble();
                hMap[key] += (additionalNoise * collisions);
            }
        }
        private void PrintValues()
        {
            Console.WriteLine("Printing RRA: ");
            foreach (Tile t in openQueue)
            {
                Console.WriteLine("F: " + getFCost(t) + "     G: " + gCost[t.id] + "     Row: " + t.row + "    col: " + t.col);
            }
        }
        public void SetUseFlow(bool use)
        {
            gene.UseFlowRestriction = use;
        }

        public bool AddToAStarChain(bool stopAtGoal = true)
        {
            if (openQueue.Count() == 0 )
            {
                //Console.WriteLine("no path available");
                return false;
            }
            //openList.Sort((t1,t2) => (getFCost(t1).CompareTo(getFCost(t2))));
            
            bestOpenTile = openQueue.First();
            //if (numberOfStatesExpanded < 4 && gene.lookaheads > 1)
            //{
            //    printValues();
            //}
            if (bestOpenTile == goal && stopAtGoal)
            {
                InvertAStarChain(goal);
                solvedPath = true;
                return false;
            }
            bool removeSuceeded = openQueue.Remove(bestOpenTile);
            if (!removeSuceeded)
            {
                return false;
            }
            // add the current square to the closed list
            numberOfStatesExpanded++;
            closedList.Add(bestOpenTile);
            // remove it from the open list
            //openList.Remove(bestOpenTile);
           
            List<Tile> bestTileNeighbours = bestOpenTile.getNonWallNeighbours(gene.UseFlowRestriction);
            List<Tile> openNeighbours = bestTileNeighbours;
            if (prunedIds.Count > 0)
            {
                bestTileNeighbours = bestTileNeighbours.Where(d => !prunedIds.Contains(d.id)).ToList();
            }
            if (0 != gene.VisibleAgentDistance)
            {
                if (-1 == gene.VisibleAgentDistance)
                {
                    openNeighbours = bestTileNeighbours.Where(d => d.isAvailable()).ToList();
                } else
                {
                    openNeighbours = bestTileNeighbours
                        .Where(
                        d =>
                            d == goal ||
                            utils.GetEuclideanDistance(d.col,d.row,currentSpot.col,currentSpot.row) > gene.VisibleAgentDistance ||
                            d.isAvailable()).ToList();
                }
            }
            //else
            //{
            //    openNeighbours = bestTileNeighbours.Where(d => (!currentSpot.getNeighbours().Contains(d))
            //        || (currentSpot.getNeighbours().Contains(d) && d.isAvailable())).ToList();
            //}

            decimal g; g = this.gCost[bestOpenTile.id];
            // Expand the best tile
            numberOfStatesTouched += openNeighbours.Count;
            foreach (Tile connectingNode in openNeighbours)
            {
                // if this adjacent square is already in the closed list, ignore it
                if (closedList.Contains(connectingNode))
                {
                    continue;
                }

                // if it's not in the open list...
                decimal travelCost = utils.GetTravelCost(bestOpenTile, connectingNode);
                decimal tenativeG = g + travelCost;
                if (!gCost.ContainsKey(connectingNode.id))
                {
                    // compute its score, set the parent
                    gCost.Add(connectingNode.id, tenativeG);
                    parentTileMapping.Add(connectingNode.id, bestOpenTile);
                    // and add it to the open list
                    openQueue.Add(connectingNode);
                }
                else
                {
                    // test if using the current G score makes the adjacent square's F score
                    // lower, if yes update the parent because it means it's a better path

                    if (tenativeG < gCost[connectingNode.id])
                    {

                        openQueue.Remove(connectingNode);
                        gCost[connectingNode.id] = g + travelCost;
                        parentTileMapping[connectingNode.id] = bestOpenTile;
                        openQueue.Add(connectingNode);
                    }
                }
            }
            return true;
        }
        public bool solvedPath;
        /// <summary>
        /// Solves the path using A* and stores the principal variation chain. NOTE: Not sufficiently tested and may contain bugs.
        /// </summary>
        public void SolvePath()
        {
            solvedPath = false;

            while (this.openQueue.Count > 0 && !solvedPath)
            {
                bool finishedExpansion = AddToAStarChain();
                if (!finishedExpansion)
                {
                    break;
                }
                
            }
            if (!solvedPath)
            {
                //Console.WriteLine("no path available");
                //invertAStarChain();
            }
        }
        public Dictionary<Int32,Tile> InvertAStarChain(Tile tile)
        {
            principalVariation.Clear();
            while (true)
            {
                if (!parentTileMapping.ContainsKey(tile.id))
                {
                    break;
                }

                Tile parentTile = parentTileMapping[tile.id];
                this.principalVariation.Add(parentTile.id, tile);
                if (parentTile == currentSpot)
                {
                    break;
                }
                tile = parentTile;
            }
            principalVariation.Add(this.goal.id, this.goal);
            return principalVariation;
        }
        public List<Tile> moves = new List<Tile>();
        public List<Tile> CreateMoveList(Tile tile)
        {
            InvertAStarChain(tile);
            Tile nextT = tile;
            Tile currentT = tile;
            moves.Clear();           
            while (true)
            {
                currentT = nextT;
                if (!principalVariation.ContainsKey(nextT.id))
                {
                    break;
                }
                nextT = principalVariation[currentT.id];
                if (currentT.getNeighbours().Contains(nextT))
                {


                    moves.Add(nextT);
                    if (nextT == goal)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            return moves;
        }
        public int lookAheadsPerformed = 0;

        /// <summary>
        /// Returns the next node in the principle variation chain.
        /// </summary>
        /// <returns>Next node in the principal variation chain</returns>
        public override Tile getNextNode()
        {
            if (gene.SolvePathCompletely && !principalVariation.ContainsKey(this.currentSpot.id))
            {
                SolvePath();
            } else if (!principalVariation.ContainsKey(this.currentSpot.id))
            {
                if (!config.singleExpansionPerUpdate)
                {
                    lookAheadsPerformed = 0;

                    while (lookAheadsPerformed < gene.Lookaheads)
                    {
                        AddToAStarChain();
                        lookAheadsPerformed++;
                    }
                } else
                {
                    AddToAStarChain();
                    lookAheadsPerformed++;
                }
            }

            if (principalVariation.ContainsKey(this.currentSpot.id))
            {
                return principalVariation[this.currentSpot.id];
            }
            else
            {
                return currentSpot;
            }
        }
        public void RestartPlanning()
        {
            gCost.Clear();
            gCost.Add(currentSpot.id, 0);
            ModH();

            this.openQueue.Clear();
            this.closedList.Clear();
            this.parentTileMapping.Clear();
            this.principalVariation.Clear();
            this.openQueue.Add(currentSpot);
            if (gene.SolvePathCompletely)
            {
                SolvePath();
            }
        }

        internal void SetPrunedNodes(HashSet<int> expandedTiles)
        {

            prunedIds = expandedTiles;
        }
    }
    /// <summary>
    /// A star factory creator.
    /// </summary>
    class AStarCreator : SearchCreator
    {
        /// <summary>
        /// Concrete a star factory. A star agents solve their problem before returning the agent.
        /// </summary>
        /// <param name="id">id of the agent.</param>
        /// <param name="goal">goal of the agent</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="height">height of map.</param>
        /// <param name="width">width of map.</param>
        /// <param name="parameters">params of algorithm</param>
        /// <returns></returns>
        public override SearchAgent FactoryMethod(int id, Tile goal, Tile start, int height, int width, AlgorithmParameters parameters)
        {
            AStarAgent agent = new AStarAgent(id, goal, start,height,width , "AStar", parameters);
           
            return agent;
        }
    }
}

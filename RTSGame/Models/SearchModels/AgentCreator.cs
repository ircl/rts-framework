﻿using RTSGame.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.SearchModels
{
    public enum AgentType
    {
        FAR,
        AStar,
        LRAStar,
        RTAAStar,
        uLRTAStar,
        WHCAStar,
        RRAStar
    }
    class AgentCreator
    {
        private SearchCreator ultraCreator = new uLRTACreator();
        private SearchCreator aStarCreator = new AStarCreator();
        private SearchCreator RTTAStarCreator = new RealTimeAdaptiveAStarFactory();
        private SearchCreator localRepairAStarCreator = new localRepairAStarCreator();
        private SearchCreator FARCreator = new FARCreator();
        private SearchCreator newSearchCreator = new newSearchCreator();
        public AgentCreator()
        {

        }
        internal SearchAgent CreateAgent(Tile start, Tile goal, int i, int width, int height, AlgorithmParameters algorithmParameters)
        {
            SearchAgent agent;
            AgentType type = (AgentType)Enum.Parse(typeof(AgentType), algorithmParameters.name);
            switch (type)
            {
                case (AgentType.uLRTAStar):
                    agent = ultraCreator.FactoryMethod(i, goal, start, height, width, algorithmParameters);
                    break;
                case (AgentType.AStar):
                     agent = aStarCreator.FactoryMethod(i, goal, start, height, width, algorithmParameters);
                    break;
                case (AgentType.LRAStar):
                    agent = localRepairAStarCreator.FactoryMethod(i, goal, start, height, width, algorithmParameters);
                    break;

                case (AgentType.RTAAStar):
                    agent = RTTAStarCreator.FactoryMethod(i, goal, start, height, width, algorithmParameters);
                    break;
                case (AgentType.FAR):
                    agent = FARCreator.FactoryMethod(i, goal, start, height, width, algorithmParameters);
                    break;
                default:
                    throw new ArgumentException("Agent type not recognized by the agent controller");
            }
            return agent;
        }
    }
}

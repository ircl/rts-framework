﻿
using RTSGame.Models.Configurations;
using RTSGame.Models.SearchControllers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using static RTSGame.MainController;
using static RTSGame.ParameterConfiguration;

namespace RTSGame
{
   
    /// <summary>
    /// uLRTA style search agent with genetic properties.
    /// </summary>
    public class FARAgent : SearchAgent
    {
        /// <summary>
        /// Defines the gene for the FAR agent (see https://www.aaai.org/Papers/ICAPS/2008/ICAPS08-047.pdf for details).
        /// </summary>
        struct FARParameters
        {
           public int NumberOfReservations { get; set; }
           public bool UseFlow { get; set; }
        }
      
        /// <summary>
        /// The agents gene.
        /// </summary>
        FARParameters FARGene;
        AlgorithmParameters parameters;
        AlgorithmParameters aStarParameters;
        Tile originalGoal;
        /// <param name="id">id of the agent</param>
        /// <param name="goal">goal for the agent.</param>
        /// <param name="start">starting location for the agent.</param>
        /// <param name="height">height.</param>
        /// <param name="width">width.</param>
        /// <param name="type">type of agent.</param>
        /// <param name="parameters">gene params</param>
        public FARAgent(int id, Tile goal, Tile start, int height, int width, string type, AlgorithmParameters parameters)
            : base(id, goal, start, height, width, type, parameters)
        {
            this.parameters = parameters;
            FARGene = ParseParameters(parameters);
            originalGoal = goal;
            currentGoal = goal;
            GeneToString(FARGene);
            aStarParameters = new AlgorithmParameters
            {
                name = "astar",
                heuristic = parameters.heuristic,
                parameters = new Dictionary<string, string>()
                {
                    {"visible agent distance", parameters.parameters.ContainsKey("visible agent distance") ?
                        parameters.parameters["visible agent distance"] : "0" },
                    {"solvePathCompletely","true" },
                    {"lookaheads", "1"},// not used as the path is solved completely
                    {"useFlowRestricted", FARGene.UseFlow.ToString()}
                }
            };
            this.name = "FAR(" + FARGene.NumberOfReservations.ToString() + ")" + 
                (FARGene.UseFlow ? "_flow" : "_noFlow");
            InitializeValues();
        }
        
        /// <summary>
        /// Override the base setNewGoal.
        /// </summary>
        /// <param name="newGoal">New goal for the agent</param>
        public override void setNewGoal(Tile newGoal)
        {
            base.setNewGoal(newGoal);
            InitializeValues();
        }
        Tile currentGoal;
        private void setCurrentGoal(Tile tempGoal)
        {
            this.currentGoal = tempGoal;
            InitializeValues();
        }
        internal override void collision()
        {
            base.collision();
        }
        public override Dictionary<int, Tile> getMoves()
        {
            createMoveList();
            return nextMoves;
        }
        public override bool moveOffPath(int time)
        {
            List<Tile> possilbeMoves = this.currentSpot.getNonWallNeighbours(false)
                .Where(t => t.isAvailable()).ToList();
            Tile moveToTile;
            //if (possilbeMoves.Any(t => reservationTable.isAvailable(t, time + 1))) // check if there is a non-reserved spot
            //{
            //    moveToTile = possilbeMoves.First(t => reservationTable.isAvailable(t, time + 1));
            //}
            if (possilbeMoves.Count() > 0)
            { // move
                moveToTile = possilbeMoves.First(t => t.isAvailable());
            }
            else
            {
                return false;
            }
            Tile forcedOffTile = this.currentSpot;
            crossEdge(moveToTile, utils.GetTravelCost(this.currentSpot, moveToTile));
            if (originalGoalPath.ContainsKey(forcedOffTile.id))
            {
                setCurrentGoal(forcedOffTile);
            } else
            {
                List<Tile> vals = originalGoalPath.Values.ToList();
                decimal minDist = vals.Min(t => utils.GetEuclideanDistance(t.col, t.row, forcedOffTile.col, forcedOffTile.row));
                setCurrentGoal(vals.First(t=>
                    minDist == utils.GetEuclideanDistance(t.col, t.row, forcedOffTile.col, forcedOffTile.row)));
            }
            planMoveList(time, true);
            return true;
        }
        /// <summary>
        /// Interweaves the planning and execution of the agent. 
        /// </summary>
        /// <returns>the tile the agent will try to move to.</returns>
        public override Tile getNextNode()
        {
            if (originalGoalPath != null &&
                originalGoalPath.ContainsKey(currentSpot.id))
            {
                currentGoal = originalGoal;
                nextMoves = originalGoalPath;
                return originalGoalPath[currentSpot.id];
            }
            if (nextMoves.ContainsKey(currentSpot.id))
            {
                return nextMoves[currentSpot.id];
            } else
            {
                return currentSpot;
            }
        }
        Dictionary<int, Tile> originalGoalPath = null;
        public override void createMoveList(int timeStep = 0, bool forceReplan = false)
        {
            //MarkNextKMoves(timeStep);
            if (nextMoves == null)
            {
                AStarAgent aStarAgent;
                aStarAgent = new AStarAgent(id, currentGoal, startingLocation, height, width, "FAR", aStarParameters);
                aStarAgent.setCurrentSpot(this.currentSpot);
                aStarAgent.setNewGoal(currentGoal);
                aStarAgent.SolvePath();
                if (aStarAgent.principalVariation.Count == 0)
                {
                    aStarAgent.SetUseFlow(false);
                    aStarAgent.setCurrentSpot(this.currentSpot);
                    aStarAgent.setNewGoal(currentGoal);
                    aStarAgent.SolvePath();
#if DEBUG
                    Console.WriteLine("Failed to solve with flow");
#endif
                }
                numberOfStatesExpanded += aStarAgent.numberOfStatesExpanded;
                numberOfStatesTouched += aStarAgent.numberOfStatesTouched;
                nextMoves = aStarAgent.principalVariation;
                if(currentGoal == originalGoal)
                {
                    originalGoalPath = aStarAgent.principalVariation;
                }
            }
            //nextMoves = aStarAgent.principalVariation;
        }
        //////////////////////////////
        /// PRIVATE FUNCTIONS
        /////////////////////////////
        private void InitializeValues()
        {
            mostUpdatedValue = 0.0f;
            getHeuristic(this.currentSpot);
            reservedUntil = -1;
            nextMoves = null;
        }


        /// <summary>
        /// Convert the gene to human readable string
        /// </summary>
        /// <param name="gene">gene</param>
        private void GeneToString(FARParameters gene)
        {
            String name = "FAR";
            this.name = name;
        }
        /// <summary>
        /// Parse the stringified data
        /// </summary>
        /// <param name="parameters">Parameters of the agent</param>
        /// <returns>converted and typed gene</returns>
        private FARParameters ParseParameters(AlgorithmParameters parameters)
        {
            bool useFlow = parameters.parameters.ContainsKey("useFlow") ?
                    bool.Parse(parameters.parameters["useFlow"]) :
                    true;
            return new FARParameters
            {
                NumberOfReservations = parameters.parameters.ContainsKey("numberOfReservations") ?
                   int.Parse(parameters.parameters["numberOfReservations"]) :
                   1,
                UseFlow = useFlow
            };
        }
    }
    /// <summary>
    /// uLRTA Factory
    /// </summary>
    class FARCreator : SearchCreator
    {
        /// <summary>
        /// Concrete uLRTAa star factory. 
        /// </summary>
        /// <param name="id">id of the agent.</param>
        /// <param name="goal">goal of the agent</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="height">height of map.</param>
        /// <param name="width">width of map.</param>
        /// <param name="parameters">params</param>
        /// <returns>Returns a new uLRTA agent</returns>
        public override SearchAgent FactoryMethod(int id, Tile goal, Tile start, int height, int width, AlgorithmParameters parameters)
        {
            return new FARAgent(id, goal, start, height, width, "FAR",  parameters);
        }
    }
}

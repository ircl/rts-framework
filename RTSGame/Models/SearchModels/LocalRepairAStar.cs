﻿using RTSGame.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.SearchModels
{
    class LocalRepairAStarAgent : AStarAgent
    {
        private int replans = 0;
        /// <summary>
        /// Initializes the A* algorithm.
        /// </summary>
        /// <param name="id">id of the agent</param>
        /// <param name="goal">goal of the agent.</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="h0">initial heuristic of the agent.</param>
        /// <param name="name">name of the agent. </param>
        /// <param name="config">config</param>
        public LocalRepairAStarAgent(int id, Tile goal, Tile start, int height, int width, string name, AlgorithmParameters config) 
            : base(id, goal, start, height, width, name, config)
        {
            this.name = "LRA*";
            this.agentType = "LRA*";
            this.name = "LRA_ignoreAgents(" + config.parameters["visible agent distance"] + ")";
            base.setNewGoal(goal);
        }

        /// <summary>
        /// Returns the next node in the principle variation chain.
        /// </summary>
        /// <returns>Next node in the principal variation chain</returns>
        public override Tile getNextNode()
        {
          
            if (principalVariation.ContainsKey(this.currentSpot.id))
            {
                Tile nextSpot = principalVariation[this.currentSpot.id];
                
                if (!nextSpot.isAvailable())
                {
                    this.RestartPlanning();
                    replans++;
                    collisions++;
                    return this.currentSpot;
                }
                else
                {
                    return principalVariation[this.currentSpot.id];
                }
            }
            else
            {
                this.RestartPlanning();
                replans++;
                collisions++;
                // if there is no path then there would not be a principalVariation.
                if (principalVariation.ContainsKey(this.currentSpot.id))
                {
                    return principalVariation[currentSpot.id];
                }
                else
                {
                    return this.currentSpot;

                }
            }

        }
    }
    /// <summary>
    /// A star factory creator.
    /// </summary>
    class localRepairAStarCreator : SearchCreator
    {
        /// <summary>
        /// Concrete a star factory. A star agents solve their problem before returning the agent.
        /// </summary>
        /// <param name="id">id of the agent.</param>
        /// <param name="goal">goal of the agent</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="h">heuristic of the agent.</param>
        /// <param name="h0">initial heuristic of the agent.</param>
        /// <param name="parameters">params of algorithm</param>
        /// <returns></returns>
        public override SearchAgent FactoryMethod(int id, Tile goal, Tile start, int height, int width, AlgorithmParameters parameters)
        {
            LocalRepairAStarAgent agent = new LocalRepairAStarAgent(id, goal, start, height, width, "LocalRepairAStar", parameters);
            //agent.solvePath();
            return agent;
        }
    }
}

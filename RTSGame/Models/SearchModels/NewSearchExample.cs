﻿using RTSGame.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.SearchModels
{
    class NewSearchExample : SearchAgent
    {
        public NewSearchExample(int id, Tile goal, Tile start, int height, int width, AlgorithmParameters parameters) 
            : base(id, goal, start, height, width, "My Search Example", parameters)
        {
            // ADD YOUR INITIALIZATION CODE HERE
        }
       
        /// <summary>
        /// Return the next location that the agent wishes to move to.
        /// </summary>
        /// <returns>the next tile the agent wish to move to</returns>
        public override Tile getNextNode()
        {
            // Override the base class with your algorithm here
            // This return the next location the agent wishes to move to.
            // DO NOT SET THE AGENTS LOCATION HERE
            return this.currentSpot;
        }
    }
    /// <summary>
    /// new search Factory
    /// </summary>
    class newSearchCreator : SearchCreator
    {
        /// <summary>
        /// Concrete factory for a new search algorith. 
        /// Creates your new algorithm
        /// </summary>
        /// <param name="id">id of the agent.</param>
        /// <param name="goal">goal of the agent</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="h">heuristic of the agent.</param>
        /// <param name="h0">initial heuristic of the agent.</param>
        /// <param name="cutoff">Max travel distance</param>
        /// <param name="parameters">params of algorithm</param>
        
        /// <returns>Returns a new agent</returns>
        public override SearchAgent FactoryMethod(int id, Tile goal, 
            Tile start, int height, int width, AlgorithmParameters parameters)
        {
            return new NewSearchExample(id, goal, start, height, width,  parameters);
        }
    }
}

﻿using RTSGame.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models.SearchModels
{
    struct RTAAGene
    {
        public int Movements { get; set; }
        public int Lookahead { get; set; }
        public decimal VisibleAgentRange { get; set; }
    }
    class RealTimeAdaptiveAStar : SearchAgent
    {
        AlgorithmParameters aStarParameters ;
        HashSet<int> expandedTiles = new HashSet<int>();
        AlgorithmParameters parameters;
        RTAAGene rTAAGene = new RTAAGene();

        private void GeneToString(AlgorithmParameters gene)
        {
            //String[] lop = { "min", "avg", "median", "max" };
            //String name = string.Format("({0:0.00} * {1}_{{{2:0.00}}}({3:0.00} * c + h){4}{5})({6})",
            //    gene.weight,
            //    lop[gene.learningOperator],
            //    gene.beamWidth,
            //    gene.wc,
            //    gene.depressionAvoidance ? "+da" : "",
            //    gene.markExpendable ? "+E" : "",
            //    gene.lookahead);
            
            this.name = "RTAA*( "+ rTAAGene.Lookahead + ")";
        }
        /// <summary>
        /// Creates a uLRTA* agent
        /// </summary>
        /// <param name="id">id of the agent</param>
        /// <param name="goal">goal for the agent.</param>
        /// <param name="start">starting location for the agent.</param>
        /// <param name="height">height.</param>
        /// <param name="width">width.</param>
        /// <param name="type">type of agent.</param>
        /// <param name="parameters">gene params</param>
        public RealTimeAdaptiveAStar(int id, Tile goal, Tile start, int height, int width, string type, AlgorithmParameters parameters) 
            :base(id, goal, start, height, width, type, parameters)
        {
            this.parameters = parameters;
            rTAAGene.Lookahead = int.Parse(gene.parameters["lookahead"]);
            rTAAGene.Movements = parameters.parameters.ContainsKey("movement") ? int.Parse(parameters.parameters["movement"]) : 1;
            rTAAGene.VisibleAgentRange = parameters.parameters.ContainsKey("visibleAgentRange") ? 
                decimal.Parse(parameters.parameters["visibleAgentRange"]) : 0;
            //gene = parseParameters(parameters);
            aStarParameters = new AlgorithmParameters
            {
                name = "astar",
                heuristic = "octile",
                parameters = new Dictionary<string, string>()
                {
                    {"visible agent distance", rTAAGene.VisibleAgentRange.ToString() },
                    {"solvePathCompletely","false" },
                    {"lookaheads", parameters.parameters["lookahead"]}
                }
            };
            GeneToString(gene);
            //this.weight = gene.weight;
            //this.wc = gene.wc;
            //this.da = gene.depressionAvoidance;
            //this.markExpendable = gene.markExpendable;
            //this.lop = gene.learningOperator;
            //this.beamWidth = gene.beamWidth;
            //this.lookahead = Math.Max(1, gene.lookahead);
            //this.filterAgents = gene.ignoreOtherAgentsFromPlanning;
            InitializeValues();
        }
        private void InitializeValues()
        {
            expandedTiles.Clear();
        }
        /// <summary>
        /// Performs the RTAA* style update procedure, number of lookaheads defined in aStarParameters.
        /// </summary>
        /// <param name="lookAheadFromTile">Tile to be updated.</param>
        /// <returns>The tile the agent wishes to move to</returns>
        private Tile RTAALookahead(Tile lookAheadFromTile)
        {

            AStarAgent aStarAgent = new AStarAgent(0, goal, lookAheadFromTile, height, width, "AStar", aStarParameters);
            aStarAgent.setNewH(hMap);
            aStarAgent.getNextNode();
            HashSet<Tile> closedList = aStarAgent.closedList;

            Tile nodeToBeExpanded;
            if (aStarAgent.solvedPath)
            {
                nodeToBeExpanded = goal;
            } else
            {
                if (aStarAgent.openQueue.Count > 0)
                {
                    nodeToBeExpanded = aStarAgent.openQueue.First();
                } else
                {
                    return currentSpot;
                }
            }

            foreach (Tile updateTile in closedList)
            {
                decimal gToBeExpanded = aStarAgent.gCost[nodeToBeExpanded.id];
                decimal hToBeExpaned = aStarAgent.getHeuristic(nodeToBeExpanded);
                decimal gToBeUpdated = aStarAgent.gCost[updateTile.id];
                //get the heuristic update value
                decimal newH = gToBeExpanded +
                        hToBeExpaned -
                        gToBeUpdated;
                //float prevH = getHeuristic(updateTile);
                updateHValue(updateTile, newH);
                //h[updateTile.row, updateTile.col] = newH;
            }
            aStarAgent.InvertAStarChain(nodeToBeExpanded);
            Tile bestTile = aStarAgent.principalVariation[this.currentSpot.id];
            return bestTile;
        }
        public override Tile getNextNode()
        {
            return this.RTAALookahead(this.currentSpot);
        }
    }
    /// <summary>
    /// RTAA* Factory
    /// </summary>
    class RealTimeAdaptiveAStarFactory : SearchCreator
    {
        /// <summary>
        /// Concrete RTAA* star factory. 
        /// </summary>
        /// <param name="id">id of the agent.</param>
        /// <param name="goal">goal of the agent</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="height">height of map.</param>
        /// <param name="width">width of map.</param>
        /// <param name="parameters">params</param>
        /// <returns>Returns a new RTAA* agent</returns>
        public override SearchAgent FactoryMethod(int id, Tile goal, Tile start, int height, int width, AlgorithmParameters parameters)
        {
            return new RealTimeAdaptiveAStar(id, goal, start, height, width, "RTAA*", parameters);
        }
    }
}

﻿using RTSGame.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models.SearchModels
{
    internal class RRATileComparer : IComparer<Tile>
    {
        ReverseResumableAStar agent;
        public RRATileComparer(ReverseResumableAStar agent)
        {
            this.agent = agent;
        }
        public int Compare(Tile x, Tile y)
        {
            if (x == y)
                return 0;
            //first by f
            var xf = agent.getFCost(x);
            var yf = agent.getFCost(y);
            var xg = agent.gCost[x.id];
            var yg = agent.gCost[y.id];
            int result = xf.CompareTo(yf);

            //then g
            if (result == 0)
                result = -xg.CompareTo(yg);
            if (result == 0)
                result = x.row.CompareTo(y.row);
            if (result == 0)
                result = x.col.CompareTo(y.col);
            if (result == 0)
                result = x.id.CompareTo(y.id);

            return result;
        }
    }
   
    public class ReverseResumableAStar 
    {
        public Dictionary<int,decimal> gCost = new Dictionary<int, decimal>();
        private Dictionary<int, decimal> hMap = new Dictionary<int, decimal>();
        public int numberOfStatesTouched = 0;
        public int numberOfStatesExpanded = 0;
        Utilities utils = new Utilities();
        public SortedSet<Tile> openQueue;
        //public SimplePriorityQueue<Tile> closedQueue = new SimplePriorityQueue<Tile>();
        //public List<Tile> openList;
        public HashSet<Tile> closedList;
        private Tile goal;
        private Tile start;
        private string heuristicType = "octile";
        private bool saveMemory = false;

        public Dictionary<Int32, Tile> parentTileMapping;
        public Dictionary<Int32, Tile> principalVariation;
        /// <summary>
        /// Initializes the A* algorithm.
        /// </summary>
        /// <param name="id">id of the agent</param>
        /// <param name="goalTile">goal of the agent.</param>
        /// <param name="startTile">start location of the agent.</param>
        /// <param name="height">height of map.</param>
        /// <param name="width">Width of map</param>
        /// <param name="name">name of the agent. </param>
        /// <param name="config">config</param>
        public ReverseResumableAStar(int id, Tile startTile, Tile goalTile, int height, 
            int width, string name, AlgorithmParameters config)
        {
            this.goal = startTile;
            this.start = goalTile;
            
            gCost[start.id] = 0;
            openQueue = new SortedSet<Tile>(new RRATileComparer(this));
            openQueue.Add(start);
            this.parentTileMapping = new Dictionary<Int32, Tile>();
            this.principalVariation = new Dictionary<Int32, Tile>();

            closedList = new HashSet<Tile>();
           // ResumeRRAStar(goal);
        }
        public List<Tile> moves;
        public Dictionary<Int32, Tile> invertAStarChain(Tile tile)
        {
            principalVariation.Clear();
            while (true)
            {
                if (!parentTileMapping.ContainsKey(tile.id))
                {
                    break;
                }

                Tile parentTile = parentTileMapping[tile.id];
                this.principalVariation.Add(parentTile.id, tile);
                if (parentTile == goal)
                {
                    break;
                }
                tile = parentTile;
            }
            if (!principalVariation.ContainsKey(this.goal.id))
            {
                principalVariation.Add(this.goal.id, this.goal);
            }
            moves = createListFromPrincipleVariation();
            return principalVariation;
        }
        /// <summary>
        /// Returns the heuristic of the tile
        /// </summary>
        /// <param name="tile">tile in question</param>
        /// <returns>heuristic of the tile to the goal</returns>
        private decimal getHeuristic(Tile tile)
        {
            if (tile == goal)
            {
                return 0;
            }
            else
            {
                if (saveMemory)
                {
                    return createHeuristic(tile);
                } else
                {
                    if (!hMap.ContainsKey(tile.id))
                    {
                        hMap.Add(tile.id, createHeuristic(tile));
                    }
                    return hMap[tile.id];
                }
            }
        }
        private decimal createHeuristic(Tile tile)
        {
            
            return utils.GetHeuristic(tile, goal, heuristicType);
            
        }
        public decimal getFCost(Tile t)
        {
            return (gCost[t.id] + getHeuristic(t));
        }
        private void printValues()
        {
            Console.WriteLine("Printing RRA: ");
            foreach(Tile t in openQueue)
            {
                Console.WriteLine("F: " + getFCost(t) + "     G: " + gCost[t.id] + "     Row: " + t.row + "    col: " + t.col);
            }
        }
        private List<Tile> createListFromPrincipleVariation()
        {
            List<Tile> moves = new List<Tile>();
            Tile nextTile = goal;
            while (parentTileMapping.ContainsKey(nextTile.id))
            {
                nextTile = parentTileMapping[nextTile.id];
                moves.Add(nextTile);
                if (nextTile == start)
                {
                    break;
                }
            }
            return moves;
        }
        private bool firstSearch = true;
        public bool ResumeRRAStar(Tile Node)
        {
            while (this.openQueue.Count > 0)
            {
                //if (numberOfStatesExpanded < 3)
                //{
                //    printValues();
                //}
                Tile P = openQueue.First();
                
                openQueue.Remove(P);  
                
                closedList.Add(P);
                numberOfStatesExpanded++;
                List<Tile> neighbours = P.getNonWallNeighbours();
                decimal g = gCost[P.id];
                numberOfStatesTouched += neighbours.Count;
                foreach (Tile Q in neighbours)
                {
                    // if it's not in the open list...
                    decimal travelCost = utils.GetTravelCost(Q, P);
                    decimal tenativeG = g + travelCost;
                    if (closedList.Contains(Q))
                    {
                        continue;
                    }
                    //gCost[Q.row, Q.col] = gCost[P.row, P.col] + Cost(P, Q);
                    bool added = false;
                    if (!gCost.ContainsKey(Q.id))
                    {
                        gCost.Add(Q.id, tenativeG);
                        added = openQueue.Add(Q);
                        parentTileMapping.Add(Q.id,P);
                        if (!added)
                        {
                            Console.WriteLine("Failed to add to open list");
                        }
                    }
                   
                    if (tenativeG < gCost[Q.id])
                    {
                        openQueue.Remove(Q);
                        this.gCost[Q.id] = tenativeG;
                        openQueue.Add(Q);
                        parentTileMapping[Q.id] = P;
                    }
                }
                if (P == Node)
                {
                    //printValues();
                    if (firstSearch)
                    {
                        firstSearch = false;
                        invertAStarChain(P);
                    }
                    return true;
                }
            }
            return false;
        }

        public decimal AbstractDist(Tile Node)
        {
            if (this.closedList.Contains(Node))
            {
                return this.gCost[Node.id];
            } else
            {
                if (ResumeRRAStar(Node))
                {
                    return this.gCost[Node.id];
                } else
                {
                    return decimal.MaxValue;
                }
            }
        }
    }
}

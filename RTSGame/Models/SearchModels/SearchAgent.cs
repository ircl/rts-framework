﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using static RTSGame.ParameterConfiguration;
using RTSGame.Models.Configurations;
using RTSGame.Models.SearchModels;
using RTSGame.Models.SearchControllers;

namespace RTSGame
{
    internal class TileComparer: IComparer<Tile>
    {
        SearchAgent agent;
        public TileComparer(SearchAgent agent)
        {
            this.agent = agent;
        }
        public int Compare(Tile x, Tile y)
        {

            if (x.id == y.id)
                return 0;
            //first by f
            int result = agent.getFCost(x).CompareTo(agent.getFCost(y));

            //then g
            if (result == 0)
                result = -(agent.gCost[x.id].CompareTo(agent.gCost[y.id]));
            if (result == 0)
                result = x.row.CompareTo(y.row);
            if (result == 0)
                result = x.col.CompareTo(y.col);
            if (result == 0)
                result = x.id.CompareTo(y.id);
            return result;
        }
    }

    /// <summary>
    /// Abstract base class that is overridden by concrete search algorithms such as uLRTA, and a star
    /// </summary>
    public abstract class SearchAgent
    {
        /// <summary>
        /// Tile for the goals starting location
        /// </summary>
        public Tile goal;
        /// <summary>
        /// The current tile the agent is residing in
        /// </summary>
        public Tile currentSpot;
        /// <summary>
        /// The distance the agent has traveled
        /// </summary>
        public float distanceTravelled = 0.0f;
        /// <summary>
        /// A boolean for if the agent has reached the goal.
        /// </summary>
        public Boolean reachedGoal = false;
        /// <summary>
        /// The agents current heuristic
        /// </summary>
        public Dictionary<int, decimal> hMap = new Dictionary<int, decimal>();


        public Dictionary<int, decimal> gCost = new Dictionary<int, decimal>();
        public SortedSet<Tile> openQueue = new SortedSet<Tile>();
        //public SimplePriorityQueue<Tile> closedQueue = new SimplePriorityQueue<Tile>();
        //public List<Tile> openList;
        public HashSet<Tile> closedList = new HashSet<Tile>();
        /// <summary>
        /// The agents ID
        /// </summary>
        public int id;
        /// <summary>
        /// boolean value for if the cutoff was reached.
        /// </summary>
        public bool reachedCutoff = false;
        /// <summary>
        /// name of agent
        /// </summary>
        public string name = "search agent";
        /// <summary>
        /// The agents utility functions.
        /// </summary>
        public Utilities utils = new Utilities();
        /// <summary>
        /// The agents most updated value (the most amount of learning any state has.)
        /// </summary>
        public float mostUpdatedValue;
        /// <summary>
        /// A list of states the agent has visited
        /// </summary>
        public List<int> statesVisited = new List<int>();
        /// <summary>
        /// The goal achievement time for the agent.
        /// </summary>
        public float goalAchivementTime = float.NaN;
        /// <summary>
        /// stopwatch for recording the agents time.
        /// </summary>
        public Stopwatch _totalTime = new Stopwatch();
        /// <summary>
        /// Thinking time
        /// </summary>
        private Stopwatch _thinkingTime = new Stopwatch();
        /// <summary>
        /// Time spent thinking and returning move
        /// </summary>
        public float thinkingTime = 0f;
        public List<float> _thikingTimes = new List<float>();
        /// <summary>
        ///  the type of search agent (i.e A*/uLRTA*)
        /// </summary>
        public string agentType;
        /// <summary>
        /// number of steps the agent took. 
        /// </summary>
        public int numberOfSteps = 0;
        public int numberOfStatesTouched = 0;
        public int numberOfStatesExpanded = 0;
        public int stepsTakenToReachGoal;
        public Tile startingLocation;
        public int collisions = 0;
        public int width;
        public int height;
        private string heuristicType;
        public AlgorithmParameters gene;
        public float firstMoveTime;
        public ReverseResumableAStar RRAStar;
        public Tile desiredNode = null; // for debugging;
        public Dictionary<Int32, Tile> pathToDesiredNode = new Dictionary<int, Tile>();
        /// <summary>
        /// Constructor for the search agent.
        /// </summary>
        /// <param name="id">id of the agent.</param>
        /// <param name="goal">goal of the agent.</param>
        /// <param name="start">starting location of the agent.</param>
        /// <param name="height"> height of map</param>
        /// <param name="width">width of map</param>
        /// <param name="gene">configuration to define the parameter</param>
        /// <param name="agentType">type of agent</param>
        public SearchAgent(int id, Tile goal, Tile start, int height, int width, string agentType, AlgorithmParameters gene)
        {

            this.gene = gene;
            _totalTime.Start();
            this.width = width;
            this.height = height;
            //h = new float[height, width];
            this.startingLocation = start;
            this.heuristicType = gene.heuristic;
            this.goal = goal;
            this.currentSpot = start;
            this.id = id;
            if (heuristicType == "perfect")
            {
                RRAStar = new ReverseResumableAStar(0, start, goal, height, width, "RRA", gene);
            }
            this.agentType = agentType;
            this.name = agentType;
            // sets reached goal true if agent is at the goal

            if (goal.col == start.col && start.row == goal.row)
            {
                this.reachedGoal = true;
            }
            if (hMap.ContainsKey(goal.id))
            {
                hMap.Add(goal.id, 0);
            }
            generateAllH(start);
        }
        private void generateAllH(Tile start)
        { 
            HashSet<Tile> exploredTiles = new HashSet<Tile>();
            HashSet<Tile> unexploredTiles = new HashSet<Tile>();

            unexploredTiles.Add(start);
            while(unexploredTiles.Count != 0)
            {
                Tile exploreTile = unexploredTiles.First();
                getHeuristic(exploreTile);
                unexploredTiles.Remove(exploreTile);
                exploredTiles.Add(exploreTile);
                foreach(Tile n in exploreTile.getNeighbours())
                {
                    if (!exploredTiles.Contains(n) && !unexploredTiles.Contains(n))
                    {
                        unexploredTiles.Add(n);
                    }
                }
            }
        }
        /// get the f cost for a tile
        /// </summary>
        /// <param name="l">tile for which the f cost is calculated</param>
        /// <returns>f cost of the tile.</returns>
        public decimal getFCost(Tile l)
        {
            return gCost[l.id] + getHeuristic(l);
        }
        public float timeLimit = float.MaxValue;
        public void setThinkingTimeLimit(float timeLimit)
        {
            this.timeLimit = timeLimit;
        }
       
        /// <summary>
        /// Returns the heuristic of the tile
        /// </summary>
        /// <param name="tile">tile in question</param>
        /// <returns>heuristic of the tile to the goal</returns>
        public decimal getHeuristic(Tile tile)
        {
          
            if (!hMap.ContainsKey(tile.id))
            {
                hMap.Add(tile.id, createHeuristic(tile));
            }
            return hMap[tile.id];
        }
        internal int reservedUntil = -1;
        internal Dictionary<int, Tile> nextMoves = new Dictionary<int, Tile>();
        internal List<Reservation> myReservations = new List<Reservation>();
        public virtual Dictionary<int, Tile> getMoves()
        {
            return new Dictionary<int, Tile>();
        }
        public virtual bool moveOffPath(int time)
        {
            List<Tile> possilbeMoves = this.currentSpot.getNonWallNeighbours(false)
                .Where(t => t.isAvailable()).ToList();
            Tile moveToTile;
            //if (possilbeMoves.Any(t => reservationTable.isAvailable(t, time + 1))) // check if there is a non-reserved spot
            //{
            //    moveToTile = possilbeMoves.First(t => reservationTable.isAvailable(t, time + 1));
            //}
            if (possilbeMoves.Count() > 0)
            { // move
                moveToTile = possilbeMoves.First(t => t.isAvailable());
            }
            else
            {
                return false;
            }
            crossEdge(moveToTile, utils.GetTravelCost(this.currentSpot, moveToTile));
            setNewGoal(this.goal);
            planMoveList(time,true);
            return true;
        }
        public void IncrementPaths()
        {
            foreach (KeyValuePair<int, Tile> pair in nextMoves)
            {
                pair.Value.IncrementPathCount();
            }
        }


        internal int numberOfReservations;
        internal void SetNumberOfReservations()
        {
            this.numberOfReservations = int.Parse(gene.parameters["numberOfReservations"]);
        }
      
        private decimal createHeuristic(Tile tile)
        {
            if (tile == goal)
            {
                return 0;
            }
            if (heuristicType == "perfect")
            {
                return RRAStar.AbstractDist(tile);
            }
            else
            {
                return utils.GetHeuristic(tile, goal, gene.heuristic);
            }
        }
        public void setNewH(Dictionary<int, decimal> h)
        {
            this.hMap = h;
        }
        public void setNewRRAStar(ReverseResumableAStar reverseResumableAStar)
        {
            this.RRAStar = reverseResumableAStar;
        }
        public decimal getLearningAmount(Tile t)
        {
            decimal val = getHeuristic(t) - createHeuristic(t);

            return val;
        }
        public void updateHValue(Tile t, decimal newH)
        {
            if (hMap.ContainsKey(t.id)) {
                hMap[t.id] = newH;
            } else
            {
                hMap.Add(t.id, newH);
            }
        }
        public virtual int getNumberOfStatesTouched()
        {
            if (heuristicType == "perfect")
            {
                //return numberOfStatesTouched;
                return numberOfStatesTouched + RRAStar.numberOfStatesTouched;
            } else
            {
                return numberOfStatesTouched;
            }
        }
        public virtual int getNumberOfStatesExpanded()
        {
            if (heuristicType == "perfect")
            {
                //return numberOfStatesExpanded;
                return numberOfStatesExpanded + RRAStar.numberOfStatesExpanded;
            }
            else
            {
                return numberOfStatesExpanded;
            }
        }

        public virtual void setNewGoal(Tile newGoal)
        {
            this.goal = newGoal;
            this.numberOfSteps = 0;
            //this.numberOfStatesExpanded = 0;
            //this.numberOfStatesTouched = 0;
            this.thinkingTime = 0;
        }
        public virtual void setCurrentSpot(Tile newSpot)
        {
            this.currentSpot = newSpot;
        }
        public bool atGoal()
        {
            return (this.goal.col == this.currentSpot.col && this.currentSpot.row == this.goal.row);

        }
        /// <summary>
        /// Return the next location that the agent wishes to move to.
        /// </summary>
        /// <returns>the next tile the agent wish to move to</returns>
        public virtual Tile getNextNode()
        {
            // should be overridden
            return this.currentSpot;
        }
        internal virtual void collision()
        {

        }
        
        private bool _firstMove = true;

        /// <summary>
        /// Return the next location that the agent wishes to move to.
        /// </summary>
        /// <returns>the next tile the agent wish to move to</returns>
        public void planMoveList(int time, bool forceReplan = false)
        {
            _thinkingTime.Restart();
            _thinkingTime.Start();
            createMoveList(time);
            _thinkingTime.Stop();
            thinkingTime += (float) _thinkingTime.Elapsed.TotalMilliseconds;
            _thikingTimes.Add((float)_thinkingTime.Elapsed.TotalMilliseconds);
            if (_firstMove)
            {
                firstMoveTime = (float)_thinkingTime.Elapsed.TotalMilliseconds;
            }
        }

        public virtual void createMoveList(int depth, bool forceReplan = false)
        {
            Console.WriteLine("should be overwritten");
        }
        /// <summary>
        /// how the heuristic update is calculated.
        /// </summary>
        /// <param name="row">row for the compare value</param>
        /// <param name="col">column for the compare value</param>
        /// <returns>ratio between 0 and 1 for the update value</returns>
        public float getHeuristicUpdateValueRatio(int row, int col)
        {
            if (this.mostUpdatedValue == 0) return 0.0f;
            return 0f;
            //return Math.Abs(createHeuristic(t- this.h[row, col]) / this.mostUpdateValue;
        }
        public decimal getH0(Tile tile)
        {
            return createHeuristic(tile);
        }
        
        public Boolean itReachGoal()
        {
            return this.reachedGoal;
        }
        /// <summary>
        /// Automatically moves the agent to the goal.
        /// </summary>
        public void moveToGoal()
        {
            this.currentSpot.removeAgent();
            this.currentSpot = this.goal;
            this.reachedGoal = true;
            this.currentSpot.addAgent(this, this.reachedGoal);
        }
      
        /// <summary>
        /// Has the agent move to the new tile and records the cost as distance traveled.
        /// </summary>
        /// <param name="newLocation">new location of the agent</param>
        /// <param name="cost">cost for the agent to move to the new tile.</param>
        public bool crossEdge(Tile newLocation, decimal cost)
        {
           
            numberOfSteps++;
            if (newLocation == currentSpot)
            {
                return true;
            }
            if (newLocation.isAvailable() == false)
            {
                collisions++;
                collision();
                return false;
            }
            if (!currentSpot.getNeighbours().Contains(newLocation))
            {
                Console.WriteLine("Tried to teleport, returned fail");
                return false;
            }
            statesVisited.Add(newLocation.id);
            this.distanceTravelled += (float)cost;
            this.currentSpot.removeAgent();
            this.currentSpot = newLocation;
            
            if (goal == currentSpot)
            {
                this.reachedGoal = true;
                stepsTakenToReachGoal = numberOfSteps;
                goalAchivementTime = (float)_totalTime.Elapsed.TotalMilliseconds;  
            }
            else
            {
                this.reachedGoal = false;
            }
            this.currentSpot.addAgent(this, this.reachedGoal);
            return true;
        }
    }
    /// <summary>
    /// Abstract factory that is overridden by concrete factories.
    /// </summary>
    abstract class SearchCreator
    {
        /// <summary>
        /// Abstract factory method that is overridden by concrete search algorithms
        /// </summary>
        /// <param name="id">id of the agent</param>
        /// <param name="goal">goal tile of the agent</param>
        /// <param name="start">start tile of the agent.</param>
        /// <param name="height">height of map</param>
        /// <param name="width">width of map</param>
        /// <param name="parameters">params for the agents</param>
        /// <returns>the Search agent created by the factory.</returns>
        /// 
        public abstract SearchAgent FactoryMethod(int id, Tile goal, Tile start, int height, int width,  AlgorithmParameters parameters);
    }
}

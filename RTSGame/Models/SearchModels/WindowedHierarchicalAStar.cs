﻿using RTSGame.Models.Configurations;
using RTSGame.Models.SearchControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Models.SearchModels
{
    struct whcaParameters
    {
        public int window { get; set; }
        public bool enhanced { get; set; }
    }
    class TimedTile
    {

        public Tile tile;
        public int time;
        public decimal g;
        public decimal h;
        public decimal f;
        public Int64 hash;
        public TimedTile(Tile tile, Int32 time, Int64 hash)
        {
            this.tile = tile;
            this.time = time;

            //hash = (((((prime1 + value1) * prime2) + value2) * prime3) + value3) * prime4;
            this.hash = hash;
        }

    }
    class TimedTileHolder
    {
        Utilities utils = new Utilities();

        Dictionary<Int64, TimedTile> holder = new Dictionary<long, TimedTile>();
        public TimedTileHolder()
        {

        }
        public TimedTile getTile(long id)
        {
            return holder[id];
        }
        public TimedTile createTile(Tile t, Int32 time)
        {
            Int16 value1 = t.col;
            Int16 value2 = t.row;
            Int32 value3 = time;
            TimedTile tmp = new TimedTile(t, time, utils.MakeLong(value3, utils.Make32Bit(value1, value2)));
            if (!holder.ContainsKey(tmp.hash)){
            
                holder.Add(tmp.hash, tmp);
            }
            return holder[tmp.hash];
        }
        public void Clear()
        {
            holder.Clear();
        }
    }
    //create comparer
    internal class TimedTileComparer : IComparer<TimedTile>
    {
        public bool firstMove = true;
        public ReverseResumableAStar reverseResumableA = null;
        public int Compare(TimedTile x, TimedTile y)
        {
            if (x.hash == y.hash)
            {
                return 0;
            }
            //first by f
            int result = x.f.CompareTo(y.f);
            if (reverseResumableA != null)
            {
                if (result == 0)
                {
                    bool xIsOnRRAPath = reverseResumableA.principalVariation.ContainsKey(x.tile.id);
                    bool yIsOnRRAPath = reverseResumableA.principalVariation.ContainsKey(y.tile.id);
                    result = xIsOnRRAPath.CompareTo(yIsOnRRAPath);
                }
            }
            
            //then g
            if (result == 0)
                result = -x.g.CompareTo(y.g);
            
     
            ////a third sort
            if (result == 0)
                result = -x.hash.CompareTo(y.hash);

            return result;
        }
    }

    class WindowedHierarchicalAStar : SearchAgent
    {
        private whcaParameters parseWHCAStarParameters(AlgorithmParameters config)
        {
            return new whcaParameters
            {
                window = int.Parse(config.parameters["windowSize"]),
                enhanced = bool.Parse(config.parameters["enhanced"])

            };
        }
 
        public SortedSet<TimedTile> openQueue;
        public HashSet<Int64> openHashSet = new HashSet<Int64>();
        //public SimplePriorityQueue<Tile> closedQueue = new SimplePriorityQueue<Tile>();
        //public List<Tile> openList;
        public HashSet<TimedTile> closedList = new HashSet<TimedTile>();
        public HashSet<Int64> closedHashList = new HashSet<Int64>();
        public Dictionary<Int64, TimedTile> parentTileMapping=new Dictionary<Int64, TimedTile>();
        public Dictionary<Int64, TimedTile> principalVariation = new Dictionary<Int64, TimedTile>();
        private Dictionary<Tile, decimal> gCosts = new Dictionary<Tile, decimal>();
        public whcaParameters whcaParameters;
        AlgorithmParameters config;
        TimedTileHolder holder = new TimedTileHolder();
        TimedTileComparer comparer = new TimedTileComparer();
        /// <summary>
        /// Initializes the A* algorithm.
        /// </summary>
        /// <param name="id">id of the agent</param>
        /// <param name="goal">goal of the agent.</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="height">height of map.</param>
        /// <param name="width">Width of map</param>
        /// <param name="name">name of the agent. </param>
        /// <param name="config">config</param>
        public WindowedHierarchicalAStar(int id, Tile goal, Tile start, int height, int width, string name, AlgorithmParameters config) 
            : base(id, goal, start, height, width, name, config)
        {
            this.config = config;
            openQueue = new SortedSet<TimedTile>(comparer);
            whcaParameters = parseWHCAStarParameters(config);            
            this.name = "WHCAStar_(" + whcaParameters.window.ToString() + ")";
            this.agentType = "WHCAStar_(" + whcaParameters.window.ToString() + ")";
            comparer.reverseResumableA = RRAStar;
            //RRAStar.ResumeRRAStar(currentSpot);
        }
        decimal getAbstractDist(Tile node)
        {
            return getHeuristic(node);
        }
        public TimedTile GetTimedTile(long id)
        {
            return holder.getTile(id);
        }
        private void printValues()
        {
            Console.WriteLine("Printing WHCA: ");
            foreach (TimedTile tt in openQueue)
            {
                Tile t = tt.tile;
                Console.WriteLine("F: " + tt.f + "     G: " + tt.g + "     Row: " + t.row + "    col: " + t.col);
            }
        }
        
        TimedTile cs;
        public bool concreteSearch(int currentTime)
        {
            cs.g = 0;
            decimal aDist2 = getAbstractDist(currentSpot);
            cs.h = aDist2;
            openQueue.Add(cs);
            gCosts.Add(cs.tile, 0);
            openHashSet.Add(cs.hash);
            Tile lastPickedTile = currentSpot;
            while(openQueue.Count > 0)
            {
                if (_totalTime.Elapsed.Milliseconds > timeLimit && timeLimit >0)
                {
                    return false;
                }
                //if (numberOfStatesExpanded <3)
                //{
                //    printValues();
                //}
                
                TimedTile P = openQueue.First();
                openQueue.Remove(P);
                openHashSet.Remove(P.hash);
                if (P.tile == goal && reservationTable.isAvailable(P.tile,P.time) &&
                     reservationTable.isAvailable(P.tile, P.time + 1) && P.time > currentTime)
                {
                    invertAStarChain(P);
                    return true;
                }
                closedList.Add(P);
                closedHashList.Add(P.hash);
                numberOfStatesExpanded++;
                List<Tile> spaceNeighbours = P.tile.getNonWallNeighbours();

                List<TimedTile> spaceTimeNeighbours = new List<TimedTile>();
                spaceNeighbours.Add(P.tile);
                foreach (Tile t in spaceNeighbours)
                {

                    if ((
                            reservationTable.isAvailable(t, P.time + 1) ||
                            reservations.Any(r => r.tile == t && r.time == P.time + 1)
                        ) &&
                        (
                            reservationTable.isAvailable(t, P.time + 2) ||
                            reservations.Any(r => r.tile == t && r.time == P.time + 2)
                        ))
                    {
                        if (P == cs)
                        {
                            if (t == P.tile || t.isAvailable()) {
                                if (P.time + 1 > depthLimit )
                                {
                                    TimedTile tt = holder.createTile(goal, P.time + 1);
                                    spaceTimeNeighbours.Add(tt);
                                }
                                else
                                {
                                    TimedTile tt = holder.createTile(t, P.time + 1);
                                    spaceTimeNeighbours.Add(tt);
                                }
                            }
                        } else
                        {
                            if (P.time + 1 > depthLimit )
                            {
                                TimedTile tt = holder.createTile(goal, P.time + 1);
                                spaceTimeNeighbours.Add(tt);
                            }
                            else
                            {
                                TimedTile tt = holder.createTile(t, P.time + 1);
                                spaceTimeNeighbours.Add(tt);
                            }
                        }

                    }
                }
                foreach (TimedTile Q in spaceTimeNeighbours)
                {
                    decimal hDist = getAbstractDist(Q.tile);
                    Q.h = hDist;
                }
                spaceTimeNeighbours.Sort((a, b) => a.h.CompareTo(  b.h));
                numberOfStatesTouched += spaceTimeNeighbours.Count;
                foreach(TimedTile Q in spaceTimeNeighbours)
                {
                    decimal tenativeG = P.g + Cost(P,Q);
                    if (closedHashList.Contains(Q.hash))
                    {
                        continue;
                    }
                    if (!openHashSet.Contains(Q.hash) && !closedHashList.Contains(Q.hash))
                    {
                        Q.g = tenativeG;
                        decimal hDist = getAbstractDist(Q.tile);
                        decimal f = tenativeG + hDist;
                        Q.f = f;
                        openHashSet.Add(Q.hash);
                        openQueue.Add(Q);
                        parentTileMapping.Add(Q.hash, P);
                    }
                    if (openHashSet.Contains(Q.hash) && tenativeG <= Q.g)
                    {
                        openQueue.Remove(Q);

                        Q.g = tenativeG;
                        decimal hDist = getAbstractDist(Q.tile);

                        decimal f = tenativeG + hDist;
                        Q.f = f;
                        openQueue.Add(Q);
                        parentTileMapping[Q.hash] =  P;
                    }
                }
            }
            return false;
        }
        private decimal Cost(TimedTile p, TimedTile q)
        {
            if (p.tile == q.tile && p.tile == goal)
            {
                return 0;
            }
            if (p.tile == q.tile)
            {
                return 0.0001M;
            } else if (p.tile.getNeighbours().Contains(q.tile))
            {
                return utils.GetTravelCost(p.tile, q.tile);
            } else if (q.tile == goal)
            {
                return getAbstractDist(p.tile);
            } else
            {
                return utils.GetManhattanDistance(p.tile.row, p.tile.col, q.tile.row, q.tile.col);
            }
        }
        public List<Reservation> reservations = new List<Reservation>();
        public void addReservation(Reservation reservation)
        {
            reservations.Add(reservation);
        }
        public Dictionary<Int64, TimedTile> invertAStarChain(TimedTile tile)
        {
            principalVariation.Clear();
            while (true)
            {
                if (!parentTileMapping.ContainsKey(tile.hash))
                {
                    break;
                }

                TimedTile parentTile = parentTileMapping[tile.hash];
                this.principalVariation.Add(parentTile.hash, tile);
                if (parentTile == cs)
                {
                    break;
                }
                tile = parentTile;
            }

            //principalVariation.Add(tile, tile);
            return principalVariation;
        }
        private List<Tile> createListFromPrincipleVariation()
        {
            TimedTile nextT = cs;
            TimedTile currentT = cs;
            List<Tile> moves = new List<Tile>();
            int i = curStep;
            while (i < depthLimit)
            {
                currentT = nextT;
                if (!principalVariation.ContainsKey(nextT.hash))
                {
                    break;
                }
                nextT = principalVariation[currentT.hash];
                if (currentT.tile.getNeighbours().Contains(nextT.tile) ||
                    currentT.tile == nextT.tile)
                {
                    moves.Add(nextT.tile);
                    if (nextT.tile == goal)
                    {
                        break;
                    }
                    i++;
                } else
                {
                    break;
                }
            }
            return moves;
        }
        //public override int getNumberOfStatesExpanded()
        //{
        //    return base.getNumberOfStatesExpanded();
        //}
        //public override int getNumberOfStatesTouched()
        //{
        //    return base.getNumberOfStatesTouched();
        //}
        private List<Tile> moves;
        int depthLimit;
        int curStep;
        public override void createMoveList(int currentStep, bool forceReplan = false)
        {
            this.curStep = currentStep;
            // 1. search up to w tiles in the real domain
            // 2. finish search in the abstract distance
            this.depthLimit = currentStep + whcaParameters.window;
            principalVariation.Clear();
            openQueue.Clear();
            openHashSet.Clear();
            closedList.Clear();
            holder.Clear();
            closedHashList.Clear();
            parentTileMapping.Clear();
            gCosts.Clear();
            cs = holder.createTile(currentSpot, currentStep);
            cs.g = 0;
            if (collisions > 0 || !whcaParameters.enhanced)
            {
                bool finishedSearch = concreteSearch(currentStep);
                if (finishedSearch)
                {
                    this.moves = createListFromPrincipleVariation();
                    moveIndex = 0;
                    markReservationTable(currentStep);
                }
                else
                {
                    this.moves = new List<Tile>() { this.currentSpot };
                    markReservationTable(currentStep);
                }
            } else
            {
                this.moves = RRAStar.moves;
                markReservationTable(currentStep);
            }
            //return invertAStarChain(goal);
        }
        int moveIndex = 0;
        private void markReservationTable(int timeStep)
        {
            WindowedHierarchicalAStar whaAgent = this;
            Tile curTile = whaAgent.currentSpot;
            Tile nextTile = whaAgent.getMove(moveIndex);
            for (int i = 0; i < whcaParameters.window; i++)
            {
                if (reservationTable.isAvailable(nextTile, timeStep + i + 1) ||
                    whaAgent.hasReservation(nextTile, timeStep + i + 1))
                {
                    //Console.WriteLine("Agent {0} moves (row,col, t): {1}, {2}, {3}", whaAgent.id, nextTile.row, nextTile.col, timeStep + i + 1);
                    //reservationTable.reserveSpot(curTile, timeStep + i + 1);
                    reservationTable.reserveSpot(curTile, timeStep + i);
                    reservationTable.reserveSpot(nextTile, timeStep + i + 1);
                    Reservation cur = new Reservation(curTile, timeStep + 1);
                    //Reservation curF = new Reservation(curTile, timeStep + 1);
                    Reservation nex = new Reservation(nextTile, timeStep + i + 1);
                    whaAgent.addReservation(cur);
                    whaAgent.addReservation(nex);
                    //nextTile = principalVariation[nextTile];
                }
                else
                {
                    //reservationTable.isAvailable(nextTile, timeStep + i + 1);
                    //Console.WriteLine("Agent {0} Invalid reservation (row,col, t): {1}, {2}, {3}", whaAgent.id, nextTile.row, nextTile.col, timeStep + i + 1);
                }
                curTile = whaAgent.getMove(i);
                nextTile = whaAgent.getMove(i + 1);
            }
        }
    
        public Tile getMove(int i)
        {
            if (moves.Count ==0)
            {
                return cs.tile;
            }
            if (i < moves.Count)
            {
                return moves[i];
            } else
            {
                return moves[moves.Count - 1];
            }
        }
        public override Tile getNextNode()
        {
            Tile nextTile;
            if (moves.Count == 0)
            {
                nextTile= cs.tile;
            } else if (moveIndex < moves.Count)
            {

                nextTile= moves[moveIndex];
                moveIndex++;
            }
            else
            {
                nextTile= moves[moves.Count - 1];
            }
            return nextTile;

        }
        private ReservationTable reservationTable;
        public void setReservationTable(ReservationTable reservationTable)
        {
            this.reservationTable = reservationTable;
        }
        public void clearReservations()
        {
            reservations.Clear();
        }
        public bool hasReservation(Tile t, int time)
        {
            return reservations.Any(r => r.tile == t && r.time == time);
        }
    }
    /// <summary>
    /// A star factory creator.
    /// </summary>
    class windowedHierarchicalAStarCreator : SearchCreator
    {
        /// <summary>
        /// Concrete a star factory. A star agents solve their problem before returning the agent.
        /// </summary>
        /// <param name="id">id of the agent.</param>
        /// <param name="goal">goal of the agent</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="height">height of map.</param>
        /// <param name="width">width of map.</param>
        /// <param name="parameters">params of algorithm</param>
        /// <returns></returns>
        public override SearchAgent FactoryMethod(int id, Tile goal, Tile start, int height, int width, AlgorithmParameters parameters)
        {
            WindowedHierarchicalAStar agent = new WindowedHierarchicalAStar(id, goal, start, height, width, "AStar", parameters);
            return agent;
        }
    }
}

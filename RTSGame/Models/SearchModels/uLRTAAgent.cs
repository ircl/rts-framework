﻿
using RTSGame.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using static RTSGame.MainController;
using static RTSGame.ParameterConfiguration;

namespace RTSGame
{
   
    /// <summary>
    /// uLRTA style search agent with genetic properties.
    /// </summary>
    class ULRTAAgent : SearchAgent
    {
        /// <summary>
        /// Defines the gene for the uLRTA agent (see per-Map algorithm selection (Bulitko, SOCS 2016) for details.
        /// </summary>
        struct ULRTAParameters
        {
            /// <summary>
            /// Weight of the heuristic update
            /// </summary>
            public decimal weight { get; set; }
            /// <summary>
            /// Weight of the lateral learning update.
            /// </summary>
            public decimal wc { get; set; }
            /// <summary>
            /// Whether or not depression avoidance is active.
            /// </summary>
            public Boolean depressionAvoidance { get; set; }
            /// <summary>
            /// Whether or not depression avoidance is active.
            /// </summary>
            public Boolean markAndAviod { get; set; }
            /// <summary>
            /// Whether states should be marked as expendable and pruned
            /// </summary>
            public Boolean markExpendable { get; set; }
            /// <summary>
            /// Learning operation function
            /// </summary>
            public learningOperator learningOperator { get; set; }
            /// <summary>
            /// Beam width of the lateral learning
            /// </summary>
            public float beamWidth { get; set; }
            /// <summary>
            /// How many steps a head the agent considers when making its move. Currently implements a IDA* style lookahead.
            /// </summary>
            public int lookahead { get; set; }
            /// <summary>
            /// Filter agents from planning?
            /// </summary>
            public decimal visibleAgentRange { get; set; }
            /// <summary>
            /// Type of lookahead by agent
            /// </summary>
            public string lookaheadType { get; set; }
            /// <summary>
            /// Number of steps before replanning
            /// </summary>
            public int stepsBeforeReplan { get; set; }
            public bool useFlowRestricted { get; set; }
            public bool requestOffGoal { get; set; }
        }
        enum learningOperator 
        {
            minimum,
            median,
            mean,
            maximum
        }
        /// <summary>
        /// whether to permanently remove the node or to simply add a large sum to the heuristic.
        /// </summary>
        Boolean hardExpendable = true;
        /// <summary>
        /// Amount to increase the states heuristic by if using a soft expendable (i.e where map is dynamic and a state may no longer be expendable
        /// </summary>
        decimal expendableAddition = 1000;
        /// <summary>
        /// The agents gene.
        /// </summary>
        ULRTAParameters uLRTAGene;
        /// <summary>
        /// The states that the agent has expended.
        /// </summary>
        public HashSet<int> expendedTiles = new HashSet<int>();
        /// <summary>
        /// Contains a list of mark and avoid tiles
        /// </summary>
        HashSet<int> markAndAvoidTiles = new HashSet<int>();
        /// <summary>
        /// Parameters for the a star agent
        /// </summary>
        AlgorithmParameters aStarParameters;
        private int replanAt = 0;
        /// <summary>
        /// Raw parameters passed in
        /// </summary>
        AlgorithmParameters parameters;
        /// <summary>
        /// A star agent used for frontier
        /// </summary>
        AStarAgent aStarAgent;
        /// <summary>
        /// Frontier tile the agent is trying to move to
        /// </summary>
        Tile lastFrontierTile = null;
        /// <summary>
        /// The last trajectory the agent has found.
        /// </summary>
        Dictionary<Tile, Tile> lastTrajectory = new Dictionary<Tile, Tile>();
        const int doulbePrecission = 5;
        const decimal precissionThreshold = 0.0005M;
        /// <param name="id">id of the agent</param>
        /// <param name="goal">goal for the agent.</param>
        /// <param name="start">starting location for the agent.</param>
        /// <param name="height">height.</param>
        /// <param name="width">width.</param>
        /// <param name="type">type of agent.</param>
        /// <param name="parameters">gene params</param>
        public ULRTAAgent(int id, Tile goal, Tile start, int height, int width,  string type, AlgorithmParameters parameters) 
            :base(id, goal, start, height, width, type, parameters)
        {
            this.parameters = parameters;
            uLRTAGene = parseParameters(parameters);
            aStarParameters = new AlgorithmParameters
            {
                name = "astar",
                heuristic = parameters.heuristic,
                singleExpansionPerUpdate = parameters.singleExpansionPerUpdate,
                parameters = new Dictionary<string, string>()
                {
                    {"visible agent distance", parameters.parameters["visible agent distance"] },
                    {"solvePathCompletely","false" },
                    {"useFlowRestricted", parameters.parameters["useFlowRestricted"]}, 
                    {"lookaheads",parameters.parameters["lookahead"]}
                }
            };
            lastFrontierTile = start;
            geneToString(uLRTAGene);
            initializeValues();
            aStarAgent = new AStarAgent(0, goal, this.currentSpot, height, width, "AStar", aStarParameters);

            aStarAgent.setNewH(hMap);
            aStarAgent.setNewRRAStar(this.RRAStar);
        }
        /// <summary>
        /// Override the base setNewGoal.
        /// </summary>
        /// <param name="newGoal">New goal for the agent</param>
        public override void setNewGoal(Tile newGoal)
        {
            base.setNewGoal(newGoal);
            initializeValues();
            //if (myReservations.Count != 0)
            //{
            //    reservationTable.freeSpots(myReservations);
            //}
        }
        internal override void collision()
        {
            base.collision();
            nextMoves.Clear();
            replanAt = 0;
        }
        private void printMoveList()
        {
            Tile curTile = this.currentSpot;
            int i = 0;
            while (i < nextMoves.Count) {
                Tile nextTile = nextMoves[curTile.id];
                i++;
                Console.WriteLine("Agent {0}: [{1},{2}] -> [{3},{4}]", this.id, curTile.row, curTile.col,
                    nextTile.row, nextTile.col);
                curTile = nextTile;
                if (nextMoves.ContainsKey(curTile.id) == false)
                {
                    break;
                }
            }
            Console.WriteLine("____________\n\n\n");
        }
        /// <summary>
        /// Interweaves the planning and execution of the agent. 
        /// </summary>
        /// <returns>the tile the agent will try to move to.</returns>
        public override Tile getNextNode()
        {
            if (nextMoves.ContainsKey(currentSpot.id))
            {
                return nextMoves[currentSpot.id];
            }
            else
            {
                return currentSpot;
            }
        }
        /// <summary>
        /// Return the agents moves
        /// </summary>
        /// <returns></returns>
        public override Dictionary<int, Tile> getMoves()
        {
            return aStarAgent.principalVariation;
        }
        /// <summary>
        /// Generates the agents next moves
        /// </summary>
        /// <param name="timeStep">current time</param>
        /// <param name="forceReplan">force replan</param>
        /// <returns></returns>
        public override void createMoveList(int timeStep, bool forceReplan = false)
        {
            if (numberOfSteps > replanAt || !nextMoves.ContainsKey(currentSpot.id) || forceReplan)
            {
                preformLookahead();
                replanAt = this.numberOfSteps + Math.Min(uLRTAGene.stepsBeforeReplan,nextMoves.Count());
            }
        }
        //////////////////////////////
        /// PRIVATE FUNCTIONS      ///
        //////////////////////////////
        private void initializeValues()
        {
            mostUpdatedValue = 0.0f;
            expendedTiles.Clear();
            markAndAvoidTiles.Clear();
            getHeuristic(this.currentSpot);

        }
        /// <summary>
        /// Expends states in either a hard manner where they cant be revisited or soft where with enough learning they can be.
        /// </summary>
        /// <param name="updateTile">tile to be updated</param>
        /// <param name="newH"> the new h value for the update cell.</param>
        /// <returns>how much to increment the new h value by</returns>
        private void expendState(Tile updateTile)
        {
            if (updateTile == goal)
            {
                return;
            }
            List<Tile> neighbours = updateTile.getNonWallNeighbours().Where(t => !expendedTiles.Contains(t.id)).ToList();
            // TODO: Vadim had used the hUpdate as a check in the matlab code?
            if (utils.Expenable(neighbours, updateTile, expendedTiles))
            {
                if (hardExpendable)
                {
                    expendedTiles.Add(updateTile.id);
                } else
                {
                    hMap[updateTile.id] += expendableAddition; // had heuristic to the tile
                }
            }
        }
        /// <summary>
        /// Filters a list of tiles based on the agents gene. For example mark and avoid, 
        /// </summary>
        /// <param name="queue"></param>
        /// <param name="gValues"></param>
        /// <param name="hValues">mapping of </param>
        /// <param name="maxLearningLimit"> amount of learning limit</param>
        /// <param name="filterMarked">should filter mark and avoid tiles?</param>
        /// <returns></returns>
        private SortedSet<Tile> filterQueueForValueAndMarkedTiles(SortedSet<Tile> queue, Dictionary<int, decimal> gValues, Dictionary<int, decimal> hValues, decimal maxLearningLimit = decimal.MaxValue,  bool filterMarked = false)
        {
            if (uLRTAGene.markExpendable && hardExpendable)
            {
                queue.RemoveWhere(t => expendedTiles.Contains(t.id));
            }
            queue.RemoveWhere(t => getLearningAmount(t) > maxLearningLimit);
            return queue;
        }
        /// <summary>
        /// Filters out non-applicable nodes and return the set of best nodes
        /// </summary>
        /// <param name="possibleNodes">potential nodes</param>
        /// <param name="gValues">gValues of the possible nodes</param>
        /// <param name="hValues">hValues of the possible nodes</param>
        /// <returns>Set of best nodes</returns>
        private SortedSet<Tile> filterDepressionsAndGetBestNode(SortedSet<Tile> possibleNodes, Dictionary<int, decimal> gValues, Dictionary<int, decimal> hValues)
        {
            SortedSet<Tile> bestTiles = new SortedSet<Tile>();
            if (possibleNodes.Count == 0)
            {
                return possibleNodes;
            }
            
            if (uLRTAGene.markAndAviod && !uLRTAGene.depressionAvoidance)
            {
                bool hasUnmarkedTiles = possibleNodes.Any(t=>!markAndAvoidTiles.Contains(t.id));
                if (hasUnmarkedTiles)
                {

                    return filterQueueForValueAndMarkedTiles(queue: possibleNodes, gValues: gValues, hValues: hValues, filterMarked: true);
                } else
                {
                    return possibleNodes;
                }
            } else if (uLRTAGene.depressionAvoidance && !uLRTAGene.markAndAviod) {

                decimal minLearning = possibleNodes.Min(n => getLearningAmount(n));
                return filterQueueForValueAndMarkedTiles(possibleNodes, gValues, hValues, minLearning, false);               
            } else if (uLRTAGene.depressionAvoidance && uLRTAGene.markAndAviod)
            {
                bool anyUnmarkedTiles = possibleNodes.Any(d => !markAndAvoidTiles.Contains(d.id));
                if (anyUnmarkedTiles)
                {
                    decimal minLearning = possibleNodes.Min(n =>  markAndAvoidTiles.Contains(n.id) ? decimal.MaxValue : getLearningAmount(n));
                    return filterQueueForValueAndMarkedTiles(possibleNodes, gValues, hValues, minLearning, true);
                }
                else
                {
                    decimal minLearning = possibleNodes.Min(n =>getLearningAmount(n));
                    return filterQueueForValueAndMarkedTiles(possibleNodes,  gValues, hValues, minLearning, false);
                }
            } else
            {
                return possibleNodes;
            }
        }
        /// <summary>
        /// Gets the F value that will be used to update the agent's heuristic
        /// </summary>
        /// <param name="bestTiles">set of best tiles for updating from</param>
        /// <param name="gValues">gValues of the tiles</param>
        /// <param name="hValues">hValues of the tiles</param>
        /// <returns>The F Value of the tile</returns>
        private decimal getUpdateFValue(SortedSet<Tile> bestTiles, Dictionary<int, decimal> gValues)
        {
            int numberOfNeighboursForUpdate = Math.Max(1, (int)Math.Ceiling(uLRTAGene.beamWidth * bestTiles.Count));
            decimal[] fVals = new decimal[numberOfNeighboursForUpdate];
            for(int i = 0; i < numberOfNeighboursForUpdate; i++){
                Tile n = bestTiles.First();
                bestTiles.Remove(n);
                fVals[i] = gValues[n.id] + getHeuristic(n);
            }
            decimal updateFVal;
            switch (uLRTAGene.learningOperator)
            {
                default:
                case learningOperator.minimum:
                    updateFVal = uLRTAGene.weight * fVals.Min();
                    break;
                case learningOperator.mean:
                    updateFVal = uLRTAGene.weight * fVals.Average();
                    break;
                case learningOperator.median:
                    updateFVal = uLRTAGene.weight * utils.GetMedian(fVals);
                    break;
                case learningOperator.maximum:
                    updateFVal = uLRTAGene.weight * fVals.Max();
                    break;
            }
            return updateFVal;
        }
        /// <summary>
        /// Update heuristic of the tiles of the agents.
        /// </summary>
        /// <param name="nodesToUpdate">set of tile to updates</param>
        /// <param name="bestFValue">F value used to update from</param>
        /// <param name="gValues">set of g values.</param>
        private void updateHeuristics(HashSet<Tile> nodesToUpdate, decimal bestFValue, Dictionary<int, decimal> gValues)
        {
            // RTAA* update
            foreach (Tile updateTile in nodesToUpdate)
            {
                decimal updateG = gValues[updateTile.id];
                decimal oldH = getHeuristic(updateTile);
                decimal newH = bestFValue - updateG;
                decimal learning = newH - oldH;
                if (learning > precissionThreshold)
                {
                   updateHValue(updateTile, newH);
                }
                if (uLRTAGene.markAndAviod)
                {
                    if (getLearningAmount(updateTile) > precissionThreshold)
                    {
                        markAndAvoidTiles.Add(updateTile.id);
                    }
                }
            }
        }

        private Dictionary<Int32,Tile> getPathToBestNode(Tile lookAheadFromTile, Tile bestNode)
        {
            Dictionary<Int32, Tile> pathToBestNode;
            if (uLRTAGene.lookahead == 1)
            {
                pathToBestNode = new Dictionary<Int32, Tile>()
                {
                    { lookAheadFromTile.id , bestNode}
                };
            }
            else
            {
                pathToBestNode = aStarAgent.InvertAStarChain(bestNode); // CREATE PATH TO BEST TILE
            }
            return pathToBestNode;
        }
        /// <summary>
        /// Performs the RTAA* style update procedure, number of lookaheads defined in aStarParameters.
        /// </summary>
        /// <param name="lookAheadFromTile">Tile to search from.</param>
        /// <returns>The path mapping from the lookahead tile to the best selected tile</returns>
        private Dictionary<Int32,Tile> rTAALookahead(Tile lookAheadFromTile)
        {
            ////////
            // A* LOOKAHEAD
            ///////
            Dictionary<int, decimal> aStarGValues;
            if (gene.singleExpansionPerUpdate)
            {
                if (aStarAgent.lookAheadsPerformed == 0 || aStarAgent.lookAheadsPerformed == uLRTAGene.lookahead)
                {
                    aStarAgent.lookAheadsPerformed = 0;
                    aStarAgent.SetPrunedNodes(expendedTiles);
                    aStarAgent.setCurrentSpot(this.currentSpot);
                    aStarAgent.setNewGoal(goal);
                }
                aStarAgent.getNextNode();
                closedList = aStarAgent.closedList;
                openQueue = aStarAgent.openQueue;
                aStarGValues = aStarAgent.gCost;

                if (openQueue.Count == 0 || lookAheadFromTile == goal || (aStarAgent.lookAheadsPerformed < uLRTAGene.lookahead && openQueue.First() != goal))
                {
                    return new Dictionary<Int32, Tile>()
                {
                    { lookAheadFromTile.id, lookAheadFromTile }
                };
                }
                if (openQueue.First() == goal)
                {
                    desiredNode = openQueue.First();
                    pathToDesiredNode = getPathToBestNode(lookAheadFromTile, openQueue.First()); // RETURN BEST TILE
                    return pathToDesiredNode;
                }
                ////////
                // LOOKAHEAD
                ///////
                //numberOfStatesTouched += openQueue.Count;
                //numberOfStatesExpanded += openQueue.Count;
                SortedSet<Tile> bestNodes = filterDepressionsAndGetBestNode(openQueue, aStarGValues, hMap);
                if (uLRTAGene.markExpendable)
                {
                    expendState(currentSpot);
                }
                Tile bestNode = bestNodes.First();
                decimal bestFValue = getUpdateFValue(openQueue, aStarGValues); // SELECT THE BEST TILE
                updateHeuristics(closedList, bestFValue, aStarGValues); // UPDATE CLOSED LIST BASED ON BEST TILE
                desiredNode = bestNode;
                pathToDesiredNode = getPathToBestNode(lookAheadFromTile, bestNode); // RETURN BEST TILE
                return pathToDesiredNode;
            }
            else
            {

                aStarAgent.SetPrunedNodes(expendedTiles);
                aStarAgent.setCurrentSpot(this.currentSpot);
                aStarAgent.setNewGoal(goal);
                aStarAgent.getNextNode();
                closedList = aStarAgent.closedList;
                openQueue = aStarAgent.openQueue;
                aStarGValues = aStarAgent.gCost;

                if (openQueue.Count == 0 || lookAheadFromTile == goal)
                {
                    return new Dictionary<Int32, Tile>()
                {
                    { lookAheadFromTile.id, lookAheadFromTile }
                };
                }
                if (openQueue.First() == goal)
                {

                    return getPathToBestNode(lookAheadFromTile, goal);
                }
                ////////
                // LOOKAHEAD
                ///////
                //numberOfStatesTouched += openQueue.Count;
                //numberOfStatesExpanded += openQueue.Count;
                SortedSet<Tile> bestNodes = filterDepressionsAndGetBestNode(openQueue, aStarGValues, hMap);
                Tile bestNode = bestNodes.First();
                if (uLRTAGene.markExpendable)
                {
                    expendState(currentSpot);
                }
                decimal bestFValue = getUpdateFValue(openQueue, aStarGValues); // SELECT THE BEST TILE
                updateHeuristics(closedList, bestFValue, aStarGValues); // UPDATE CLOSED LIST BASED ON BEST TILE
                return getPathToBestNode(lookAheadFromTile, bestNode); // RETURN BEST TILE
            }
        }

       
        /// <summary>
        /// Selects the type of lookahead to preform
        /// Current only uses rTAA lookahead
        /// </summary>
        private void preformLookahead()
        {
            nextMoves = rTAALookahead(currentSpot);
        }
        public override int getNumberOfStatesExpanded()
        {
            return aStarAgent.getNumberOfStatesExpanded();
        }
        public override int getNumberOfStatesTouched()
        {
            return aStarAgent.getNumberOfStatesTouched();
        }
        /// <summary>
        /// Convert the gene to human readable string
        /// </summary>
        /// <param name="gene">gene</param>
        private void geneToString(ULRTAParameters gene)
        {
            String[] lop = { "min", "avg", "median", "max" };
            String name = string.Format("({0:0.00} * {1}_{{{2:0.00}}}({3:0.00} * c + h){4}{5})({6}[{7}]){8}{9}{10}_{11}",
                gene.weight,//0
                gene.learningOperator.ToString(),
                gene.beamWidth, //2
                gene.wc,
                gene.depressionAvoidance ? "+da" : "",//4
                gene.markAndAviod ? "+a" : "",
                gene.markExpendable ? "+E" : "",//6
                gene.lookahead,
                gene.lookaheadType,//8
                gene.useFlowRestricted ? "_Flow":"",
                gene.requestOffGoal ? "_push":"", // 10
                gene.visibleAgentRange); //11
            this.name = name;
        }
        /// <summary>
        /// Parse the stringified data
        /// </summary>
        /// <param name="parameters">Parameters of the agent</param>
        /// <returns>converted and typed gene</returns>
        private ULRTAParameters parseParameters(AlgorithmParameters parameters)
        {

            return new ULRTAParameters
            {
                beamWidth = float.Parse(parameters.parameters["beam width"]),
                wc = decimal.Parse(parameters.parameters["wc"]),
                weight = decimal.Parse(parameters.parameters["weight"]),
                lookahead = Math.Max(1, Int32.Parse(parameters.parameters["lookahead"])),
                learningOperator = (learningOperator)Int16.Parse(parameters.parameters["learning operator"]),
                depressionAvoidance = bool.Parse(parameters.parameters["depression avoidance"]),
                markExpendable = bool.Parse(parameters.parameters["mark expendable"]),
                visibleAgentRange = decimal.Parse(parameters.parameters["visible agent distance"]),
                lookaheadType = parameters.parameters["lookahead type"],
                markAndAviod = bool.Parse(parameters.parameters["mark and avoid"]),
                useFlowRestricted = bool.Parse(parameters.parameters["useFlowRestricted"]),
                 requestOffGoal = bool.Parse(parameters.parameters["forceOffGoal"]),
                stepsBeforeReplan = Math.Max(
                    1,
                    Math.Min(
                        int.Parse(parameters.parameters["steps before replanning"]),
                        int.Parse(parameters.parameters["lookahead"])
                    )
                )
            };
        }
    }
    /// <summary>
    /// uLRTA Factory
    /// </summary>
    class uLRTACreator : SearchCreator
    {
        /// <summary>
        /// Concrete uLRTAa star factory. 
        /// </summary>
        /// <param name="id">id of the agent.</param>
        /// <param name="goal">goal of the agent</param>
        /// <param name="start">start location of the agent.</param>
        /// <param name="height">height of map.</param>
        /// <param name="width">width of map.</param>
        /// <param name="parameters">params</param>
        /// <returns>Returns a new uLRTA agent</returns>
        public override SearchAgent FactoryMethod(int id, Tile goal, Tile start, int height, int width, AlgorithmParameters parameters)
        {
            return new ULRTAAgent(id, goal, start, height, width, "uLRTA",  parameters);
        }
    }
}

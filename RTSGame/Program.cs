﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Net.Mail;

namespace RTSGame
{

    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
#if DEBUG && !LINUX
           
           using (var game = new MainController())
                game.Run(); 
#else
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            Trace.AutoFlush = true;
            Trace.Indent();
            Trace.WriteLine("Starting experiment");
            var fileName = "experiment.json";
            //args = new string[]
            //{
            //    "experiment.json",
            //    "1"

            //};
            ExperimentRunner experimentRunner;

            if (args.Length > 0)
            {
                fileName = args[0];
                if (args.Length > 2)
                {

                    int i = int.Parse(args[1]);
                    int multiplier = int.Parse(args[2]);
                    int startI = i * multiplier;
                    int endI = i * multiplier + multiplier;
                    Console.WriteLine("multi: " + multiplier.ToString());
                    Console.WriteLine("startI: " + (startI.ToString()));
                    Console.WriteLine("endI: " + (endI.ToString()));
                    experimentRunner = new ExperimentRunner(fileName, startI, endI);
                }
                else if (args.Length > 1)
                {
                    int index = int.Parse(args[1]);
                    experimentRunner = new ExperimentRunner(fileName, index);
                } else
                {
                    experimentRunner = new ExperimentRunner(fileName);
                }
            } else
            {
                experimentRunner = new ExperimentRunner(fileName);
            }
            bool succeded;
            try
            {
                if(args.Length > 2)
                {
                    int i = int.Parse(args[1]);
                    int multiplier = int.Parse(args[2]);
                    int startI = i * multiplier;
                    int endI = i * multiplier + multiplier;
                    succeded = experimentRunner.RunExperiments(startI,endI);
                }
                else if (args.Length > 1)
                {
                    int index = int.Parse(args[1]);
                    succeded = experimentRunner.RunExperiments(index);
                }
                else
                {
                    succeded = experimentRunner.RunExperiments();
                }
            }
            catch
            {
                succeded = false;
            }
           
            Trace.WriteLine("Experiment passed: " + succeded.ToString());

            //if (experimentRunner.emails.Count > 0)
            //{
            //    MailMessage mail = new MailMessage();
            //    foreach(string email in experimentRunner.emails)
            //    {
            //        mail.To.Add(new MailAddress(email));
            //    }
            //    mail.From = new MailAddress("dbsigurd@ualberta.ca");

            //    SmtpClient client = new SmtpClient();
            //    client.Port = 25;
            //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //    client.UseDefaultCredentials = false;
            //    client.Host = "smtp.gmail.com";
            //    mail.Subject = "this is a test email.";
            //    mail.Body = "this is my test email body" + succeded.ToString();
            //    client.Send(mail);
            //}

#endif


        }
    }
}

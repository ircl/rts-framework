﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTSGame
{
    /// <summary>
    /// Utility functions for the program to be used.
    /// The functions should be functional, meaning no changes to the parameters passed in.
    /// </summary>
    public class Utilities
    {
        private decimal multiple = 1;
        private decimal _cardinalCost = 1;
        //private float _diagonalCost = (float)Math.Sqrt(2);
        private decimal _diagonalCost = 1.4M;
        /// <summary>
        /// Returns the flat Linear index as of a 2d matrix starting at 0
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="rowLength"></param>
        /// <returns></returns>
        public int GetLinearIndexFromRowCol(int row, int col, int rowLength)
        {
            return rowLength * (row) +  col;
        }
        public int GetTileId(Int16 row, Int16 col) {
            return Make32Bit(row, col);
        }
        public Int64 MakeLong(Int32 left, Int32 right)
        {
            //implicit conversion of left to a long
            Int64 res = left;

            //shift the bits creating an empty space on the right
            // ex: 0x0000CFFF becomes 0xCFFF0000
            res = (res << 32);

            //combine the bits on the right with the previous value
            // ex: 0xCFFF0000 | 0x0000ABCD becomes 0xCFFFABCD
            res = res | (Int64)(uint)right; //uint first to prevent loss of signed bit

            //return the combined result
            return res;
        }
        public int GetLeft32Bit(Int32 whole)
        {
            int tempLong = ((whole >> 16) << 16); //shift it right then left 32 bits, which zeroes the lower half of the long
            int yourInt = (int)(whole - tempLong);
            return yourInt;
        }
        public int GetRight32Bit(Int32 whole)
        {

            return (whole >> 16);
        }
        public Int32 Make32Bit(Int16 left, Int16 right)
        {
            //implicit conversion of left to a long
            Int32 res = left;

            //shift the bits creating an empty space on the right
            // ex: 0x0000CFFF becomes 0xCFFF0000
            res = (res << 16);
            Int16 tmp = right;
            Int32 t = res | right;
            //combine the bits on the right with the previous value
            // ex: 0xCFFF0000 | 0x0000ABCD becomes 0xCFFFABCD
            res = res | (Int32)(UInt16)right; //uint first to prevent loss of signed bit

            //return the combined result
            return res;
        }
        /// <summary>
        /// Ranks a 8 connected tile in comparison against the provided center.
        /// </summary>
        /// <param name="centerX">X position of the center</param>
        /// <param name="centerY">Y position of the center</param>
        /// <param name="compareX">X position of the compare</param>
        /// <param name="compareY">Y position of the compare</param>
        /// <returns>The position ranked clockwise from top left to left middle</returns>
        public int RankClockwiseTopMiddle(int centerX,int  centerY, int compareX,int compareY)
        {
            if (compareX < centerX && compareY < centerY) // top left
            {
                return 1;
            }
            else if (compareX == centerX && compareY < centerY) // above
            {
                return 2;

            }
            else if (compareX > centerX && compareY < centerY) // top right
            {
                return 3;
            }
            
            else if (compareX > centerX && compareY == centerY) // right mid
            {
                return 4;
            }
            else if (compareX > centerX && compareY > centerY) // bottom  right
            {
                return 5;
            }
            else if (compareX == centerX && compareY > centerY) // bottom 
            {
                return 6;
            }
            else if (compareX < centerX && compareY > centerY) // bottom  left
            {
                return 7;
            }
            else if (compareX < centerX && compareY == centerY) // left mid
            {
                return 8;
            } else
            {
                return 9;
            }
        }
        /// <summary>
        /// Determines if a tile is expendable without preserving the shortest path. Based on the pruning expendable states paper. NOTE: This function may be moved in to a Search agent utility function soon.
        /// </summary>
        /// <param name="neighbours">Neigbhours of the tile to be expended (neighbours is in the agent's eye and not the map meaning, previously expended states can be excluded despite existing in the map)</param>
        /// <param name="removedTile">Tile to be expended</param>
        /// <returns>A boolean representing if the state can be pruned</returns>
        public Boolean Expenable(List<Tile> neighbours, Tile removedTile, HashSet<int> expendedIds)
        {
            if (neighbours == null)
                throw new System.ArgumentException("Parameter cannot be null", "neighbours");
            if (removedTile == null)
                throw new System.ArgumentException("Parameter cannot be null", "removedTile");
            if (neighbours.Contains(null))
                throw new System.ArgumentException("The list neighbours cannot contain nulls");
            // rotate in a clock wise fashion from smallest neighbours
            if (neighbours.Count == 8 || neighbours.Count == 0)
            {
                return false;
            }

            Tile firstNeighbour = neighbours[0];
            HashSet<Tile> unexploredNeighbours = new HashSet<Tile>();
            HashSet<Tile> exploredNeighbours = new HashSet<Tile>();
            unexploredNeighbours.Add(firstNeighbour);
            while (unexploredNeighbours.Count > 0)
            {
                Tile exploringNode = unexploredNeighbours.First();
                unexploredNeighbours.Remove(exploringNode);
                List<Tile> possibleNeighbours = exploringNode.getNonWallNeighbours().Where(t => !expendedIds.Contains(t.id) &&
                    neighbours.Contains(t) &&
                    !exploredNeighbours.Contains(t) &&
                    !unexploredNeighbours.Contains(t)).ToList();
                foreach(Tile n in possibleNeighbours)
                {
                    unexploredNeighbours.Add(n);
                }
                exploredNeighbours.Add(exploringNode);
            }
            
            return exploredNeighbours.Count() == neighbours.Count();
        }
        /// <summary>
        /// Calculates the median value for the list
        /// </summary>
        /// <param name="x">List to have the median value determined</param>
        /// <returns>Median value of the inputed list</returns>
        public decimal GetMedian(decimal[] x)
        {   
            if (x == null)
            {
                Console.WriteLine("Requested of median of null array");
                return decimal.MaxValue;
            }
            if (x.Length == 0)
            {
                Console.WriteLine("Requested of median of empty array");
                return 0;
            }
            decimal[] tempArray = x;
            int count = tempArray.Length;

            Array.Sort(tempArray);

            decimal medianValue = 0;

            if (count % 2 == 0)
            {
                // count is even, need to get the middle two elements, add them together, then divide by 2
                decimal middleElement1 = tempArray[(count / 2) - 1];
                decimal middleElement2 = tempArray[(count / 2)];
                medianValue = (middleElement1 + middleElement2) / 2;
            }
            else
            {
                // count is odd, simply get the middle element.
                medianValue = tempArray[(count / 2)];
            }

            return medianValue;
        }

        internal bool IsCardinalNeighbour(Tile t, Tile tile)
        {
            return t.col == tile.col || t.row == tile.row;
        }

        /// <summary>
        /// Gets the heuristic.
        /// </summary>
        /// <param name="mapHeight"></param>
        /// <param name="mapWidth"></param>
        /// <param name="goal"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        //public float[,] getHeuristics(int mapHeight, int mapWidth, Tile goal, string type)
        //{
        //    if (type == "perfect")
        //    {
        //        return getPerfectHeuristic(mapHeight, mapWidth, goal);
        //    } else if (type == "octile")
        //    {
        //        return getOctileHeuristic(mapHeight, mapWidth, goal.col, goal.row);

        //    }
        //    else if (type == "manhattan")
        //    {
        //        return getManhattanHeuristic(mapHeight, mapWidth, goal.col, goal.row);
        //    } else if (type == "euclidean")
        //    {
        //        return getEuclideanHeuristic(mapHeight, mapWidth, goal.col, goal.row);

        //    }
        //    else 
        //    {
        //        return getOctileHeuristic(mapHeight, mapWidth, goal.col, goal.row);
        //    }
        //}
        /// <summary>
        /// Gets the heuristic for 2 tiles.
        /// </summary>
        /// <param name="node1">node 1</param>

        /// <param name="node2">node 2</param>
        /// <param name="type">type of heuristic</param>
        /// <returns></returns>
        public decimal GetHeuristic(Tile node1, Tile node2, string type)
        {
            if (type == "octile")
            {
                return OctileDistance(node1.col, node1.row, node2.col, node2.row);

            }
            else if (type == "manhattan")
            {
                return GetManhattanDistance(node1.col, node1.row, node2.col, node2.row);
            }
            else if (type == "euclidean")
            {
                return GetEuclideanDistance(node1.col, node1.row, node2.col, node2.row);

            }
            else
            {
                return OctileDistance(node1.col, node1.row, node2.col, node2.row);
            }
        }
        /// <summary>
        /// Return straight-line distance
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        public decimal GetEuclideanDistance(int x1, int y1, int x2, int y2)
        {
            return Math.Round((decimal)Math.Sqrt(Math.Pow((x1 - x2),2) + Math.Pow((y1 - y2),2)),1);
        }

        /// <summary>
        /// Returns the Euclidean Distance for a square grid from the goal.
        /// </summary>
        /// <param name="mapHeight">Height of the 8 connected grid</param>
        /// <param name="mapWidth">Width of the 8 connected grid</param>
        /// <param name="goalX">X position of the goal</param>
        /// <param name="goalY">Y position for the goal</param>
        /// <returns>A 2d matrix with manhattan distance from the goal for each cell</returns>
        public decimal[,] GetEuclideanHeuristic(int mapHeight, int mapWidth, int goalX, int goalY)
        {
            if (mapHeight <= 0)
                throw new System.ArgumentException("Parameter cannot be less than 0", "mapHeight");
            if (mapWidth <= 0)
                throw new System.ArgumentException("Parameter cannot be less than 0", "mapWidth");
            if (goalX >= mapWidth)
                throw new System.ArgumentException("Your goal is larger in the x direction than the map");

            if (goalY >= mapHeight)
                throw new System.ArgumentException("Your goal is larger in the y direction than the map");
            if (goalY < 0)
                throw new System.ArgumentException("Goal Y Location must be a positive int");
            if (goalX < 0)
                throw new System.ArgumentException("Goal X Location must be a positive int");

            decimal[,] h = new decimal[mapHeight, mapWidth];
            for (int row = 0; row < mapHeight; row++)
            {
                for (int col = 0; col < mapWidth; col++)
                {
                    h[row, col] = GetEuclideanDistance(goalX, goalY, col, row);
                }
            }
            return h;
        }
        /// <summary>
        /// Return manhattan distance
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        public decimal GetManhattanDistance(int x1, int y1, int x2, int y2)
        {
            return (Math.Abs(x1 - x2) + Math.Abs(y1 - y2)) * multiple;
        }
        /// <summary>
        /// Returns the Manhattan Distance for a square grid from the goal.
        /// </summary>
        /// <param name="mapHeight">Height of the 8 connected grid</param>
        /// <param name="mapWidth">Width of the 8 connected grid</param>
        /// <param name="goalX">X position of the goal</param>
        /// <param name="goalY">Y position of the goal</param>
        /// <returns>A 2d matrix with manhattan distance from the goal for each cell</returns>
        public decimal[,] GetManhattanHeuristic(int mapHeight, int mapWidth, int goalX, int goalY)
        {
            if (mapHeight <= 0)
                throw new System.ArgumentException("Parameter cannot be less than 0", "mapHeight");
            if (mapWidth <= 0)
                throw new System.ArgumentException("Parameter cannot be less than 0", "mapWidth");
            if (goalX >= mapWidth)
                throw new System.ArgumentException("Your goal is larger in the x direction than the map");

            if (goalY >= mapHeight)
                throw new System.ArgumentException("Your goal is larger in the y direction than the map");
            if (goalY < 0)
                throw new System.ArgumentException("Goal Y Location must be a positive int");
            if (goalX < 0)
                throw new System.ArgumentException("Goal X Location must be a positive int");

            decimal[,] h = new decimal[mapHeight, mapWidth];
            for (int row = 0; row < mapHeight; row++)
            {
                for (int col = 0; col < mapWidth; col++)
                {
                    h[row, col] = GetManhattanDistance(goalX, goalY, col, row);
                }
            }
            return h;
        }
        /// <summary>
        /// Returns the Manhattan Distance for a square grid from the goal.
        /// </summary>
        /// <param name="mapHeight">Height of the 8 connected grid</param>
        /// <param name="mapWidth">Width of the 8 connected grid</param>
        /// <param name="goalX">X position of the goal</param>
        /// <param name="goalY">Y position of the goal</param>
        /// <returns>A 2d matrix with manhattan distance from the goal for each cell</returns>
        public decimal[,] GetOctileHeuristic(int mapHeight, int mapWidth, int goalX, int goalY)
        {
            if (mapHeight <= 0)
                throw new System.ArgumentException("Parameter cannot be less than 0", "mapHeight");
            if (mapWidth <= 0)
                throw new System.ArgumentException("Parameter cannot be less than 0", "mapWidth");
            if (goalX >= mapWidth)
                throw new System.ArgumentException("Your goal is larger in the x direction than the map");

            if (goalY >= mapHeight)
                throw new System.ArgumentException("Your goal is larger in the y direction than the map");
            if (goalY < 0)
                throw new System.ArgumentException("Goal Y Location must be a positive int");
            if (goalX < 0)
                throw new System.ArgumentException("Goal X Location must be a positive int");

            decimal[,] h = new decimal[mapHeight, mapWidth];
            for (int row = 0; row < mapHeight; row++)
            {
                for (int col = 0; col < mapWidth; col++)
                {
                    h[row, col] = OctileDistance(goalX, goalY, col, row);
                }
            }
            return h;
        }
        /// <summary>
        /// Labels the component
        /// </summary>
        /// <param name="tile">tile</param>
        /// <param name="labelNum">label</param>
        public void LabelConnected(Tile tile, int labelNum)
        {
            HashSet<Tile> explored = new HashSet<Tile>();
            HashSet<Tile> openList = new HashSet<Tile>();
            tile.labeled_component = labelNum;
            openList.Add(tile);
            while (openList.Count > 0)
            {
                Tile nextTile = openList.First(d=> true);
                nextTile.labeled_component = labelNum;
                openList.Remove(nextTile);
                explored.Add(nextTile);
                List<Tile> neighbours = nextTile.getNonWallNeighbours();
                foreach (Tile neighbour in neighbours)
                {
                    if (!explored.Contains(neighbour) && 
                        !openList.Contains(neighbour))
                    {
                        openList.Add(neighbour);
                    }
                }
            }
        }
        //private float[,] getPerfectHeuristic(int mapHeight, int mapWidth, Tile goal)
        //{
        //    float[,] g = new float[mapHeight, mapWidth];
        //    float cost = 0;
        //    HashSet<Tile> explored = new HashSet<Tile>();
        //    SimplePriorityQueue<Tile> frontier = new SimplePriorityQueue<Tile>();
        //    for (int row = 0; row < mapHeight; row++)
        //    {
        //        for (int col = 0; col < mapWidth; col++)
        //        {
        //            g[row, col] = float.MaxValue;
        //        }
        //    }
        //    g[goal.row, goal.col] = 0;
        //    frontier.Enqueue(goal, 0);
        //    Tile bestOpenTile;
        //    while (frontier.Count > 0)
        //    {

        //        bestOpenTile = frontier.Dequeue();
        //        explored.Add(bestOpenTile);
        //        List<Tile> openNeighbours = bestOpenTile.getNonWallNeighbours();
        //        cost = g[bestOpenTile.row, bestOpenTile.col];
        //        foreach(Tile connectingNode in openNeighbours)
        //        {
        //            if (explored.Contains(connectingNode))
        //            {
        //                continue;
        //            }

        //            // if it's not in the open list...
        //            float travelCost = this.getTravelCost(connectingNode, bestOpenTile);
        //            float tenativeG = cost + travelCost;
        //            if (!frontier.Contains(connectingNode))
        //            {
        //                // compute its score, set the parent
        //                g[connectingNode.row, connectingNode.col] = tenativeG;

        //                // and add it to the open list
        //                frontier.Enqueue(connectingNode, (float)tenativeG);
        //            }
        //            else
        //            {
        //                // test if using the current G score makes the adjacent square's F score
        //                // lower, if yes update the parent because it means it's a better path

        //                if (tenativeG >= g[connectingNode.row, connectingNode.col])
        //                {
        //                    continue;
        //                }
        //                else
        //                {
        //                    g[connectingNode.row, connectingNode.col] = tenativeG;
        //                    frontier.UpdatePriority(connectingNode, (float)tenativeG);
        //                }
        //            }
        //        }
        //    }
        //    return g;

        //}
        /// <summary>
        /// Returns a tile id based on the linear index
        /// </summary>
        /// <param name="row">row of the tile</param>
        /// <param name="col">column of the tile</param>
        /// <param name="rowLength">Length of a row</param>
        /// <returns></returns>
        public int GetTileId(int row, int col, int rowLength)
        {
            return GetLinearIndexFromRowCol(row, col, rowLength);
        }
        /// <summary>
        /// Calculates the Manhattan distance between two points.
        /// </summary>
        /// <param name="x1">X position of point 1</param>
        /// <param name="y1">Y position of point 1</param>
        /// <param name="x2">X position of point 2</param>
        /// <param name="y2">Y position of point 2</param>
        /// <returns>The octile distance</returns>
        public decimal OctileDistance(int x1, int y1, int x2, int y2)
        {
            try
            {
                checked
                {

                    long dx = Math.Abs((long)(x1 - x2));
                    long dy = Math.Abs((long)(y1 - y2));
                    decimal distance = _diagonalCost * Math.Min(dx, dy) + Math.Max(dx, dy) - Math.Min(dx, dy);
                    return distance * multiple;
                }
            } catch(OverflowException e)
            {
                Console.WriteLine("CHECKED and CAUGHT:  " + e.ToString());
                throw new ArgumentException("Your arguments resulted in a overflow error consider not computing a heuristic of such a distance.");
            }
        }
        
        /// <summary>
        /// Computes the travel cost assuming the tiles are neighbours in an 8 connected grid, and bi-direction symmetrical cost
        /// </summary>
        /// <param name="node1">Tile 1</param>
        /// <param name="node2">Tile 2</param>
        /// <returns></returns>
        public decimal GetTravelCost(Tile node1, Tile node2)
        {
          
            if (node1 == null)
            {
                Console.WriteLine("Travel of node1 is null");
                throw new ArgumentException("The nodes are not connected. To get an estimated distance use the appropriate heuristic estimate");
            }
            if (node2 == null)
            {
                Console.WriteLine("Travel of node2 is null");
                throw new ArgumentException("The nodes are not connected. To get an estimated distance use the appropriate heuristic estimate");
            }
            if (node1 == node2)
            {
                return 0;
            }
            if (!node1.getNeighbours(false).Contains(node2))
            {
                throw new ArgumentException("The nodes are not connected. To get an estimated distance use the appropriate heuristic estimate");
            }
            try
            {
                if (node1.row == node2.row || node1.col == node2.col)
                {
                    return _cardinalCost * multiple;
                } else
                {
                    return _diagonalCost * multiple;
                }
                //float dist = octileDistance(node1.col, node1.row, node2.col, node2.row);
                //return dist;
            }
            catch (ArgumentException)
            {
                throw;
            }
        }
        /// <summary>
        /// Loads in a MovingAI formatted map
        /// The maps have the following format:
/*
 * All maps begin with the lines:
 * type octile
 * height x
 * width y
 * map
 * 
 * where x and y are the repsective height and width of the map.
 * The map data is store as an ASCII grid.The upper-left corner of the map is (0,0). The following characters are possible:
 * . - passable terrain
 * G - passable terrain
 * @ - out of bounds
 * O - out of bounds
 * T - trees(unpassable)
 * S - swamp( *UNPASSABLE*) MovingAI has other meaning
 * W - water(*UNPASSABLE*) MovingAI has other meaning
 */
        /// </summary>
        /// <returns>1/0 formatted map string</returns>
        public string[] LoadMovingAIMap(string fileName)
        {
            string[] fileText = System.IO.File.ReadAllLines(fileName);
            string type = fileText[0];
            int height = int.Parse(fileText[1].Split(' ')[1]);
            int width = int.Parse(fileText[2].Split(' ')[1]);
            string[] mapText = new string[height];
            int rowsAdded = 0;
            for (int i = 4; i < 4 + height; i++)
            {
                string row = fileText[i];
                StringBuilder convertedRow = new StringBuilder();
                for (int j = 0; j < width; j++)
                {
                    char tile = row[j];
                    convertedRow.Append(CovertMovingAICharToBinaryWallFormat(tile));
                }
                mapText[rowsAdded] = convertedRow.ToString();
                rowsAdded++;
            }
            return mapText;
        }
        /// <summary>
        /// converts moving ai map
        /// </summary>
        /// <param name="unformattedChar">raw char</param>
        ///  
/*
 * . - passable terrain
 * G - passable terrain
 * @ - out of bounds
 * O - out of bounds
 * T - trees(unpassable)
 * S - swamp( *UNPASSABLE*) MovingAI has other meaning
 * W - water(*UNPASSABLE*) MovingAI has other meaning
 */
        /// <returns>parsed char</returns>
        public char CovertMovingAICharToBinaryWallFormat(char unformattedChar)
        {
            if (unformattedChar.Equals('.'))
            {
                return '0';
            } else if (unformattedChar.Equals('G'))
            {
                return '0';
            } else
            {
                return '1';
            }
        } 
        /// <summary>
        /// Determines the Average scrubbing for a list of state Ids. Simply put the number of unique ids over the number of ids.
        /// </summary>
        /// <param name="states">State ids</param>
        /// <returns>average amount an id appears in the list.</returns>
        public float CalculateScrubbing(List<int> states)
        {
            if (states == null)
                return 0.0f;
            float uniqueStates = (float) states.Distinct().Count();
            float visits = (float)states.Count();
            return visits / uniqueStates;
        }
    }

}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RTSGame.Models.Configurations;
using RTSGame.Models.ExperimentModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.View
{
    /// <summary>
    /// Base Agent View that is the default view type
    /// </summary>
    class AgentView
    {
        /// <summary>
        /// Struct for agent textures
        /// </summary>
        private struct Textures
        {
            public Texture2D agent { get; set; }
            public Texture2D goal { get; set; }
            public Texture2D heurstic { get; set; }
        }
        private Textures _textures = new Textures();
        private int sizeOfSprites;
        private Utilities utils = new Utilities();
        public SearchAgent selectedAgent;
        public int xoffset { get; set; } = 0;
        public int yoffset { get; set; } = 0;
        /// <summary>
        /// Type of view
        /// </summary>
        public string viewType { get; set; }
        /// <summary>
        /// List of all agents to draw
        /// </summary>
        public List<SearchAgent> agents { get; set; }
        /// <summary>
        /// Set the agents that the view will draw
        /// </summary>
        /// <param name="agents">Agents to be drawn</param>
        public void setAgents(List<SearchAgent> agents)
        {
            this.agents = agents;
        }
        /// <summary>
        /// Sets the views agent
        /// </summary>
        /// <param name="agent">Primary Agent to draw </param>
        public virtual void setAgent(SearchAgent agent)
        {
            this.selectedAgent = agent;
        }
        private SpriteFont fontTexture;
        /// <summary>
        /// Creates the veiw that draws the agents 
        /// </summary>
        /// <param name="sizeOfSprites">size of sprite</param>
        /// <param name="agentSprite">Sprite for the agent</param>
        /// <param name="goal">Sprite for the goal</param>
        /// <param name="heuristic">Sprite for the heuristic</param>
        /// <param name="type">Type of agent</param>
        /// <param name="fontTexture">font</param>
        public AgentView(int sizeOfSprites, Texture2D agentSprite,
            Texture2D goal, Texture2D heuristic, SpriteFont fontTexture, string type)
        {
            this.sizeOfSprites = sizeOfSprites;
            _textures.agent = agentSprite;
            _textures.goal = goal;
            _textures.heurstic = heuristic;
            this.fontTexture = fontTexture;
            this.viewType = type;

            // Add "ListChanged" to the Changed event on "List".
            //_gameModel.changedScenarios += new ChangedEventHandler(agentsChanged);
            //_agent = agent;
        }
        public float getXPostion(int xPosition, float xScale)
        {
            return xPosition * sizeOfSprites * xScale + xoffset;
        }
        public float getYPostion(int yPosition, float yScale)
        {
            return yPosition * sizeOfSprites * yScale + yoffset;
        }
        private void drawAllAgents(SpriteBatch spriteBatch, Vector2 nonuniformscale)
        {
            float xScale = nonuniformscale.X;
            float yScale = nonuniformscale.Y;
            foreach (SearchAgent agent in agents)
            {
                var xGoalPos = getXPostion(agent.goal.col, xScale);
                var yGoalPos = getYPostion(agent.goal.row, yScale);
                var goalPosition = new Vector2(xGoalPos, yGoalPos);

                if (!agent.reachedGoal)
                {
                    spriteBatch.Draw(_textures.goal, goalPosition, null, Color.White, 0f,
                            Vector2.Zero, nonuniformscale, SpriteEffects.None, 0.2f);
                }
                var xCurPos = getXPostion(agent.currentSpot.col, xScale);
                var yCurPos = getYPostion(agent.currentSpot.row, yScale);
                var xStartPos = getXPostion(agent.startingLocation.col, xScale);
                var yStartPos = getYPostion(agent.startingLocation.row, yScale);
                var startPos = new Vector2(xStartPos, yStartPos);
                var curPosition = new Vector2(xCurPos, yCurPos);
                spriteBatch.Draw(_textures.agent, curPosition, null, agent.reachedGoal ? Color.Green : Color.White, 0f,
                        Vector2.Zero, nonuniformscale, SpriteEffects.None, 0.05f);
                if (VisualizationParameters.showStart)
                {
                    spriteBatch.Draw(_textures.heurstic, startPos, null, Color.LightGreen, 0f, Vector2.Zero, nonuniformscale, SpriteEffects.None, 0.1f);

                }
                if (VisualizationParameters.showAgentId)
                {
                    var midPos = new Vector2(xCurPos + (sizeOfSprites * xScale / 4), yCurPos + (sizeOfSprites * yScale / 4));
                    spriteBatch.DrawString(fontTexture, agent.id.ToString(), midPos, Color.Blue);
                    var midGoalPos = new Vector2(xGoalPos + (sizeOfSprites *  3 * xScale / 4), yGoalPos + (sizeOfSprites *3* yScale / 4));
                    spriteBatch.DrawString(fontTexture, agent.id.ToString(), midGoalPos, Color.Blue);
                    var midStartPos = new Vector2(xStartPos + (sizeOfSprites * 3 * xScale / 4), yStartPos + (sizeOfSprites * 3 * yScale / 4));
                    spriteBatch.DrawString(fontTexture, agent.id.ToString(), midStartPos, Color.Blue);
                }
            }
        }
        
        public VisualizationParameters VisualizationParameters = new VisualizationParameters()
        {
            showAgentId = true,
            showHeuristic = false,
        };
        public void setVisualizationParameters(VisualizationParameters visualizationParameters)
        {
            this.VisualizationParameters = visualizationParameters;
            yoffset = visualizationParameters.titleBuffer;
        }
        /// <summary>
        /// Draw function for the the view
        /// </summary>
        /// <param name="spriteBatch">Batch for drawing</param>
        /// <param name="nonuniformscale">scaling factor</param>
        
        public virtual void draw(SpriteBatch spriteBatch, Vector2 nonuniformscale)
        {
            drawAllAgents(spriteBatch, nonuniformscale);
            //if (bool.Parse(ConfigurationManager.AppSettings["showHeuristic"]))
            //{
            if (agents.Count > 0)
            {
                selectedAgent = agents[0];
               
                drawHeuristic(spriteBatch, nonuniformscale, sizeOfSprites);
                
            }
        }
        /// <summary>
        /// Function for drawing the selected agents heuristic update. White is no update and Dark is the most updated
        /// </summary>
        /// <param name="spriteBatch">Batch being drawn</param>
        /// <param name="nonuniformscale">Scaling factor</param>
        /// <param name="sizeOfSprites">size of the sprite</param>
        public virtual void drawHeuristic(SpriteBatch spriteBatch, Vector2 nonuniformscale, int sizeOfSprites)
        {
            float xScale = nonuniformscale.X;
            float yScale = nonuniformscale.Y;
            Dictionary<int, decimal> map = this.selectedAgent.hMap;
            GridMap gridWorld = this.selectedAgent.currentSpot.gridMap;
            if (map.Count == 0)
            {
                return;
            }
           // float maxVal = (float)map.Max(kvp => kvp.Value);
            float maxVal = 50;
            float minVal = 0;
            
            //Console.WriteLine("Max value: {0}",maxVal);
            
            //float minVal = (float)map.Min(kvp => kvp.Value);
            for (int row = 0; row < gridWorld.height; row++)
            {
                for (int col = 0; col < gridWorld.width; col++)
                {
                    Tile tile = gridWorld.getTile(row, col);
                    var position = new Vector2(getXPostion(col, xScale), getYPostion(row, yScale));
                    if (tile.isWall()) {
                        spriteBatch.Draw(_textures.heurstic, position, null, new Color(Color.Black, 1f), 0f,
                            Vector2.Zero, nonuniformscale, SpriteEffects.None, .9f);
                    } else if (map.ContainsKey(tile.id))
                    {
                        if (VisualizationParameters.showHeuristic)
                        {
                            float updatePercent = ((float)map[tile.id]) / (maxVal);
                            spriteBatch.Draw(_textures.heurstic, position, null, new Color(Color.Purple, updatePercent), 0f,
                                Vector2.Zero, nonuniformscale, SpriteEffects.None, .9f);
                        }
                    } else
                    {
                        
                        spriteBatch.Draw(_textures.heurstic, position, null, new Color(VisualizationParameters.hideUnseenNodes ?
                            Color.Black :
                            Color.White, VisualizationParameters.hideUnseenNodes ? .9f : 1f), 0f,
                            Vector2.Zero, nonuniformscale, SpriteEffects.None, 1f);
                        
                    }
                }
            }
            if (VisualizationParameters.showHeuristic)
            {
                drawClosedList(spriteBatch, nonuniformscale, sizeOfSprites);
                drawOpenList(spriteBatch, nonuniformscale, sizeOfSprites);
                drawPathToDesiredNode(spriteBatch, nonuniformscale, sizeOfSprites);
            }
        }
        private void drawClosedList(SpriteBatch spriteBatch, Vector2 nonuniformscale, int sizeOfSprites)
        {
            float xScale = nonuniformscale.X;
            float yScale = nonuniformscale.Y;
            foreach (Tile tile in this.selectedAgent.closedList)
            {
                var position = new Vector2(getXPostion(tile.col, xScale), getYPostion(tile.row, yScale));
                spriteBatch.Draw(_textures.heurstic, position, null, new Color(Color.LightCyan, 1f), 0f,
                    Vector2.Zero, nonuniformscale, SpriteEffects.None, .85f);
            }
        }
        private void drawPathToDesiredNode(SpriteBatch spriteBatch, Vector2 nonuniformscale, int sizeOfSprites)
        {
            float xScale = nonuniformscale.X;
            float yScale = nonuniformscale.Y;
            
            foreach (KeyValuePair<int, Tile> pair in this.selectedAgent.pathToDesiredNode)
            {
                Tile tile = pair.Value;
                var position = new Vector2(getXPostion(tile.col, xScale), getYPostion(tile.row, yScale));
                spriteBatch.Draw(_textures.heurstic, position, null, new Color(Color.Goldenrod, 0.5f), 0f,
                    Vector2.Zero, nonuniformscale, SpriteEffects.None, .75f);
            }
            
        }
        private void drawOpenList(SpriteBatch spriteBatch, Vector2 nonuniformscale, int sizeOfSprites)
        {
            float xScale = nonuniformscale.X;
            float yScale = nonuniformscale.Y;
            
            foreach (Tile tile in this.selectedAgent.openQueue)
            {
                var position = new Vector2(getXPostion(tile.col, xScale), getYPostion(tile.row, yScale));
                spriteBatch.Draw(_textures.heurstic, position, null, new Color(Color.Blue, 1f), 0f,
                    Vector2.Zero, nonuniformscale, SpriteEffects.None, .80f);
            }
        }
    }
    abstract class ViewCreator
    {
        public abstract AgentView FactoryMethod(int sizeOfSprites, Texture2D agentSprite,
            Texture2D goal, Texture2D heuristic, SpriteFont fontTexture, string agentType);
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RTSGame.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.View.AgentViews
{
    class AgentViewController
    {
        private AgentView currentView = null;
        private AgentViewFactory viewFactory = new AgentViewFactory();
        private SpriteFont fontTexture;
        int sizeOfSprites;
        Texture2D agentSprite;
        Texture2D goal;
        Texture2D heuristic;
        /// <summary>
        /// View Controller that creates the required views and wraps the draw/change agent functions
        /// </summary>
        /// <param name="agent"> Agent the view is drawing for </param>  
        /// <param name="sizeOfSprites"> size of square sprite </param> 
        /// <param name="agentSprite"> sprite of the agent </param>
        /// <param name="goal"> sprite for the goal </param> 
        /// <param name="heuristic"> sprite for the heuristic </param> 
        /// <param name="fontTexture">font texture</param>
        public AgentViewController(int sizeOfSprites, Texture2D agentSprite,
            Texture2D goal, Texture2D heuristic, SpriteFont fontTexture)
        {
            this.fontTexture = fontTexture;
            this.goal = goal;
            this.heuristic = heuristic;
            this.agentSprite = agentSprite;
            this.sizeOfSprites = sizeOfSprites;
            currentView = new WindowAStarView(sizeOfSprites, agentSprite, goal, heuristic, fontTexture, "search");
            //currentView = viewFactory.createAgentView(sizeOfSprites, agentSprite, goal, heuristic, fontTexture);
            //setAgent(agent, sizeOfSprites, agentSprite, goal, heuristic, fontTexture);
        }
        /// <summary>
        /// Set the agents that the view will draw
        /// </summary>
        /// <param name="agents">Agents to be drawn</param>
        public void setAgents(List<SearchAgent> agents)
        {
            currentView.setAgents(agents);
        }
        /// <summary>
        /// Updates the current view to the new agent. If the agent is of a different type uses the factory to create the new view
        /// </summary>
        /// <param name="agent"> Agent the view is drawing for </param>  
        public void setAgent(SearchAgent agent)
        {
            if (currentView != null && currentView.viewType == agent.agentType)
            {
                currentView.setAgent(agent);
            } else
            {
                currentView = viewFactory.createAgentView(agent, sizeOfSprites, agentSprite, goal, heuristic, fontTexture);
            }
        }
      
        /// <summary>
        /// Draw wrapper for the current view
        /// </summary>
        /// <param name="spriteBatch"> current batch being drawn </param> 
        /// <param name="nonuniformscale"> nonumiform scale vector </param> 
        public void draw(SpriteBatch spriteBatch, Vector2 nonuniformscale)
        {
            currentView.draw(spriteBatch, nonuniformscale);
        }

        internal void setVisualizationParameters(VisualizationParameters visualizationParameters)
        {
            currentView.setVisualizationParameters(visualizationParameters);
        }
    }
}

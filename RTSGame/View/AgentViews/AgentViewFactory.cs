﻿using Microsoft.Xna.Framework.Graphics;
using RTSGame.Models.ExperimentModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.View.AgentViews
{
    /// <summary>
    /// Search agent view factory
    /// </summary>
    class AgentViewFactory
    {
        
        /// <summary>
        /// Agent factory to create desired view
        /// </summary>
        public AgentViewFactory()
        {
        }
        /// <summary>
        /// Creates the agents View based on the agent type
        /// </summary>
        /// <param name="agent">Agent to be drawn </param>
        /// <param name="sizeOfSprites">Size of sprties</param>
        /// <param name="agentSprite">Sprite for the agent</param>
        /// <param name="goal">Sprite for the goal</param>
        /// <param name="heuristic">sprite for the heuristic</param>
        /// <param name="fontTexture">font sprite</param>
        /// <returns></returns>
        public AgentView createAgentView(SearchAgent agent, int sizeOfSprites, Texture2D agentSprite,
            Texture2D goal, Texture2D heuristic, SpriteFont fontTexture)
        {
            Console.WriteLine(agent.agentType);
            if (agent.agentType == "uLRTA")
            {
                return new uLRTAAgentView(sizeOfSprites, agentSprite, goal, heuristic, fontTexture, agent.agentType);
            } else if (agent.agentType == "AStar")
            {
                return new AStarView(sizeOfSprites, agentSprite, goal, heuristic, fontTexture, agent.agentType);
            } else if (agent.gene.name== "whca")
            {
                return new WindowAStarView(sizeOfSprites, agentSprite, goal, heuristic, fontTexture, agent.agentType);
            }
            else
            {
                return new AgentView(sizeOfSprites, agentSprite, goal, heuristic, fontTexture, agent.agentType); // Default type if unknown;
            }
        }
    }
}

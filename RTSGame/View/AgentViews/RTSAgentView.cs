﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RTSGame.Models.ExperimentModels;
using System.Collections.Generic;
using System.Linq;

namespace RTSGame.View.AgentViews
{
    class uLRTAAgentView : AgentView
    {
        /// <summary>
        /// Struct for the texture of the view
        /// </summary>
        private struct Textures
        {
            public Texture2D agent { get; set; }
            public Texture2D goal { get; set; }
            public Texture2D heurstic { get; set; }
        }
        private Textures _textures = new Textures();
        private Utilities utils = new Utilities();
        public ULRTAAgent agent;
        /// <summary>
        /// Creates the view for the uLRTA agents
        /// </summary>
        /// <param name="agent"> Agent the view is drawing for </param>  
        /// <param name="sizeOfSprites"> size of square sprite </param> 
        /// <param name="agentSprite"> sprite of the agent </param>
        /// <param name="goal"> sprite for the goal </param> 
        /// <param name="heuristic"> sprite for the heuristic </param> 
        /// <param name="type"> sprite for the heuristic</param> 
        /// <param name="fontTexture">font</param>
        public uLRTAAgentView(int sizeOfSprites, Texture2D agentSprite,
            Texture2D goal, Texture2D heuristic, SpriteFont fontTexture, string type) : base(sizeOfSprites, agentSprite, goal, heuristic, fontTexture, type)
        {
            this.viewType = type;
            this._textures.agent = agentSprite;
            this._textures.goal = goal;
            this._textures.heurstic = heuristic;
        }
        /// <summary>
        /// Sets the view's agent to the inputed agent
        /// </summary>
        /// <param name="agent">Search agent to be drawn</param>
        public override void setAgent(SearchAgent agent)
        {
            base.setAgent(agent);
            this.agent = (ULRTAAgent)agent;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="spriteBatch"> Batch being drawn.</param>
        /// <param name="nonuniformscale"> scaling vector</param>
        /// <param name="sizeOfSprites"> size of the sprite being drawn </param>
        /// <summary>
        /// Function for drawing the selected agents heuristic update. White is no update and Dark is the most updated
        /// </summary>
        /// <param name="spriteBatch">Batch being drawn</param>
        /// <param name="nonuniformscale">Scaling factor</param>
        /// <param name="sizeOfSprites">size of the sprite</param>
        public override void drawHeuristic(SpriteBatch spriteBatch, Vector2 nonuniformscale, int sizeOfSprites)
        {
            base.drawHeuristic(spriteBatch, nonuniformscale, sizeOfSprites);
            if (VisualizationParameters.showHeuristic)
            {
                HashSet<int> expendedTiles = ((ULRTAAgent)selectedAgent).expendedTiles;
                float xScale = nonuniformscale.X;
                float yScale = nonuniformscale.Y;
                foreach (int id in expendedTiles)
                {
                    int col = utils.GetLeft32Bit(id);
                    int row = utils.GetRight32Bit(id);
                    var position = new Vector2(getXPostion(col, xScale), getYPostion(row, yScale));
                    spriteBatch.Draw(_textures.heurstic, position, null, new Color(Color.SlateGray, 1f), 0f,
                      Vector2.Zero, nonuniformscale, SpriteEffects.None, .1f);
                }
            }
        }
    }
    /// <summary>
    /// ULRTA Factory view creator
    /// </summary>
    class uLRTAViewCreator : ViewCreator
    {
        /// <summary>
        /// Factory method for uLRTA Agents
        /// </summary>
        /// <param name="sizeOfSprites"> size of the sprites being drawn</param>
        /// <param name="agentSprite"> uLRTA agent's sprite</param>
        /// <param name="goal"> agent's goal sprite</param>
        /// <param name="heuristic"> agent's heuristic sprite </param>
        /// <param name="agentType"> type of agent being drawn</param>
        /// <param name="spriteFont">font</param>
        /// <returns></returns>
        public override AgentView FactoryMethod(int sizeOfSprites, Texture2D agentSprite,
            Texture2D goal, Texture2D heuristic, SpriteFont spriteFont, string agentType)
        {
            return new uLRTAAgentView(sizeOfSprites, agentSprite, goal, heuristic, spriteFont, agentType);
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RTSGame.Models.SearchModels;

namespace RTSGame.View.AgentViews
{
    class WindowAStarView : AgentView
    {
        /// <summary>
        /// Struct for the texture of the view
        /// </summary>
        private struct Textures
        {
            public Texture2D agent { get; set; }
            public Texture2D goal { get; set; }
            public Texture2D heurstic { get; set; }
        }
        /// <summary>
        /// Sets the view's agent to the inputed agent
        /// </summary>
        /// <param name="agent">Search agent to be drawn</param>
        public override void setAgent(SearchAgent agent)
        {
            base.setAgent(agent);
            this.agent = (WindowedHierarchicalAStar)agent;
        }

        private Textures _textures = new Textures();
        private Utilities utils = new Utilities();
        public WindowedHierarchicalAStar agent;
        /// <summary>
        /// Creates the view for the uLRTA agents
        /// </summary>
        /// <param name="sizeOfSprites"> size of square sprite </param> 
        /// <param name="agentSprite"> sprite of the agent </param>
        /// <param name="goal"> sprite for the goal </param> 
        /// <param name="heuristic"> sprite for the heuristic </param> 
        /// <param name="type"> sprite for the heuristic</param> 
        /// <param name="fontTexture">font</param>
        public WindowAStarView(int sizeOfSprites, Texture2D agentSprite,
            Texture2D goal, Texture2D heuristic, SpriteFont fontTexture, string type):base(sizeOfSprites, agentSprite, goal, heuristic,fontTexture, type)
        {
            this.viewType = type;
            this._textures.agent = agentSprite;
            this._textures.goal = goal;
            this._textures.heurstic = heuristic;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="spriteBatch"> Batch being drawn.</param>
        /// <param name="nonuniformscale"> scaling vector</param>
        /// <param name="sizeOfSprites"> size of the sprite being drawn </param>
        public override void drawHeuristic(SpriteBatch spriteBatch, Vector2 nonuniformscale, int sizeOfSprites)
        {
            float xScale = nonuniformscale.X;
            float yScale = nonuniformscale.Y;
            WindowedHierarchicalAStar agent = this.agent;
            if (agent == null)
            {
                return;
            }
            SortedSet<TimedTile> openQueue;
            try
            {
                openQueue = agent.openQueue;
            }
            catch
            {
                return;
            }
            HashSet<long> openList = agent.openHashSet;
            HashSet<TimedTile> closedList = agent.closedList;
            Dictionary<Int64, TimedTile> aStarChain = agent.parentTileMapping;
            Dictionary<Int64, TimedTile> principalVariation = agent.principalVariation;
            // draw OPEN list
            foreach(long id in openList)
            {
                TimedTile TimedTile = agent.GetTimedTile(id);
                Tile tile = TimedTile.tile;
                int col = tile.col;
                int row = tile.row;
                var position = new Vector2(getXPostion(col, xScale), getYPostion(row, yScale));
                if (tile != agent.currentSpot && tile != agent.goal)
                {
                    spriteBatch.Draw(_textures.heurstic, position, null, Color.Blue, 0f,
                        Vector2.Zero, nonuniformscale, SpriteEffects.None, .1f);
                }

            }
            // draw closed list
            foreach (TimedTile timedTile in closedList)
            {
                Tile tile = timedTile.tile;
                int col = tile.col;
                int row = tile.row;
                var position = new Vector2(getXPostion(col,xScale),getYPostion(row,yScale));
                if (tile != agent.currentSpot && tile != agent.goal)
                {
                    spriteBatch.Draw(_textures.heurstic, position, null, Color.White, 0f,
                        Vector2.Zero, nonuniformscale, SpriteEffects.None, .1f);
                }
            }
            
            foreach (KeyValuePair<long, TimedTile> pair in principalVariation)
            {
                Tile tile = pair.Value.tile;
                int col = tile.col;
                int row = tile.row;
                var position = new Vector2(getXPostion(col,xScale),getYPostion(row,yScale));
                if (tile != agent.currentSpot && tile != agent.goal)
                {
                    spriteBatch.Draw(_textures.heurstic, position, null, Color.Red, 0f,
                        Vector2.Zero, nonuniformscale, SpriteEffects.None, .09f);
                }
            }
        }
        
    }
    /// <summary>
    /// ULRTA Factory view creator
    /// </summary>
    class windowAStarViewCreator : ViewCreator
    {
        /// <summary>
        /// Factory method for uLRTA Agents
        /// </summary>
        /// <param name="sizeOfSprites"> size of the sprites being drawn</param>
        /// <param name="agentSprite"> uLRTA agent's sprite</param>
        /// <param name="goal"> agent's goal sprite</param>
        /// <param name="heuristic"> agent's heuristic sprite </param>
        /// <param name="agentType"> type of agent being drawn</param>
        /// <param name="spriteFont">font</param>
        /// <returns></returns>
        public override AgentView FactoryMethod(int sizeOfSprites, Texture2D agentSprite,
            Texture2D goal, Texture2D heuristic, SpriteFont spriteFont, string agentType)
        {
            return new WindowAStarView(sizeOfSprites, agentSprite, goal, heuristic, spriteFont, agentType);
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.View
{
    class ScoreView
    {
        private SpriteFont _font;
        public ScoreView(SpriteFont font)
        {
            _font = font;
        }
        public void drawName(SpriteBatch spriteBatch, string type, int id, string problemType)
        {
            spriteBatch.DrawString(_font, "Agent(" + id.ToString() + ") :    " + type, new Vector2(20, 10), Color.Black);
            spriteBatch.DrawString(_font, "Problem type: " + problemType, new Vector2(20, 30), Color.Black);
        }
        public void draw(SpriteBatch spriteBatch, Boolean solved, float distance, float scrubbing)
        {

            // TODO: Add your drawing code here
            //spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            //spriteBatch.DrawString(_font, "At Goal:   " + solved.ToString(), new Vector2(20, 25), Color.Black);
            spriteBatch.DrawString(_font, "Distance Traveled: " + Math.Round(distance, 2).ToString(), new Vector2(20, 30), Color.Black);
            spriteBatch.DrawString(_font, "Scrubbing: " + Math.Round(scrubbing, 2).ToString(), new Vector2(350, 30), Color.Black);
        }
    }
}

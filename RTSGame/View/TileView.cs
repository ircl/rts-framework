﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RTSGame.Models.ExperimentModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.View
{
    class TileView
    {
        private Textures _textures = new Textures();
        private int sizeOfSprites;
        private Utilities utils = new Utilities();
        private struct Textures
        {
            public Texture2D floor { get; set; }
            public Texture2D wall { get; set; }
        }
        SpriteFont font;
        public TileView(int sizeOfSprites, Texture2D wall, Texture2D floor, SpriteFont font)
        {
            this.sizeOfSprites = sizeOfSprites;
            _textures.wall = wall;
            _textures.floor = floor;
            this.font = font;
        }
        public int yOffset { get; set; } = 0;
        public void draw(SpriteBatch spriteBatch, GameModel gameModel, Vector2 nonuniformscale)
        {

            // TODO: Add your drawing code here
            //spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);


            float xScale = nonuniformscale.X;
            float yScale = nonuniformscale.Y;

            for (int row = 0; row < gameModel.mapHeight(); row++)
            {
                for (int col = 0; col < gameModel.mapWidth(); col++)
                {
                    Tile tile = gameModel.getTileAt(row,col);
                    var position = new Vector2(col * sizeOfSprites * xScale, row * sizeOfSprites * yScale + yOffset);

                    if (tile.isWall())
                    {
                        spriteBatch.Draw(_textures.wall, position, null, Color.White, 0f,
                            Vector2.Zero, nonuniformscale, SpriteEffects.None, .1f);
                    } else
                    {
                        spriteBatch.Draw(_textures.floor, position, null, Color.White, 0f,
                           Vector2.Zero, nonuniformscale, SpriteEffects.None, 1f);
                    }

                    //var midPos = new Vector2(col * sizeOfSprites * xScale + (sizeOfSprites * xScale / 4), row * sizeOfSprites * yScale + (sizeOfSprites * yScale / 4));
                    //spriteBatch.DrawString(this.font, tile.labeled_component.ToString(), midPos, Color.Blue);
                }
            }
        }
    }
}

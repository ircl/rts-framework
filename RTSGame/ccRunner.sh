#!/bin/bash
#SBATCH --array=0-839:10
#SBATCH --job-name=RTSGame
#SBATCH --time 02:59:59
#SBATCH --mem=4000M
#SBATCH --error=aiide/logs/RTSGame%A%a.err
#SBATCH --output=aiide/logs/RTSGame%A%a.out
#SBATCH --mail-user=dbsigurd@ualberta.ca
#SBATCH --mail-type=ALL
module load nixpkgs/16.09
module load gcc/5.4.0
module load mono/5.4.0.56
for n in $(seq 0 10); do
    workunit=$(($SLURM_ARRAY_TASK_ID+$n))
	mono RTSGame.exe experiment.json ${workunit}
done

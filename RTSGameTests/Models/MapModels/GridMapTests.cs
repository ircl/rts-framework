﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RTSGame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Tests
{
    [TestClass()]
    public class GridMapTests
    {
       

        [TestMethod()]
        public void isLeftFlowTest()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile topLeftTile = gMap.getTile(1, 1);
            bool actual = gMap.isLeftFlow(topLeftTile);
            bool expected = true;
            Assert.AreEqual(actual, expected);
            Tile secondTopLeftTile = gMap.getTile(2, 1);
            actual = gMap.isLeftFlow(secondTopLeftTile);
            expected = false;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void isUpFlowTest()
        {
            string[] mapString = new string[]
                        {
                "000",
                "000",
                "000"
                        };
            GridMap gMap = new GridMap(mapString, "1");
            Tile topLeftTile = gMap.getTile(1, 1);
            bool actual = gMap.isUpFlow(topLeftTile);
            bool expected = true;
            Assert.AreEqual(actual, expected);
            Tile secondLeftTopTile = gMap.getTile(1, 2);
            actual = gMap.isUpFlow(secondLeftTopTile);
            expected = false;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void isSinkTest()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile topLeftTile = gMap.getTile(1,1);
            bool actual = gMap.isSink(topLeftTile);
            bool expected = true;
            Assert.AreEqual(expected, actual);

            Tile bottomRightTile = gMap.getTile(3, 3);
            actual = gMap.isSink(bottomRightTile);
            expected = false;
            Assert.AreEqual(expected, actual);

            Tile midTile = gMap.getTile(2,2);
            actual = gMap.isSink(midTile);
            expected = false;
            Assert.AreEqual(expected, actual);

            Tile topRightTile = gMap.getTile(1, 3);
            actual = gMap.isSink(topRightTile);
            expected = false;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void isSourceTest()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile topLeftTile = gMap.getTile(1,1);
            bool actual = gMap.isSource(topLeftTile);
            bool expected = false;
            Assert.AreEqual(expected, actual);

            Tile bottomRightTile = gMap.getTile(3,3);
            actual = gMap.isSource(bottomRightTile);
            expected = true;
            Assert.AreEqual(expected, actual);

            Tile midTile = gMap.getTile(2,2);
            actual = gMap.isSource(midTile);
            expected = false;
            Assert.AreEqual(expected, actual);

            Tile topRightTile = gMap.getTile(1, 3);
            actual = gMap.isSource(topRightTile);
            expected = false;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void getVerticalFlowNeighbourTest()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile topLeftTile = gMap.getTile(1,1);

            List<Tile> vertN = gMap.getVerticalFlowNeighbour(topLeftTile).Where(t => !t.isWall()).ToList();
            int actual = vertN.Count;
            int expected = 0;
            Assert.AreEqual(expected, actual);

            Tile secondTopleft = gMap.getTile(2, 1);
            vertN = gMap.getVerticalFlowNeighbour(secondTopleft).Where(t => !t.isWall()).ToList();
            actual = vertN.Count;
            expected = 1;
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(topLeftTile, vertN[0]);

            Tile secondTopSecondleft = gMap.getTile(2,2);
            vertN = gMap.getVerticalFlowNeighbour(secondTopSecondleft).Where(t => !t.isWall()).ToList();
            actual = vertN.Count;
            expected = 1;
            Tile expectedTile = gMap.getTile(3, 2);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedTile, vertN[0]);
        }

        [TestMethod()]
        public void getHorizontalFlowNeighbourTest()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile topLeftTile = gMap.getTile(1,1);

            List<Tile> hN = gMap.getHorizontalFlowNeighbour(topLeftTile).Where(t=> !t.isWall()).ToList();
            int actual = hN.Count;
            int expected = 0;
            Assert.AreEqual(expected, actual);

            Tile topSecondLeft = gMap.getTile(1, 2);
            hN = gMap.getHorizontalFlowNeighbour(topSecondLeft).Where(t => !t.isWall()).ToList();
            actual = hN.Count;
            expected = 1;
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(topLeftTile, hN[0]);

            Tile secondTopSecondleft = gMap.getTile(2, 2);
            hN = gMap.getHorizontalFlowNeighbour(secondTopSecondleft).Where(t => !t.isWall()).ToList();
            actual = hN.Count;
            expected = 1;
            Tile expectedTile = gMap.getTile(2, 3);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(expectedTile, hN[0]);
        }
        [TestMethod()]
        public void getNeighbourTest1()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");
            
            Tile topLeftTile = gMap.getTile(1, 1);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbourTest2()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile tile = gMap.getTile(1, 2);
            List<Tile> actualNeighbours = tile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,1),
                gMap.getTile(2,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbourTest3()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile tile = gMap.getTile(1, 3);
            List<Tile> actualNeighbours = tile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbourTest4()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");
            
            Tile tile = gMap.getTile(2, 1);
            List<Tile> actualNeighbours = tile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,1),
                gMap.getTile(2,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbourTest5()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile tile = gMap.getTile(2,2);
            List<Tile> actualNeighbours = tile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(3,2),
                gMap.getTile(2,3),
                gMap.getTile(3,3)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbourTest6()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile tile = gMap.getTile(2, 3);
            List<Tile> actualNeighbours = tile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,3)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbourTest7()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile tile = gMap.getTile(3, 1);
            List<Tile> actualNeighbours = tile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,1)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbourTest8()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile tile = gMap.getTile(3, 2);
            List<Tile> actualNeighbours = tile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(3,1)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbourTest9()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile tile = gMap.getTile(3,3);
            List<Tile> actualNeighbours = tile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(3,2),
                gMap.getTile(2,3)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test1()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 1);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,2),
                gMap.getTile(2,2),

            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }

        [TestMethod()]
        public void getNeighbour2Test2()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 2);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,1),
                gMap.getTile(2,2),
                gMap.getTile(1,3)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test3()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 3);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,2),
                gMap.getTile(1,4)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test4()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 4);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,3),
                gMap.getTile(1,5)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test5()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 5);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,5),
                gMap.getTile(1,4)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test6()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(2, 1);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,1),
                gMap.getTile(2,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test7()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(2,2);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(3,2),
                gMap.getTile(1,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test8()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(2, 5);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,5),
                gMap.getTile(3,5)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test9()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(3, 1);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,1)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test10()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(3, 2);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(3,1),
                gMap.getTile(4,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test11()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(3, 3);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(3,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test12()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(3, 5);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,5),
                gMap.getTile(4,5)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test13()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(4, 2);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(4,3)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test14()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(4, 3);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(3,3),
                gMap.getTile(4,4)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test15()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(4, 4);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(4,5),
                gMap.getTile(5,4),
                gMap.getTile(5,5)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test16()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(4, 5);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(3, 5),
                gMap.getTile(5, 5),
                gMap.getTile(4, 4)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test17()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(5, 3);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(4,3)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test18()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(5,4);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(5,3)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour2Test19()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00110",
                "00010",
                "10000",
                "11000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(5, 5);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(4,5),
                gMap.getTile(5,4)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour3Test1()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00011",
                "00001",
                "01000",
                "01100"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 1);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour3Test2()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00011",
                "00001",
                "01000",
                "01100"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 2);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,1),
                gMap.getTile(2,2),
                gMap.getTile(1,3)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour3Test3()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00011",
                "00001",
                "01000",
                "01100"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 3);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,2),
                gMap.getTile(1,4),
                gMap.getTile(2,3)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour3Test4()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00011",
                "00001",
                "01000",
                "01100"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 4);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,3),
                gMap.getTile(1,5)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour3Test5()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00011",
                "00001",
                "01000",
                "01100"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(1, 5);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(1,4)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour3Test6()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00011",
                "00001",
                "01000",
                "01100"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(2, 1);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,2),
                gMap.getTile(1,1),
                gMap.getTile(3,1)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour3Test7()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00011",
                "00001",
                "01000",
                "01100"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(2, 2);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,3),
                gMap.getTile(3,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour4Test1()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00000",
                "00000",
                "00000",
                "00000"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(3, 3);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,3),
                gMap.getTile(3,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
        [TestMethod()]
        public void getNeighbour4Test2()
        {
            string[] mapString = new string[]
            {
                "00000",
                "00000",
                "00000",
                "10000",
                "10100"
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile topLeftTile = gMap.getTile(3, 3);
            List<Tile> actualNeighbours = topLeftTile.getNonWallNeighbours(true);
            List<Tile> expectedNeighbours = new List<Tile>()
            {
                gMap.getTile(2,3),
                gMap.getTile(3,2)
            };
            CollectionAssert.AreEquivalent(expectedNeighbours, actualNeighbours);
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RTSGame.Models.Configurations;
using RTSGame.Models.DeadlockReslovers;
using RTSGame.Models.SearchControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RTSGame.ParameterConfiguration;

namespace RTSGame.Models.SearchControllers.Tests
{
   
    class TestHelper
    {
        public ExperimentParameters GetExperimentParameters(int numberOfAgents)
        {
            Dictionary<string, string> param = new Dictionary<string, string>() {
                            { "visible agent distance", "0"},
                            { "numberOfReservations", "3"},
                            { "forceOffGoal", "true"},
                            { "useDeadlockBreakingProcedure", "true" },
                            { "useFlowRestricted","true"}
                };
            AlgorithmParameters algorithmConfig = new AlgorithmParameters
            {
                parameters = param,
                heuristic = "octile",
                name = "FAR"
            };
            List<AlgorithmParameters> algorithmParams = new List<AlgorithmParameters>();
            for (int i = 0; i < numberOfAgents; i++)
            {
                algorithmParams.Add(algorithmConfig);
            }
            ExperimentParameters parameters = new ExperimentParameters
            {

                algorithmParameters = algorithmParams,
                experimentParameters = new Configurations.GameParameters
                {
                    fourConnected = false,
                    removeAgentsAtGoal = false,
                    numberOfAgents = 4,
                    cutoffs = new Configurations.Cutoffs
                    {
                        distance = 0,
                        goalAchivementTime = 0,
                        steps = 0,
                        thinkingTime = 0,
                        timePerMove = 0
                    }
                }
            };

            return parameters;
        }
    }

    [TestClass()]
    public class FARAStarControllerTests
    {

        [TestMethod()]
        public void IsDeadLockedTest1()
        {
            TestHelper th = new TestHelper();
            ExperimentParameters parameters = th.GetExperimentParameters(4); 
        
            FARAStarController controller = new FARAStarController(parameters);
            string[] mapString = new string[]
            {
                "00000", // l
                "00000", // r
                "00000", // l 
                "00000", // r
                "00000", // l
             //  ududu
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile start1 = gMap.getTile(3, 2);
            Tile start2 = gMap.getTile(3, 3);
            Tile start3 = gMap.getTile(4, 2);
            Tile start4 = gMap.getTile(4, 3);
            Tile goal1 = gMap.getTile(5, 2);
            Tile goal2 = gMap.getTile(3, 1);
            Tile goal3 = gMap.getTile(4, 4);
            Tile goal4 = gMap.getTile(2, 3);
            controller.CreateSearchAgent(start1,goal1,0,gMap.width,gMap.height,parameters);
            controller.CreateSearchAgent(start2,goal2,1,gMap.width,gMap.height,parameters);
            controller.CreateSearchAgent(start3,goal3,2,gMap.width,gMap.height,parameters);
            controller.CreateSearchAgent(start4,goal4,3,gMap.width,gMap.height,parameters);
            controller.GetAllAgentsMoves();
            controller.ReserveNextMoves();
            FARAgent agent = (FARAgent)controller.agents[0];
            DeadlockController deadlockController = new DeadlockController();
            bool actual = deadlockController.IsDeadLocked(agent);
            Assert.IsTrue(actual);
        }
        [TestMethod()]
        public void IsDeadLockedTest2()
        {
            TestHelper th = new TestHelper();
            ExperimentParameters parameters = th.GetExperimentParameters(4);
            FARAStarController controller = new FARAStarController(parameters);
            string[] mapString = new string[]
            {
                "00000", // l
                "00000", // r
                "00000", // l 
                "00000", // r
                "00000", // l
                         //  ududu
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile start1 = gMap.getTile(3, 2);
            Tile start2 = gMap.getTile(3, 3);
            Tile start3 = gMap.getTile(4, 2);
            Tile start4 = gMap.getTile(4, 3);
            Tile goal1 = gMap.getTile(5, 2);
            Tile goal2 = gMap.getTile(3, 1);
            Tile goal3 = gMap.getTile(4, 4);
            Tile goal4 = gMap.getTile(4, 5);
            controller.CreateSearchAgent(start1, goal1, 0, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start2, goal2, 1, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start3, goal3, 2, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start4, goal4, 3, gMap.width, gMap.height, parameters);
            controller.GetAllAgentsMoves();
            FARAgent agent = (FARAgent)controller.agents[0];
            DeadlockController deadlockController = new DeadlockController();

            bool actual = deadlockController.IsDeadLocked(agent);
            Assert.IsFalse(actual);
        }
        [TestMethod()]
        public void IsDeadLockedTest3()
        {
            TestHelper th = new TestHelper();
            ExperimentParameters parameters = th.GetExperimentParameters(4);
            FARAStarController controller = new FARAStarController(parameters);
            string[] mapString = new string[]
            {
                "00000", // l
                "00000", // r
                "00000", // l 
                "00000", // r
                "00000", // l
             //  ududu
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile start1 = gMap.getTile(3, 2);
            Tile start2 = gMap.getTile(3, 3);
            Tile start3 = gMap.getTile(4, 2);
            Tile start4 = gMap.getTile(4, 3);
            Tile goal1 = gMap.getTile(5, 2);
            Tile goal2 = gMap.getTile(3, 1);
            Tile goal3 = gMap.getTile(4, 4);
            Tile goal4 = gMap.getTile(4, 3);
            controller.CreateSearchAgent(start1, goal1, 0, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start2, goal2, 1, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start3, goal3, 2, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start4, goal4, 3, gMap.width, gMap.height, parameters);
            controller.GetAllAgentsMoves();
            FARAgent agent = (FARAgent)controller.agents[0];
            DeadlockController deadlockController = new DeadlockController();

            bool actual = deadlockController.IsDeadLocked(agent);
            Assert.IsFalse(actual);
        }
        [TestMethod()]
        public void GetDeadLockedAgents1()
        {

            TestHelper th = new TestHelper();
            ExperimentParameters parameters = th.GetExperimentParameters(4);
            FARAStarController controller = new FARAStarController(parameters);
            string[] mapString = new string[]
            {
                "00000", // l
                "00000", // r
                "00000", // l 
                "00000", // r
                "00000", // l
             //  ududu
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile start1 = gMap.getTile(3, 2);
            Tile start2 = gMap.getTile(3, 3);
            Tile start3 = gMap.getTile(4, 2);
            Tile start4 = gMap.getTile(4, 3);
            Tile goal1 = gMap.getTile(5, 2);
            Tile goal2 = gMap.getTile(3, 1);
            Tile goal3 = gMap.getTile(4, 4);
            Tile goal4 = gMap.getTile(2, 3);
            controller.CreateSearchAgent(start1, goal1, 0, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start2, goal2, 1, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start3, goal3, 2, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start4, goal4, 3, gMap.width, gMap.height, parameters);
            controller.GetAllAgentsMoves();
            controller.ReserveNextMoves();
            FARAgent farAgent = (FARAgent)controller.agents[0];
            DeadlockController deadlockController = new DeadlockController();

            bool actual = deadlockController.IsDeadLocked(farAgent);
            Assert.IsTrue(actual);
            HashSet<SearchAgent> gridLockedAgents = deadlockController.GetDeadLockedAgents(farAgent);
            HashSet<SearchAgent> expectedGridLockedAgents = new HashSet<SearchAgent>();
            foreach(SearchAgent agent in controller.agents)
            {
                expectedGridLockedAgents.Add(agent);
            }
            Assert.IsTrue(gridLockedAgents.SetEquals(expectedGridLockedAgents));
        }
        [TestMethod()]
        public void GetDeadLockedAgents2()
        {
            TestHelper th = new TestHelper();
            ExperimentParameters parameters = th.GetExperimentParameters(4);
            FARAStarController controller = new FARAStarController(parameters);
            string[] mapString = new string[]
            {
                "00000", // l
                "00000", // r
                "00000", // l 
                "00000", // r
                "00000", // l
             //  ududu
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile start1 = gMap.getTile(3, 2);
            Tile start2 = gMap.getTile(3, 3);
            Tile start3 = gMap.getTile(4, 2);
            Tile start4 = gMap.getTile(4, 3);
            Tile goal1 = gMap.getTile(5, 2);
            Tile goal2 = gMap.getTile(3, 1);
            Tile goal3 = gMap.getTile(4, 4);
            Tile goal4 = gMap.getTile(4, 3);
            controller.CreateSearchAgent(start1, goal1, 0, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start2, goal2, 1, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start3, goal3, 2, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start4, goal4, 3, gMap.width, gMap.height, parameters);
            controller.GetAllAgentsMoves();
            controller.ReserveNextMoves();

            DeadlockController deadlockController = new DeadlockController();

            SearchAgent farAgent = controller.agents[0];
            bool actual = deadlockController.IsDeadLocked(farAgent);
            Assert.IsFalse(actual);
            HashSet<SearchAgent> gridLockedAgents = deadlockController.GetDeadLockedAgents(farAgent);
            HashSet<SearchAgent> expectedGridLockedAgents = new HashSet<SearchAgent>();
            Assert.IsTrue(gridLockedAgents.SetEquals(expectedGridLockedAgents), "Had different agents");
        }
        [TestMethod()]
        public void ResolveDeadLockedTest1()
        {
            TestHelper th = new TestHelper();
            ExperimentParameters parameters = th.GetExperimentParameters(4);
            FARAStarController controller = new FARAStarController(parameters);
            string[] mapString = new string[]
            {
                "00000", // l
                "00000", // r
                "00000", // l 
                "00000", // r
                "00000", // l
                         //  ududu
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile start1 = gMap.getTile(3, 2);
            Tile start2 = gMap.getTile(3, 3);
            Tile start3 = gMap.getTile(4, 2);
            Tile start4 = gMap.getTile(4, 3);
            Tile goal1 = gMap.getTile(5, 2);
            Tile goal2 = gMap.getTile(3, 1);
            Tile goal3 = gMap.getTile(4, 4);
            Tile goal4 = gMap.getTile(4, 5);
            controller.CreateSearchAgent(start1, goal1, 0, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start2, goal2, 1, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start3, goal3, 2, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start4, goal4, 3, gMap.width, gMap.height, parameters);
            controller.GetAllAgentsMoves();
            controller.ReserveNextMoves();

            DeadlockController deadlockController = new DeadlockController();
            SearchAgent agent = controller.agents[0];
            bool actual = deadlockController.IsDeadLocked(agent);
            Assert.IsFalse(actual);
            int maxmoves = 20;
            int moves = 0;
            while (moves < maxmoves) {
                controller.UpdateAllAgents();
                moves++;
            }
            bool allAgentsAtGoal = controller.agents.All(t=> t.currentSpot == t.goal);
            Utilities utils = new Utilities();
            decimal expectedDistances = utils.GetEuclideanDistance(start1.col, start1.row, goal1.col, goal1.row) +
                utils.GetEuclideanDistance(start2.col, start2.row, goal2.col, goal2.row) +
                utils.GetEuclideanDistance(start3.col, start3.row, goal3.col, goal3.row) +
                utils.GetEuclideanDistance(start4.col, start4.row, goal4.col, goal4.row);
            decimal actualDistance = controller.agents.Sum(t => (decimal)t.distanceTravelled);

            Assert.IsTrue(allAgentsAtGoal);
            Assert.AreEqual(expectedDistances, actualDistance);
        }

        [TestMethod()]
        public void ResolveDeadLockedTest2()
        {
            TestHelper th = new TestHelper();
            ExperimentParameters parameters = th.GetExperimentParameters(4);
            FARAStarController controller = new FARAStarController(parameters);
            string[] mapString = new string[]
            {
                "00000", // l
                "00000", // r
                "00000", // l 
                "00000", // r
                "00000", // l
                         //  ududu
            };
            GridMap gMap = new GridMap(mapString, "1");

            Tile start3 = gMap.getTile(4, 2);
            Tile start4 = gMap.getTile(4, 3);
            Tile goal3 = gMap.getTile(4, 4);
            Tile goal4 = gMap.getTile(4, 3);
            controller.CreateSearchAgent(start3, goal3, 2, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start4, goal4, 3, gMap.width, gMap.height, parameters);
            controller.GetAllAgentsMoves();
            controller.ReserveNextMoves();

            FARAgent agent = (FARAgent)controller.agents[0];
            DeadlockController deadlockController = new DeadlockController();

            bool actual = deadlockController.IsDeadLocked(agent);
            Assert.IsFalse(actual);
            int maxmoves = 20;
            int moves = 0;
            while (moves < maxmoves)
            {
                controller.UpdateAllAgents();
                moves++;
            }
            bool allAgentsAtGoal = controller.agents.All(t => t.currentSpot == t.goal);
            Utilities utils = new Utilities();
            decimal perfectDistances = utils.GetEuclideanDistance(start3.col, start3.row, goal3.col, goal3.row) +
                utils.GetEuclideanDistance(start4.col, start4.row, goal4.col, goal4.row);
            decimal actualDistance = controller.agents.Sum(t => (decimal)t.distanceTravelled);

            Assert.IsTrue(allAgentsAtGoal);
            Assert.IsTrue(perfectDistances < actualDistance, "The agent moved an impossibly little distance");
        }
        [TestMethod()]
        public void ResolveDeadLockedTest3()
        {
            TestHelper th = new TestHelper();
            ExperimentParameters parameters = th.GetExperimentParameters(4);
            FARAStarController controller = new FARAStarController(parameters);
            string[] mapString = new string[]
            {
                "00000", // l
                "00000", // r
                "00000", // l 
                "00000", // r
                "00000", // l
             //  ududu
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile start1 = gMap.getTile(3, 2);
            Tile start2 = gMap.getTile(3, 3);
            Tile start3 = gMap.getTile(4, 2);
            Tile start4 = gMap.getTile(4, 3);
            Tile goal1 = gMap.getTile(5, 2);
            Tile goal2 = gMap.getTile(3, 1);
            Tile goal3 = gMap.getTile(4, 4);
            Tile goal4 = gMap.getTile(2, 3);
            controller.CreateSearchAgent(start1, goal1, 0, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start2, goal2, 1, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start3, goal3, 2, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start4, goal4, 3, gMap.width, gMap.height, parameters);
            controller.GetAllAgentsMoves();
            controller.ReserveNextMoves();
            FARAgent agent = (FARAgent)controller.agents[0];
            DeadlockController deadlockController = new DeadlockController();
            
            bool actual = deadlockController.IsDeadLocked(agent);
            Assert.IsTrue(actual);
            int maxmoves = 20;
            int moves = 0;
            while (moves < maxmoves)
            {
                controller.UpdateAllAgents();
                moves++;
            }
            bool allAgentsAtGoal = controller.agents.All(t => t.currentSpot == t.goal);
            Utilities utils = new Utilities();
            decimal perfectDistances = utils.GetEuclideanDistance(start1.col, start1.row, goal1.col, goal1.row) +
                utils.GetEuclideanDistance(start2.col, start2.row, goal2.col, goal2.row) +
                utils.GetEuclideanDistance(start3.col, start3.row, goal3.col, goal3.row) +
                utils.GetEuclideanDistance(start4.col, start4.row, goal4.col, goal4.row);
            decimal actualDistance = controller.agents.Sum(t => (decimal)t.distanceTravelled);

            Assert.IsTrue(allAgentsAtGoal);
            Assert.IsTrue(perfectDistances < actualDistance, "The agent moved an impossibly little distance");
        }
        [TestMethod()]
        public void ResolveDeadLockedTest4()
        {
            TestHelper th = new TestHelper();
            ExperimentParameters parameters = th.GetExperimentParameters(4);
            FARAStarController controller = new FARAStarController(parameters);
            string[] mapString = new string[]
            {
                "10000", // l 1
                "11011", // r 2
                "11011", // l 3
                "10011", // r 4
                "10011", // l 5
                "10000", // r 6
                "10000", // l 7
                "10000"  // r 8
             //  ududu
             //  12345
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile start1 = gMap.getTile(3, 3);
            Tile start2 = gMap.getTile(4, 2);

            Tile goal1 = gMap.getTile(7, 3);
            Tile goal2 = gMap.getTile(4, 2);

            controller.CreateSearchAgent(start1, goal1, 0, gMap.width, gMap.height, parameters);
            controller.CreateSearchAgent(start2, goal2, 1, gMap.width, gMap.height, parameters);

            int maxmoves = 20;
            int moves = 0;
            while (moves < maxmoves)
            {
                controller.Update();
                moves++;
            }
            bool allAgentsAtGoal = controller.agents.All(t => t.currentSpot == t.goal);
            Utilities utils = new Utilities();
            decimal perfectDistances = utils.GetEuclideanDistance(start1.col, start1.row, goal1.col, goal1.row) +
                utils.GetEuclideanDistance(start2.col, start2.row, goal2.col, goal2.row);
            decimal actualDistance = controller.agents.Sum(t => (decimal)t.distanceTravelled);

            Assert.IsTrue(allAgentsAtGoal);
            Assert.IsTrue(perfectDistances < actualDistance, "The agent moved an impossibly little distance");
        }

    }
}

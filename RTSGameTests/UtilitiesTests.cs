﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RTSGame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTSGame.Tests
{
    [TestClass()]
    public class UtilitiesTests
    {
        //[TestMethod()]
        //public void createMapTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void allAgentsAtGoalTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void expenableTestEmpty()
        {
            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            Utilities utils = new Utilities();

            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(2, 2);
            Boolean expendable = utils.Expenable(midTile.getNonWallNeighbours(), midTile, new HashSet<int>());
            Boolean expected = false;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue1()
        {

            string[] mapString = new string[]
            {
                "100",
                "000",
                "000"
            };
            Utilities utils = new Utilities();

            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            Boolean expendable = utils.Expenable(midTile.getNonWallNeighbours(), midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue2()
        {
            Utilities utils = new Utilities();

            string[] mapString = new string[]
            {
                    "100",
                    "000",
                    "001"
            };
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            Boolean expendable = utils.Expenable(midTile.getNonWallNeighbours(), midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue3()
        {

            string[] mapString = new string[]
            {
                "10",
                "00",
            };
            Utilities utils = new Utilities();
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            Boolean expendable = utils.Expenable(midTile.getNonWallNeighbours(), midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue4()
        {

            string[] mapString = new string[]
            {
                "10",
                "10",
            };
            Utilities utils = new Utilities();
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            Boolean expendable = utils.Expenable(midTile.getNonWallNeighbours(), midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue5()
        {

            string[] mapString = new string[]
            {
                "101",
                "100",
                "111",

            };
            Utilities utils = new Utilities();
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            Boolean expendable = utils.Expenable(midTile.getNonWallNeighbours(), midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue6()
        {

            string[] mapString = new string[]
            {
                "101",
                "110",
                "111",

            };
            Utilities utils = new Utilities();
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 2);
            Boolean expendable = utils.Expenable(midTile.getNonWallNeighbours(), midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallFalse1()
        {

            string[] mapString = new string[]
            {
                "00",
                "01",
                "01"
            };
            Utilities utils = new Utilities();
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(2, 1);
            Boolean expendable = utils.Expenable(midTile.getNonWallNeighbours(), midTile, new HashSet<int>());
            Boolean expected = false;
            Assert.AreEqual(expected, expendable);
        }

        /// <summary>
        /// Testing filtering of previous expended states
        /// </summary>
        public void expenableTestWallTrue1Filtered()
        {

            string[] mapString = new string[]
            {
                "000",
                "000",
                "000"
            };
            Utilities utils = new Utilities();
            List<int> expandedTiles = new List<int>();
            expandedTiles.Add(1);
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            List<Tile> neighbours = midTile.getNonWallNeighbours();
            neighbours = neighbours.Where(d => !expandedTiles.Contains(d.id)).ToList(); // remove expanded nodes;

            Boolean expendable = utils.Expenable(neighbours, midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue2Filtered()
        {
            string[] mapString = new string[]
             {
                "000",
                "000",
                "000"
             };
            Utilities utils = new Utilities();
            List<int> expandedTiles = new List<int>();
            expandedTiles.Add(0);
            expandedTiles.Add(8);
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            List<Tile> neighbours = midTile.getNonWallNeighbours();
            neighbours = neighbours.Where(d => !expandedTiles.Contains(d.id)).ToList(); // remove expanded nodes;

            Boolean expendable = utils.Expenable(neighbours, midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue3Filtered()
        {

            string[] mapString = new string[]
            {
                "10",
                "00",
            };
            Utilities utils = new Utilities();
            List<int> expandedTiles = new List<int>();
            expandedTiles.Add(1);
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            List<Tile> neighbours = midTile.getNonWallNeighbours();
            neighbours = neighbours.Where(d => !expandedTiles.Contains(d.id)).ToList(); // remove expanded nodes;

            Boolean expendable = utils.Expenable(neighbours, midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue4Filtered()
        {

            string[] mapString = new string[]
            {
                "10",
                "10",
            };
            Utilities utils = new Utilities();
            List<int> expandedTiles = new List<int>();
            expandedTiles.Add(0);
            expandedTiles.Add(3);
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            List<Tile> neighbours = midTile.getNonWallNeighbours();
            neighbours = neighbours.Where(d => !expandedTiles.Contains(d.id)).ToList(); // remove expanded nodes;

            Boolean expendable = utils.Expenable(neighbours, midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue5Filtered()
        {

            string[] mapString = new string[]
            {
                "101",
                "100",
                "111",

            };
            Utilities utils = new Utilities();
            List<int> expandedTiles = new List<int>();
            expandedTiles.Add(1);

            expandedTiles.Add(6);

            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 1);
            List<Tile> neighbours = midTile.getNonWallNeighbours();
            neighbours = neighbours.Where(d => !expandedTiles.Contains(d.id)).ToList(); // remove expanded nodes;

            Boolean expendable = utils.Expenable(neighbours, midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallTrue6Filtered()
        {

            string[] mapString = new string[]
            {
                "101",
                "110",
                "111",

            };
            Utilities utils = new Utilities();

         

            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(1, 2);
            List<Tile> neighbours = midTile.getNonWallNeighbours();

            Boolean expendable = utils.Expenable(neighbours, midTile, new HashSet<int>());
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        [TestMethod()]
        public void expenableTestWallFalse1Filtered()
        {

            string[] mapString = new string[]
            {
                "00",
                "01",
                "01"
            };
            Utilities utils = new Utilities();

            HashSet<int> expandedTiles = new HashSet<int>();
            
           
            GridMap gMap = new GridMap(mapString, "1");
            Tile midTile = gMap.getTile(2, 1);
            Tile topLeftTile = gMap.getTile(1, 1);
            Tile topRightTile = gMap.getTile(1, 2);

            expandedTiles.Add(topLeftTile.id);
            expandedTiles.Add(topRightTile.id);
            List<Tile> neighbours = midTile.getNonWallNeighbours();
            neighbours = neighbours.Where(d => !expandedTiles.Contains(d.id)).ToList(); // remove expanded nodes;

            Boolean expendable = utils.Expenable(neighbours, midTile, expandedTiles);
            Boolean expected = true;
            Assert.AreEqual(expected, expendable);
        }
        
        //[TestMethod()]
        //public void getMedianTest()
        //{
        //    Assert.Fail();
        //}
    }
}